#ifndef __SPDM3_DISTSPMAT_H__
#define __SPDM3_DISTSPMAT_H__

#include "comm.h"
#include "spmat.h"

namespace spdm3 {

// Prototype for DistDMat.
template <class IT, class VT>
class DistDMat;

template <class IT, class VT>
class DistSpMat {
 public:
  //
  // Constructors & Destructor.
  //
  DistSpMat(Comm *comm, spdm3_format format = SPARSE_CSR);
  ~DistSpMat();
  
  //
  // Allocation.
  //
  void AllocateAux();
  void DeallocateAux();
  void GreedyPartitioning(IT *nnz_in_col);
  void UniformPartitioning();
  void CheckTmpMatSize();
  
  //
  // I/O.
  //
  void SetSize(int global_rows, int global_cols);
  void SetIdentity(int n);
  
  void GenerateUniform(int global_rows, int global_cols,
                       double nnz_ratio, int seed = 1);
  void GenerateUniform2(int global_rows, int global_cols,
                       double nnz_ratio, int seed = 1);
  void GenerateWithG500(double initiator[4], int log_numverts, double nnz_ratio, int graphs);
  void GenerateWithRMAT(int global_rows, int global_cols, double nnz_ratio,
                        double a, double b, double c, double d, int graphs);
  void GenerateSymmetric(int global_rows, int global_cols,
                         double nnz_ratio, int graphs);
  
  void LoadMatrixMarket(const char *filename); // Reads Matrix Market in parallel (adapted from Aydin's COMBBLAS).

  void SaveMatrixMarket(const char *filename);
  void SaveLocal(const char *filename);
  void SaveLocalDense(const char *filename);
  void SaveTeam(const char *filename);
  void SaveTeamDense(const char *filename);
  void Save(const char *filename);
  void SaveDense(const char *filename);
  void SavePPM(const char *filename);
  void SavePNG(const char *filename);
  void Diagonal(const DistSpMat<IT, VT> &X);
  void Diagonal(const DistDMat<IT, VT> &X);
  void Convert(const DistDMat<IT, VT> &X);
  void Add(const VT alpha, const DistSpMat<IT, VT> &A,
           const VT beta, const DistSpMat<IT, VT> &B);
  void WeirdLayoutBlockColumn(const DistSpMat<IT, VT> &X);
  void WeirdLayoutBlockRow(const DistSpMat<IT, VT> &X);
  void WeirdLayoutBlockRowTransposed(const DistSpMat<IT, VT> &X);
  void WeirdLayoutDiagonal(const DistSpMat<IT, VT> &X);
  void WeirdLayoutDiagonalTransposed(const DistSpMat<IT, VT> &X);
  void print(const char *fmt, ...);
  void printr(const char *fmt, ...);
  
  //
  // Operations.
  //
  void CheckCommsAreInitialized() const;
  bool CheckDimMatch(const DistDMat<IT, VT> &X);
  bool CheckDimMatch(const DistSpMat<IT, VT> &X);
  void SwapBuffers();
  VT FrobeniusNorm();
  void AddPiggyback(const VT *values, const IT len);
  const VT *GetPiggyback();
  void UpdateNnzCountAndDispls();
  void GetPartitioningFromLocalMatrices();
  
  // In-place.
  void ElmtWiseOp(std::function<VT(VT)> fn);
  void AddI(VT alpha, VT beta, const DistSpMat<IT, VT> &X);
  void DotMultiplyI(const DistSpMat<IT, VT> &X);
  void DotMultiplyI(const DistDMat<IT, VT> &X);
  void DotMultiplyTransposeI(const DistDMat<IT, VT> &X);
  void CommonCopy(const DistSpMat<IT, VT> &X);
  void CommonCopy(const DistDMat<IT, VT> &X);
  void DeepCopy(const DistSpMat<IT, VT> &X);
  void ShallowCopy(const DistSpMat<IT, VT> &X);
  void TakeOwnership(DistSpMat<IT, VT> &X);

  // New copy.
  DistSpMat<IT, VT> Add(VT alpha, VT beta, const DistSpMat<IT, VT> &X);
  DistSpMat<IT, VT> GetShallowCopy(bool transfer_buffer_ownership = false);
  DistSpMat<IT, VT> GetDeepCopy();
  DistSpMat<IT, VT> Diagonal();
  
  //
  // Communication.
  //
  void Shift(const spdm3_comm &comm, const IT &distance);
  void ShiftPiggyback(const spdm3_comm &comm, const IT &distance,
                      const IT piggyback_len);
  void ShiftAsync(const spdm3_comm &comm, const IT &distance);
  void ShiftWait();
  void Bcast(const spdm3_comm &comm, const IT &root);
  void Reduce(const spdm3_comm &comm, const IT &root);
  SpMat<IT, VT> *GetLocalSpMatsFromComm(const Comm& comm);
  SpMat<IT, VT> GetWholeMatrix();
  void Replicate(Comm* new_comm);
  void FillFromGraphs(Triplet **triplets, int local_graphs, LT total_edge_count);
  void FillTriplets(std::vector<Triplet> input);
  void ResetCounters();
  void ResetShift(const spdm3_comm &comm);
  void ResetShifts();
  
  //
  // Setters.
  //
  void SetDistribution(spdm3_dist dist) { dist_ = dist; }
  
  //
  // Getters.
  //
  IT rank() const;
  IT nprocs() const;
  IT rows() const;
  IT cols() const;
  IT row_displs() const;
  IT col_displs() const;
  
  IT lrows() const;
  IT lcols() const;
  IT max_ldim1() const;
  IT max_ldim2() const;

  IT dim1() const;
  IT dim2() const;
  IT dim1_displs() const;
  IT dim2_displs() const;
  
  IT lrank() const;  // layer rank (a.k.a. team id).
  IT trank() const;  // team rank (a.k.a. layer id).
  IT layer_id() const;
  IT team_id() const;
  IT layers() const;
  IT teams() const;
  spdm3_layout layout() const;
  
  LT nnz() const;
  VT nnz_ratio() const;
  
  //
  // Queries.
  //
  IT FindOwner(spdm3_find_owner method,
               IT global_row_id, IT global_col_id,
               IT &blk_row, IT &blk_col);
  VT FrobeniusNormSquare();
  
  //
  // Member variables.
  //
  IT global_rows_;
  IT global_cols_;
  
  IT max_local_rows_;
  IT max_local_cols_;
  LT max_local_nnz_;
  
  SpMat<IT, VT> mat_;
  SpMat<IT, VT> tmp_mat_;
  IT *row_counts_;
  IT *row_displs_;
  IT *col_counts_;
  IT *col_displs_;
  LT *nnz_counts_;
  LT *nnz_displs_;
  
  IT *distance_shifted_;
  IT *owner_rank_;
  
  char *name_;
  spdm3_dist dist_;  // Distribution.
  
  Comm *comm_;  // Not owned.
  MPI_Datatype mpi_it_;
  MPI_Datatype mpi_lt_;
  MPI_Datatype mpi_vt_;
};

}  // namespace spdm3

#include "distspmat.cpp"

#endif  // __SPDM3_DISTSPMAT_H__
