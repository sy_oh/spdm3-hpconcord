#ifndef __SPDM3_COMM_H__
#define __SPDM3_COMM_H__

#include <mpi.h>
#include "defs.h"
#include "utility.h"

namespace spdm3 {

struct Comm {
  int id;
  int rank;
  int size;
  MPI_Comm comm;
  spdm3_layout layout;
  
  Comm() : id(-1), rank(-1), size(-1), comm(MPI_COMM_NULL) {}
  ~Comm() {
    if (comm != MPI_COMM_NULL)
      MPI_Comm_free(&comm);
  }
};  // struct Comm

struct TransposeInfo {
  MPI_Comm comm;
  int c_f;
  int c_r;
  int c_ratio;
  int comm_size;
  int row_teams;
  int col_teams;
  int ranks_per_grid;
  int num_grids_in_1D;
  int row_teams_per_blk;
  int col_teams_per_blk;
  int starting_team_col_id;
  int blks_per_grid;
  int *rowcounts;  // In weird format.
  int *colcounts;
  int *ldas;
  int *sendcounts;
  int *senddispls;
  int *sendcounts_short;
  int *senddispls_short;
  
  int rank;
  int team_row_id;
  
  TransposeInfo()
    : comm(MPI_COMM_NULL), c_f(0), c_r(0), c_ratio(0),
      comm_size(0), row_teams(0), col_teams(0), ranks_per_grid(0),
      num_grids_in_1D(0), row_teams_per_blk(0), col_teams_per_blk(0),
      starting_team_col_id(0), blks_per_grid(0),
      rowcounts(NULL), colcounts(NULL),
      sendcounts(NULL), senddispls(NULL),
      sendcounts_short(NULL), senddispls_short(NULL),
      ldas(NULL) {}
  
  // Can't do destructor style or MPI routines will be called after MPI_Finalize().
  void destroy() {
    if (comm != MPI_COMM_NULL)
      MPI_Comm_free(&comm);
    
    FREE_IF_NOT_NULL(rowcounts);
    FREE_IF_NOT_NULL(colcounts);
    FREE_IF_NOT_NULL(sendcounts);
    FREE_IF_NOT_NULL(senddispls);
    FREE_IF_NOT_NULL(ldas);
  }
};  // struct TransposeInfo

Comm *GetComms(const MPI_Comm &world,
               spdm3_layout layout = ONED_BLOCK_ROW,
               int layers = 1);
void GetMpiCommSubset(int nprocs, MPI_Comm input, MPI_Comm *output);
int GetLayerRank(Comm *comm, spdm3_layout layout, int row_id, int col_id);
void GetRowColIdFromLayerRank(Comm *comm, spdm3_layout layout, int layer_rank,
                              int *row_id, int *col_id);

template <class VT>
void Allreduce3items(VT &a, VT &b, VT &c, MPI_Comm comm) {
  VT array[3] = {a, b, c};
  MPI_Datatype mpi_vt = (sizeof(VT) == 4)? MPI_FLOAT : MPI_DOUBLE;
  MPI_Allreduce(MPI_IN_PLACE, array, 3, mpi_vt, MPI_SUM, comm);
  a = array[0]; b = array[1], c = array[2];
}

}  // namespace spdm3

#endif  // __SPDM3_COMM_H__