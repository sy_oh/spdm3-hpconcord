#ifndef __SPDM3_FLUSH_H__
#define __SPDM3_FLUSH_H__

namespace spdm3 {
  int *dummy = NULL;
  int dummy_size = 1024 * 1024 * 64;

  void init_dummy() {
    dummy = new int[dummy_size];
  }

  // Shifts the values in the array dummy to the left by one
  // and fills the last element with a new value.
  void flush_cache() {
    int i;
    for (i = 0; i < dummy_size - 1; ++i)
      dummy[i] = dummy[i+1];
    dummy[i] = rand() % 1000;
  }

  void clear_dummy() {
    delete [] dummy;
  }
}  // namespace spdm3

#endif  /* __SPDM3_FLUSH_H__ */