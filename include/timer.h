#ifndef __SPDM3_TIMER_H__
#define __SPDM3_TIMER_H__

// Checks custom flag to exclude MPI.
#ifndef  NO_MPI
  #include <mpi.h>
  #define wallclock() MPI_Wtime()
#else
  #include <omp.h>
  #define wallclock() omp_get_wtime()
#endif /* ~NO_MPI */

#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>

#include <map>
#include <string>
#include <vector>

namespace spdm3 {

class Timer {
 public:
  //
  // Constructor & destructor.
  //
  Timer();
  ~Timer();
  
  //
  // Routines.
  //
#ifndef NO_MPI
  void Init(MPI_Comm comm);
  void Finalize();
#endif /* ~NO_MPI */
  static inline double GetTime();
  inline double GetElapsedTime();
  double GetCounter(std::string name);
  void PrintCounterLocal(std::string name);
  void Start(std::string name);
  void Stop(std::string name);
  void Report();
  void SetPrint(bool print);
  void print(const char *fmt, ...);
  void printr(const char *fmt, ...);
  
  //
  // Data structure.
  //
#ifndef NO_MPI
  int rank_;
  int nprocs_;
  MPI_Comm comm_;
#endif /* ~NO_MPI */
  double start_;
  bool print_;
  
  std::vector<std::string> names_;
  std::vector<double> timers_;
  std::map<std::string, int> lookup_;
};

//
// Constructor & destructor.
//
Timer::Timer() : print_(false) {
  timers_.clear();
  lookup_.clear();
  
#ifdef NO_MPI
  start_ = wallclock();
#endif /* NO_MPI */
}

Timer::~Timer() {
  // Blank for now.
}

//
// Routines.
//
#ifndef NO_MPI
void Timer::Init(MPI_Comm comm) {
  MPI_Comm_dup(comm, &comm_);

  // Must be called after MPI_Init() in main().
  MPI_Comm_size(comm_, &nprocs_);
  MPI_Comm_rank(comm_, &rank_);
  
  start_ = wallclock();
}

void Timer::Finalize() {
  if (comm_ != MPI_COMM_NULL)
    MPI_Comm_free(&comm_);
}
#endif /* ~NO_MPI */

inline double Timer::GetTime() {
  return wallclock();
}

inline double Timer::GetElapsedTime() {
  return wallclock() - start_;
}

void Timer::Start(std::string name) {
  double t = GetTime();
  if (lookup_.find(name) == lookup_.end()) {
    lookup_[name] = timers_.size();
    timers_.push_back(-GetTime());
    names_.push_back(name);
  } else {
    timers_[lookup_[name]] -= t;
  }
  if (print_ && rank_ == 0) {
    printf("%10.4lf: Start %s\n", t - start_, name.c_str());
    fflush(stdout);
  }
}

void Timer::Stop(std::string name) {
  double t = GetTime();
  if (lookup_.find(name) == lookup_.end()) {
    printf("ERROR! Timer[%s] not found.\n", name.c_str());
    exit(1);
  }
  timers_[lookup_[name]] += t;
  if (print_ && rank_ == 0) {
    printf("%10.4lf: Done with %s\n", t - start_, name.c_str());
    fflush(stdout);
  }
}

double Timer::GetCounter(std::string name) {
  if (lookup_.find(name) == lookup_.end()) {
    printf("ERROR! Timer[%s] not found.\n", name.c_str());
    exit(1);
  }
  return timers_[lookup_[name]];
}

void Timer::PrintCounterLocal(std::string name) {
  printf("%d: %s: %lf s\n", rank_, name.c_str(), GetCounter(name));
}

void Timer::Report() {
#ifndef NO_MPI
  int n = timers_.size();
  double *sum, *avg, *min, *max;
  if (rank_ == 0) {
    sum = new double[n];
    avg = new double[n];
    min = new double[n];
    max = new double[n];
  }
  
  MPI_Reduce(&timers_[0], sum, n, MPI_DOUBLE, MPI_SUM, 0, comm_);
  MPI_Reduce(&timers_[0], min, n, MPI_DOUBLE, MPI_MIN, 0, comm_);
  MPI_Reduce(&timers_[0], max, n, MPI_DOUBLE, MPI_MAX, 0, comm_);
  
  if (rank_ == 0) {
    for (int i = 0; i < n; ++i)
      avg[i] = sum[i] / (double) nprocs_;
#endif /* ~NO_MPI */
    
    // Prints report.
    printf("------------------------------ ---------------- ---------------- ----------------\n");
    printf("%30s %16s %16s %16s\n", "Timers", "Average", "Minimum", "Maximum");
    printf("------------------------------ ---------------- ---------------- ----------------\n");
    for(int i = 0; i < n; ++i)
      printf("%30s %16.6lf %16.6lf %16.6lf\n", names_[i].c_str(), avg[i], min[i], max[i]);
    printf("------------------------------ ---------------- ---------------- ----------------\n");
    
#ifndef NO_MPI
    // Deallocation.
    delete [] sum;
    delete [] avg;
    delete [] min;
    delete [] max;
  }
#endif /* ~NO_MPI */
}

void Timer::SetPrint(bool print) {
  print_ = print;
}

void Timer::print(const char *fmt, ...) {
  char *content;
  int len = strlen(fmt);
  content = new char [len + 20];
  
  sprintf(content, "%d: %s", rank_, fmt);
  
  va_list args;
  va_start(args, fmt);
  vprintf(content, args);
  va_end(args);
  
  delete [] content;
}

void Timer::printr(const char *fmt, ...) {
  if (rank_ != 0) return;
  char *content;
  int len = strlen(fmt);
  content = new char [len + 20];
  
  sprintf(content, "%d: %s", rank_, fmt);
  
  va_list args;
  va_start(args, fmt);
  vprintf(content, args);
  va_end(args);
  
  delete [] content;
}

}  // namespace spdm3

#endif  // __SPDM3_TIMER_H__
