#ifndef __SPDM3_DISTMATMUL_H__
#define __SPDM3_DISTMATMUL_H__

namespace spdm3 {

//
// Forward declarations.
//
class Timer;
template <class IT, class VT> class DistDMat;
template <class IT, class VT> class DistSpMat;

//
// Distributed Sparse-Dense matmuls.
//
template <class IT, class VT>
VT innerABC_shiftc_w_trace(DistSpMat<IT, VT> &A,
                           DistDMat<IT, VT> &B,
                           DistDMat<IT, VT> &C, Timer &T);
  
template <class IT, class VT>
void innerABC_shiftc_transpose(DistDMat<IT, VT> &C,
                               DistDMat<IT, VT> &T, Timer &t);
 
template <class IT, class VT>
void innerABC_shiftc_reform_sparse_from_blockRow(DistDMat<IT, VT> &D,
                                                 DistSpMat<IT, VT> &S, Timer &T);
  
template <class IT, class VT>
VT innerABC_shift1(DistDMat<IT, VT> &A,
                   DistDMat<IT, VT> &B,
                   DistDMat<IT, VT> &C);

}  // namespace spdm3

#include "distmatmul.cpp"

#endif  // __SPDM3_DISTMATMUL_H__