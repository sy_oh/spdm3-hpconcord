#ifndef __SPDM3_DISTDMAT_H__
#define __SPDM3_DISTDMAT_H__

#include "comm.h"
#include "dmat.h"

namespace spdm3 {

// Prototype for DistSpMat
template <class IT, class VT>
class DistSpMat;

template <class IT, class VT>
class DistDMat {
 public:
  //
  // Constructors & Destructor.
  //
  DistDMat(Comm *comm, spdm3_format format = DENSE_ROWMAJOR);
  ~DistDMat();
  
  //
  // Allocation.
  //
  void AllocateAux();
  void DeallocateAux();
  
  //
  // I/O.
  //
  void GenerateWithFunction(IT global_rows, IT global_cols, int seed = 1000);
  void Save(const char *filename);
  void SaveLocal(const char *filename);
  void LoadNumPy(const char *filename);
  void LoadNumPyTranspose(const char *filename);
  void Transpose(const DistDMat<IT, VT> &X);
  void StoreElmtWise(std::function<VT(VT, VT)> fn,
                     const DistDMat<IT, VT> &a, const DistSpMat<IT, VT> &b);
  void Fill(IT global_rows, IT global_cols, VT val);
  void WeirdLayoutBlockColumn(const DistDMat<IT, VT> &X);
  void WeirdLayoutBlockRow(const DistDMat<IT, VT> &X);
  void WeirdLayoutBlockRowRotatingB(const DistDMat<IT, VT> &X, const DistDMat<IT, VT> &B);
  void WeirdLayoutBlockRowTransposed(const DistDMat<IT, VT> &X);
  void print(const char *fmt, ...);
  void printr(const char *fmt, ...);
  
  //
  // Operations.
  //
  void SetSize(IT global_rows, IT global_cols);
  void CheckCommsAreInitialized() const;
  void CheckDimMatch(const DistDMat<IT, VT> &X);
  void CheckDimMatch(const DistSpMat<IT, VT> &X);
  void SwapBuffers();

  // Stores results somewhere else.
  void MultiplyAdd(DistDMat<IT, VT> &B, DistDMat<IT, VT> &C);
  
  VT LocalTeamOpThenReduce(std::function<VT(VT)> fn, spdm3_op op);
  VT Checksum(spdm3_comm comm);
  VT LocalTrace();
  
  // In-place.
  void ElmtWiseOp(std::function<VT(VT, VT)> fn, const VT &op);
  void ElmtWiseOp(std::function<VT(VT, VT)> fn, const DistDMat<IT, VT> &Op);
  void ElmtWiseOp(std::function<VT(VT, VT)> fn, const DistSpMat<IT, VT> &Op);
  void ElmtWiseOpNzOnly(std::function<VT(VT, VT)> fn, const DistSpMat<IT, VT> &Op);
  void WeirdLayoutDiagOp(std::function<VT(VT, VT)> fn, const DistSpMat<IT, VT> &D);
  void CommonCopy(const DistDMat<IT, VT> &X);
  void CommonTranspose(const DistDMat<IT, VT> &X);
  void DeepCopy(const DistDMat<IT, VT> &X);
  void ShallowCopy(const DistDMat<IT, VT> &X);
  void TakeOwnership(DistDMat<IT, VT> &X);
  
  //
  // Communication.
  //
  void Shift(const spdm3_comm &comm, const IT &distance);
  void ShiftAsync(const spdm3_comm &comm, const IT &distance);
  void ShiftWait();
  void Bcast(const spdm3_comm &comm, const IT &root);
  void Reduce(const spdm3_comm &comm, const IT &root);
  void Allreduce(const spdm3_comm & comm);
  DMat<IT, VT> *GetLocalDMatsFromComm(const Comm& comm);
  DMat<IT, VT> GetWholeMatrix();
  void Replicate(Comm* new_comm);
  void ResetShift(const spdm3_comm &comm);
  void ResetShifts();
  
  //
  // Getters.
  //
  IT rank() const;
  IT nprocs() const;
  IT rows() const;
  IT cols() const;
  IT row_displs() const;
  IT col_displs() const;
  IT dim1() const;
  IT dim2() const;
  IT dim1_displs() const;
  IT dim2_displs() const;
  IT lrows() const;  // #rows of local matrix.
  IT lcols() const;  // #cols of local matrix.
  IT llda() const;
  IT max_ldim1() const;
  IT max_ldim2() const;
  IT max_llda() const;
  
  IT lrank() const;  // layer rank (a.k.a. team id).
  IT trank() const;  // team rank (a.k.a. layer id).
  IT layer_id() const;
  IT team_id() const;
  IT layers() const;
  IT teams() const;
  spdm3_layout layout() const;
  
  //
  // Member Variables.
  //
  IT global_rows_;
  IT global_cols_;
  
  DMat<IT, VT> mat_;
  DMat<IT, VT> tmp_mat_;
  IT *row_counts_;
  IT *row_displs_;
  IT *col_counts_;
  IT *col_displs_;
  
  IT max_local_rows_;
  IT max_local_cols_;
  IT max_local_lda_;
  
  IT *distance_shifted_;
  IT *owner_rank_;
  char *name_;
  
  Comm *comm_;  // Not owned.
  MPI_Datatype mpi_it_;
  MPI_Datatype mpi_lt_;
  MPI_Datatype mpi_vt_;
  MPI_Request mpi_send_req_;
  MPI_Request mpi_recv_req_;
  spdm3_comm shift_dir_;
};

}  // namespace spdm3

#include "distdmat.cpp"

#endif  // __SPDM3_DISTDMAT_H___
