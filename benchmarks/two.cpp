#include <stdio.h>
#include "spdm3.h"
#include "flush.h"

#define IT int
#define VT double

using namespace spdm3;

int main(int argc, char *argv[]) {
  int n = read_int(argc, argv, "-n", 20000);
  int ca = read_int(argc, argv, "-ca", 1);
  int cb = read_int(argc, argv, "-cb", 1);
  int tests = read_int(argc, argv, "-t", 3);
  char *input = read_string(argc, argv, "-i", (char *) NULL);
  
  double percent_r = read_double(argc, argv, "-pr", 5.0);
  double percent_nnz = read_double(argc, argv, "-pnnz", 5.0);
  bool stats = (find_option(argc, argv, "--stats") > 0);
  int r = n * percent_r / 100.0;
  Timer T;
  
  // Initializations.
  MPI_Init(&argc, &argv);
  T.Init(MPI_COMM_WORLD);
  init_dummy();
  
  Comm *commO  = GetComms(MPI_COMM_WORLD, ONED_BLOCK_ROW, ca);
  Comm *commXT = GetComms(MPI_COMM_WORLD, ONED_BLOCK_ROW, cb);
  Comm *commX  = GetComms(MPI_COMM_WORLD, ONED_BLOCK_COLUMN, cb);
  
  DistDMat<IT, VT> X(commX, DENSE_ROWMAJOR);
  DistDMat<IT, VT> XT(commXT, DENSE_ROWMAJOR);
  
  if (input == NULL) {
    XT.GenerateWithFunction(n, r);
  } else {
    XT.LoadNumPyTranspose(input);
    r = XT.cols();
    n = XT.rows();
    percent_r = (VT) r / n * 100.0;
  }
  X.Transpose(XT);
  
  DistSpMat<IT, VT> O(commO);
  O.GenerateUniform2(n, n, percent_nnz / 100.0);
  
  DistDMat<IT, VT> Y(commO, DENSE_ROWMAJOR);
  DistDMat<IT, VT> Z(commO, DENSE_ROWMAJOR);
  Y.Fill(XT.rows(), XT.cols(), 0.0);
  Z.Fill(XT.rows(), X.cols(), 0.0);
  
  SpMat<IT, VT> *Omega = GetWeirdLayoutRotatingBSpMats(O, X);
  
  // For stats.
  if (stats) {
    T.Start("Stats");
    int per_proc = X.nprocs() / ca / cb;
    int *counts = new int[per_proc * X.nprocs()];
    for (int i = 0; i < per_proc; ++i)
      counts[i] = Omega[i].nnz_;
    if (X.rank() != 0) {
      MPI_Gather(counts, per_proc, MPI_INT,
                 NULL, per_proc, MPI_INT, 0, X.comm_[WORLD].comm);
    } else {
      MPI_Gather(MPI_IN_PLACE, per_proc, MPI_INT,
                 counts, per_proc, MPI_INT, 0, X.comm_[WORLD].comm);
      
      char name[100];
      sprintf(name, "two-%d-%d-%d", X.nprocs(), ca, cb);
      FILE *f = fopen(name, "w");
      for (int i = 0; i < X.nprocs(); ++i) {
        for (int j = 0; j < per_proc; ++j)
          fprintf(f, "%d ", counts[i * per_proc + j]);
        fprintf(f, "\n");
      }
      fclose(f);
    }
    delete [] counts;
    T.Stop("Stats");
  }
  
  for (int test = 0; test < tests; ++test) {
    flush_cache();
    MPI_Barrier(MPI_COMM_WORLD);
    T.Start("oxT");
    allblkrow_shiftc(Omega, XT, Y, T);
    T.Stop("oxT");
    
    flush_cache();
    MPI_Barrier(MPI_COMM_WORLD);
    T.Start("YX");
    inner_shiftc_rotatingB(Y, X, Z, T);
    T.Stop("YX");

    // Sanity check.
    std::function<VT(VT)> square = [](VT x)->VT { return x * x; };
    VT partial_trace = Y.LocalTeamOpThenReduce(square, SPDM3_SUM);
    MPI_Allreduce(MPI_IN_PLACE, &partial_trace, 1, X.mpi_vt_, MPI_SUM, MPI_COMM_WORLD);
    T.printr("Round %d: trace = %g\n", test, partial_trace);
  }

  T.printr("%s: n = %d, percent_r = %f, percent_nnz = %f, "
           "p = %d, ca = %d, cb = %d, tests = %d, input = %s, nnz = %d\n",
            argv[0], n, percent_r, percent_nnz,
            X.nprocs(), ca, cb, tests, input, O.nnz());
  T.Report();
  
  // Finalizations.
  clear_dummy();
  delete [] Omega;
  delete [] commO;
  delete [] commX;
  delete [] commXT;
  MPI_Finalize();
  
  return 0;
}