#include <omp.h>
#include <stdio.h>
#include "spdm3.h"
#include "flush.h"

#define IT  int
#define VT  double

using namespace spdm3;

int main(int argc, char *argv[]) {
  int m = read_int(argc, argv, "-m", 1000);
  int n = read_int(argc, argv, "-n", 1000);
  int k = read_int(argc, argv, "-k", 1000);
  int tests = read_int(argc, argv, "-t", 3);
  Timer T;
  
  // Initializations.
  MPI_Init(&argc, &argv);
  T.Init(MPI_COMM_WORLD);
  init_dummy();
  
  DMat<IT, VT> A(m, k, DENSE_ROWMAJOR);
  DMat<IT, VT> B(k, n, DENSE_ROWMAJOR);
  DMat<IT, VT> C(m, n, DENSE_ROWMAJOR);
  
  A.Generate();
  B.Generate();
  
  // Tests.
  for (int test = 0; test < tests; ++test) {
    C.Fill(0.0);
  
    flush_cache();
    T.Start("Multiply");
    A.MultiplyAdd(B, C);
    T.Stop("Multiply");
  }
  
  // Gets number of threads to report.
  int threads;
  #pragma omp parallel 
  {
    #pragma omp single
    threads = omp_get_num_threads();
  }
  
  printf("%s: m = %d, n = %d, k = %d, threads = %d, trials = %d, time per trial = %lf\n",
         argv[0], m, n, k, threads, tests, T.GetCounter("Multiply") / (double) tests);
  
  // Finalizations.
  clear_dummy();
  MPI_Finalize();
  
  return 0;
}
