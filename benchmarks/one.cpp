#include <stdio.h>
#include "spdm3.h"
#include "flush.h"

#define IT  int
#define VT  double

using namespace spdm3;

int main(int argc, char *argv[]) {
  int n = read_int(argc, argv, "-n", 20000);
  int c = read_int(argc, argv, "-c", 1);
  int tests = read_int(argc, argv, "-t", 3);
  char *input = read_string(argc, argv, "-i", (char *) NULL);
  
  double percent_r = read_double(argc, argv, "-pr", 5.0);
  double percent_nnz = read_double(argc, argv, "-pnnz", 5.0);
  bool stats = (find_option(argc, argv, "--stats") > 0);
  int r = n * percent_r / 100.0;
  Timer T;
  
  // Initializations.
  MPI_Init(&argc, &argv);
  T.Init(MPI_COMM_WORLD);
  init_dummy();
  
  Comm *commA = GetComms(MPI_COMM_WORLD, ONED_BLOCK_ROW, c);
  Comm *commB = GetComms(MPI_COMM_WORLD, ONED_BLOCK_COLUMN, c);
  
  DistDMat<IT, VT> X(commB, DENSE_ROWMAJOR);
  DistDMat<IT, VT> XT(commA, DENSE_ROWMAJOR);
  
  if (input == NULL) {
    XT.GenerateWithFunction(n, r);
  } else {
    XT.LoadNumPyTranspose(input);
    r = XT.cols();
    n = XT.rows();
    percent_r = (VT) r / n * 100.0;
  }
  X.Transpose(XT);

  DistSpMat<IT, VT> O(commA);
  O.GenerateUniform2(n, n, percent_nnz / 100.0);
  
  // For stats.
  if (stats) {
    T.Start("Stats");
    if (O.comm_[LAYER].id == 0) {
      int *counts = new int[O.teams()];
      counts[0] = O.mat_.nnz_;
      if (O.comm_[LAYER].rank != 0) {
        MPI_Gather(counts, 1, MPI_INT,
                   NULL, 1, MPI_INT, 0, O.comm_[LAYER].comm);
      } else {
        MPI_Gather(MPI_IN_PLACE, 1, MPI_INT,
                   counts, 1, MPI_INT, 0, O.comm_[LAYER].comm);
        char name[100];
        sprintf(name, "one-%d-%d", O.nprocs(), c);
        FILE *f = fopen(name, "w");
        for (int i = 0; i < O.teams(); ++i)
          fprintf(f, "%d ", counts[i]);
        fprintf(f, "\n");
        fclose(f);
      }
      delete [] counts;
    }
    T.Stop("Stats");
  }
  
  for (int test = 0; test < tests; ++test) {
    DistDMat<IT, VT> S(commB, DENSE_ROWMAJOR);
    DistDMat<IT, VT> OS(commB, DENSE_ROWMAJOR);
    O.ResetShift(LAYER_COL);
  
    flush_cache();
    MPI_Barrier(MPI_COMM_WORLD);
    T.Start("xTx");
    innerABC_bcast(XT, X, S, T);
    T.Stop("xTx");
    
    flush_cache();
    MPI_Barrier(MPI_COMM_WORLD);
    T.Start("OS");
    VT partial_trace = innerABC_shiftc_w_trace<IT, VT>(O, S, OS, T);
    T.Stop("OS");
    
    // Sanity check.
    MPI_Allreduce(MPI_IN_PLACE, &partial_trace, 1, X.mpi_vt_, MPI_SUM, MPI_COMM_WORLD);
    T.printr("Round %d: trace = %g\n", test, partial_trace);
  }
  
  T.printr("%s: n = %d, percent_r = %f, percent_nnz = %f, "
           "p = %d, ca = %d, cb = %d, tests = %d, input = %s, nnz = %d\n",
           argv[0], n, percent_r, percent_nnz,
           X.nprocs(), c, c, tests, input, O.nnz());
  
  T.Report();
  
  // Finalizations.
  clear_dummy();
  delete [] commA;
  delete [] commB;
  MPI_Finalize();
  
  return 0;
}