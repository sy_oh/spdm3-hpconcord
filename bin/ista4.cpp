#include <cstdio>
#include <cmath>

#include "spdm3.h"

#define IT  int
#define VT  CMDVT

using namespace spdm3;

Timer T;
IT inner_loop = 0;
IT outer_loop = 0;

// Inputs:  Omega, S (both normal layout).
// Outputs: OS (weird layout), h1 (scalar).
// Layout: Everything in normal layout.
VT compute_h1(DistSpMat<IT, VT> &Omega, DistDMat<IT, VT> &S,
              DistDMat<IT, VT> &OS, const VT lambda2) {
  T.Start("Compute h1");
  DistSpMat<IT, VT> D(Omega.comm_, Omega.mat_.format_);
  D.Diagonal(Omega);
  D.ElmtWiseOp(log);
  VT partial_log_det = D.mat_.Reduce(SPDM3_SUM);
  VT partial_trace = innerABC_shiftc_w_trace<IT, VT>(Omega, S, OS, T);
  
  VT partial_frobenius_sqr = Omega.mat_.FrobeniusNormSquare();

  // Gets total det and trace.
  VT array[3] = {partial_log_det, partial_trace, partial_frobenius_sqr};
  T.Start("h1: Allreduce Layer");
  MPI_Allreduce(MPI_IN_PLACE, array, 3, D.mpi_vt_, MPI_SUM, D.comm_[LAYER].comm);
  T.Stop("h1: Allreduce Layer");
  T.Start("h1: Allreduce Team");
  MPI_Allreduce(MPI_IN_PLACE, array+1, 1, D.mpi_vt_, MPI_SUM, D.comm_[TEAM].comm);
  T.Stop("h1: Allreduce Team");
  
  VT h1 = -array[0] + 0.5 * array[1] + lambda2 * array[2];
  T.Stop("Compute h1");
  
  return h1;
}

// Inputs:  Omega (weird layout), OS (weird layout).
// Outputs: gh1 (weird layout).
void compute_gh1(DistSpMat<IT, VT> &Omega, DistDMat<IT, VT> &OS,
                 DistDMat<IT, VT> &gh1, const VT lambda2) {
  T.Start("Compute gh1");
  DistDMat<IT, VT> gh1_bcol(OS.comm_, OS.mat_.format_);
  innerABC_shiftc_transpose<IT, VT>(OS, gh1_bcol, T);  // gh1 = (OS)^T = SO.
  gh1_bcol.ElmtWiseOp([](VT a, VT b)->VT { return (a + b) / 2.0; }, OS);
  
  // Changes to block row layout. This only costs local transpose.
  gh1.Transpose(gh1_bcol);
  
  const VT twol2 = 2.0 * lambda2;
  DistSpMat<IT, VT> d(Omega.comm_, Omega.mat_.format_);
  d.WeirdLayoutDiagonalTransposed(Omega);
  gh1.ElmtWiseOpNzOnly([](VT a, VT b)->VT { return a - (1.0 / b); }, d);
  gh1.ElmtWiseOpNzOnly([twol2](VT a, VT b)->VT { return a + (twol2 * b); }, Omega);
  T.Stop("Compute gh1");
}

// Inputs:  Omega_old, gh1, tau, lambda1.
// Outputs: Omega.
void compute_Omega(DistSpMat<IT, VT> &Omega_old, DistDMat<IT, VT> &gh1,
                   VT tau, DistDMat<IT, VT> &Lambda1, DistSpMat<IT, VT> &Omega,
                   DistSpMat<IT, VT> &Weird) {
  T.Start("Compute Omega");
  DistDMat<IT, VT> res(Omega_old.comm_, gh1.mat_.format_);
  res.StoreElmtWise([tau](VT a, VT b)->VT { return b - tau * a; }, gh1, Omega_old);
  
  // Soft-thresholding.
  res.ElmtWiseOp([tau](VT a, VT thres)->VT {
                   VT sign = (a > 0) - (a < 0);
                   return sign * std::max(fabs(a) - (tau * thres), 0.0);
                 }, Lambda1);
  
  innerABC_shiftc_reform_sparse_from_blockRow(res, Omega, T);
  Weird.Convert(res);
  T.Stop("Compute Omega");
}

// Inputs:  Omega, Omega_old, gh1, h1_old, tau.
// Outputs: Q, maxdiff.
VT compute_Q(DistSpMat<IT, VT> &Omega, DistSpMat<IT, VT> &Omega_old,
             DistDMat<IT, VT> &gh1, VT h1_old, VT tau, VT &maxdiff) {
  T.Start("Compute Q");
  T.Start("Q: Diagonal");
  DistSpMat<IT, VT> D(Omega.comm_, Omega.mat_.format_);
  D.Add(-1.0, Omega_old, 1.0, Omega);
  maxdiff = D.mat_.Reduce(SPDM3_MAX_ABS);
  T.Stop("Q: Diagonal");
  
  VT frobenius = D.mat_.FrobeniusNormSquare();
  frobenius /= 2.0 * tau;
  
  T.Start("Q: DotMultiplyI");
  D.DotMultiplyI(gh1);
  VT trace = D.mat_.Reduce(SPDM3_SUM);
  T.Stop("Q: DotMultiplyI");
  
  VT array[2] = {trace, frobenius};
  T.Start("Q: Allreduce");
  MPI_Allreduce(MPI_IN_PLACE, array, 2, D.mpi_vt_, MPI_SUM, D.comm_[WORLD].comm);
  T.Stop("Q: Allreduce");
  VT Q = h1_old + array[0] + array[1];
  T.Stop("Compute Q");
  
  return Q;
}

int main(int argc, char *argv[]) {
  MPI_Init(&argc, &argv);
  T.Init(MPI_COMM_WORLD);
  
  // Parses parameters.
  char *input  = read_string(argc, argv, "-i", (char *) "xf.npy");
  char *output = read_string(argc, argv, "-o", NULL);
  VT lambda1 = read_double(argc, argv, "-l1", 0.3);  // 0.1
  VT lambda2 = read_double(argc, argv, "-l2", 0.2);  // 0.2
  VT tau0 = read_double(argc, argv, "-tau", 1.0);
  VT epsilon = read_double(argc, argv, "-eps", 1.0e-5);
  int layers = read_int(argc, argv, "-c", 1);
  int max_inner_iter = read_int(argc, argv, "-max_inner", 20);
  int max_outer_iter = read_int(argc, argv, "-max_outer", 100);
  
  // Starts timer.
  T.Start("Overall");
  
  //
  // Initialization.
  //
  T.Start("Input");
  Comm *blockCol = GetComms(MPI_COMM_WORLD, ONED_BLOCK_COLUMN, 1);
  Comm *blockRow = GetComms(MPI_COMM_WORLD, ONED_BLOCK_ROW, 1);
  Comm *blockColC = GetComms(MPI_COMM_WORLD, ONED_BLOCK_COLUMN, layers);
  Comm *blockRowC = GetComms(MPI_COMM_WORLD, ONED_BLOCK_ROW, layers);
  
  DistDMat<IT, VT> Temp(blockRow, DENSE_ROWMAJOR);
  DistDMat<IT, VT> S(blockColC, DENSE_ROWMAJOR);
  Temp.LoadNumPyTranspose(input);
  Temp.Replicate(blockRowC);
  S.Transpose(Temp);
  T.Stop("Input");
  
  DistDMat<IT, VT> OS(blockColC, DENSE_ROWMAJOR);
  DistDMat<IT, VT> gh1(blockRowC, DENSE_ROWMAJOR);
  
  /*
  T.Start("Compute S");
  innerABC_bcast(XT, X, S, T);
  S.ElmtWiseOp([](VT a, VT b)->VT { return a / b; }, (VT) XT.cols());
  T.Stop("Compute S");
  */

  T.Start("Set Identity");
  DistSpMat<IT, VT> Omega(blockRowC, SPARSE_CSR), OW(blockRowC, SPARSE_CSR);
  DistSpMat<IT, VT> Omega2(blockRowC, SPARSE_CSR), OW2(blockRowC, SPARSE_CSR);
  Omega.SetIdentity(S.rows());
  OW.WeirdLayoutBlockRowTransposed(Omega);
  T.Stop("Set Identity");

  T.Start("Prepare Lambda1 matrix");
  // All lambda1's except with 0 diagonal elements.
  DistDMat<IT, VT> Lambda1(blockRowC, DENSE_ROWMAJOR);
  Lambda1.Fill(Omega.rows(), Omega.cols(), lambda1);
  Lambda1.ElmtWiseOpNzOnly([](VT a, VT b)->VT { return 0.0; }, Omega);
  DistDMat<IT, VT> WeirdL1(blockRowC, DENSE_ROWMAJOR);
  WeirdL1.WeirdLayoutBlockRowTransposed(Lambda1);
  T.Stop("Prepare Lambda1 matrix");
  
  //
  // CONCORD-ISTA.
  //
  VT nnz_ratio, sum_nnz_ratio, avg_nnz_ratio;
  IT iter_count = 0;
  VT h1, h1_old, Q, maxdiff;
  DistSpMat<IT, VT> *Oold = &Omega, *Wold = &OW;
  DistSpMat<IT, VT> *O = &Omega2, *W = &OW2;
  h1_old = compute_h1(*Oold, S, OS, lambda2);
  O->printr("h1 = %10.4lf, time: %10.4lf, %%nnz: %10.7lf\n", h1_old, T.GetElapsedTime(), O->nnz_ratio() * 100.0);
  outer_loop = 0;

  IT diagitr = 0;
  while (true) {
    compute_gh1(*Wold, OS, gh1, lambda2);

    diagitr = 0;
    inner_loop = 0;
    for (VT tau = tau0; true; tau /= 2.0) {
      compute_Omega(*Wold, gh1, tau, WeirdL1, *O, *W);
      T.Start("Make diagonal");
      DistSpMat<IT, VT> D(O->comm_);
      D.Diagonal(*O);
      T.Stop("Make diagonal");
      T.Start("Find local min");
      VT min_elmt = D.mat_.Reduce(SPDM3_MIN);
      T.Stop("Find local min");
      T.Start("Allreduce min_elmt");
      MPI_Allreduce(MPI_IN_PLACE, &min_elmt, 1, D.mpi_vt_, MPI_MIN, D.comm_[WORLD].comm);
      T.Stop("Allreduce min_elmt");
      if (min_elmt < 1e-08 && diagitr < 10) {
        ++diagitr;
        continue;
      } else if (min_elmt < 0) {
        T.printr("Error! Minimum element = %lf\n", min_elmt);
        exit(1);
      }
      ++iter_count;
      T.Start("Get nnz");
      LT nnz = O->nnz();
      nnz_ratio = (VT) nnz / O->rows() / O->cols() * 100.0;
      sum_nnz_ratio += nnz_ratio;
      T.Stop("Get nnz");
      h1 = compute_h1(*O, S, OS, lambda2);
      Q = compute_Q(*W, *Wold, gh1, h1_old, tau, maxdiff);
      O->printr("Round %03d.%02d [%10.4lf]:     tau = %10.4lf, h1 = %10.4lf, Q = %10.4lf, %%nnz = %9.6lf, nnz = %10.0lf\n",
          T.GetElapsedTime(), outer_loop, inner_loop, tau, h1, Q, nnz_ratio, (VT) nnz);
      ++inner_loop;
      
      if (h1 <= Q || inner_loop >= max_inner_iter)
        break;
    }
  
    T.Start("Allreduce maxdiff");
    MPI_Allreduce(MPI_IN_PLACE, &maxdiff, 1, O->mpi_vt_, MPI_MAX, MPI_COMM_WORLD);
    T.Stop("Allreduce maxdiff");
    ++outer_loop;
    T.Start("Allreduce nnz ratio");
    nnz_ratio = O->nnz_ratio() * 100.0;
    T.Stop("Allreduce nnz ratio");
    O->printr("time: %10.4lf, %%nnz: %10.7lf, maxdiff: %g\n",
        T.GetElapsedTime(), nnz_ratio, maxdiff);
    if (maxdiff < epsilon || outer_loop >= max_outer_iter)
      break;
    
    std::swap(h1, h1_old);
    std::swap(O, Oold);
    std::swap(W, Wold);
  }

  T.Start("Cleaning up");
  // Save output.
  if (output != NULL)
    Omega.SaveMatrixMarket(output);
  
  avg_nnz_ratio = sum_nnz_ratio / iter_count;
  if (blockRowC[WORLD].rank == 0) {
    printf("%s: input = %s, S = %d x %d, output = %s, l1 = %lf, l2 = %lf\n"
        "p = %d, c = %d, final nnz ratio = %lf%%, avg nnz ratio = %lf%%, min_diag_elmt = %g\n",
        argv[0], input, S.rows(), S.cols(), output, lambda1, lambda2,
        blockRowC[WORLD].size, blockRowC[TEAM].size, nnz_ratio, avg_nnz_ratio, min_elmt);
  }
  
  // Deallocation.
  delete [] blockCol;
  delete [] blockRow;
  delete [] blockColC;
  delete [] blockRowC;
  T.Stop("Cleaning up");
  
  // Finalization.
  T.Stop("Overall");
  T.Report();
  MPI_Finalize();
  
  return 0;
}
