#include <cstdio>
#include <cmath>
#include <limits>

#include "spdm3.h"

#define IT  int
#define VT  CMDVT

using namespace spdm3;

Timer T;
IT inner_loop = 0;
IT outer_loop = 0;
TransposeInfo info;

//
// Inputs: Omega_old (block row, weird layout),
//         XT (block row),
//         lambda2 (scalar);
// Outputs: Y (block row),
//         partial_h1 (scalar).
//
VT compute_h1(SpMat<IT, VT> *Omega,
              DistDMat<IT, VT> &XT, DistDMat<IT, VT> &Y, const VT lambda2) {
  T.Start("Compute h1");
  // Computes Y = Omega * XT
  allblkrow_shiftc(Omega, XT, Y, T);
  
  // partial_h1 = -log det diag(Omega) +
  //              Frobenius(Y)^2 + lambda2 * Frobenius(Omega)^2.
  T.Start("Compute h1: the rest");
  std::function<VT(VT)> square = [](VT x)->VT { return x * x; };
  std::function<VT(VT, VT)> logdet =
      [](VT accu, VT operand)->VT { return accu + log(operand); };
  VT partial_neg_log_det_diag =
      ReduceDiagonalWeirdLayoutRotatingB(logdet, 0.0, Omega, info);
  VT partial_Omega_frobenius_sqr = 0.0;
  for (int i = 0; i < info.num_grids_in_1D; ++i) {
    partial_Omega_frobenius_sqr += Omega[i].FrobeniusNormSquare();
  }
  
  VT partial_h1 = -partial_neg_log_det_diag +
                  0.5 * Y.LocalTeamOpThenReduce(square, SPDM3_SUM) / XT.cols() +
                  lambda2 * partial_Omega_frobenius_sqr;
  T.Stop("Compute h1: the rest");
  T.Stop("Compute h1");
  return partial_h1;
}

//
// Inputs: Omega_old (block row, weird layout),
//         Y (block row),
//         X (block column);
// Output: G (block row, weird layout).
//
void compute_gh1(SpMat<IT, VT> *Omega, DistDMat<IT, VT> &Y, DistDMat<IT, VT> &X,
                 DistDMat<IT, VT> &Z, DMat<IT, VT> *G, DistDMat<IT, VT> &ZT,
                 const VT lambda2) {
  T.Start("Compute gh1");
  // Z = YX.
  // G is the DMats version of Z (points to Z's buffer).
  inner_shiftc_rotatingB(Y, X, Z, T);
  inner_shiftc_rotatingB_transpose(Z, ZT, info, T);
  
  T.Start("Compute gh1: the rest");
  const VT twon = 2.0 * X.rows();
  Z.ElmtWiseOp([twon](VT a, VT b)->VT {return (a + b) / twon; }, ZT);
  
  // partial_gh1 = -(diag(Omega)^-1) +
  //               1/2n * (Z + Z^T) + 2 * lambda2 * Omega.
  OpDiagonalWeirdLayoutRotatingB((std::function<VT(VT, VT)>)
                                 [](VT a, VT b)->VT {
                                   return a - (1.0 / b);
                                 }, G, Omega, info);
  
  const VT twol2 = 2.0 * lambda2;
  for (int i = 0; i < info.num_grids_in_1D; ++i) {
    G[i].ElmtWiseOpNzOnly([twol2](VT a, VT b)->VT { return a + (twol2 * b); }, Omega[i]);
  }
  T.Stop("Compute gh1: the rest");
  T.Stop("Compute gh1");
}

//
// Inputs: Omega_old, G, Lambda1 (block row, weird layout),
//         tau (scalar);
// Output: Omega (block row, weird layout).
//
void compute_Omega(SpMat<IT, VT> *Omega_old, DMat<IT, VT> *G,
                   DMat<IT, VT> *Lambda1, const VT tau,
                   SpMat<IT, VT> *Omega) {
  T.Start("Compute Omega");
  // Omega = soft_thresholding(Omega_old - tau * G, tau * Lambda1).
  for (int i = 0; i < info.num_grids_in_1D; ++i) {
    DMat<IT, VT> res;
    T.Start("Compute Omega: math");
    res.StoreElmtWise([tau](VT a, VT b)->VT { return b - tau * a; }, G[i], Omega_old[i]);
    T.Stop("Compute Omega: math");
  
    // Soft-thresholding.
    T.Start("Compute Omega: thresholding");
    res.ElmtWiseOp([tau](VT a, VT thres)->VT {
                   VT sign = (a > 0) - (a < 0);
                   return sign * std::max(fabs(a) - (tau * thres), 0.0);
                 }, Lambda1[i]);
    T.Stop("Compute Omega: thresholding");
    
    // Stores in sparse format.
    T.Start("Compute Omega: convert");
    Omega[i].Convert(res);
    T.Stop("Compute Omega: convert");
  }
  T.Stop("Compute Omega");
}

//
// Inputs: Omega_old, Omega, G (block row, weird layout),
//         tau (scalar);
// Output: partial_Q.
//
VT compute_Q(SpMat<IT, VT> *Omega_old, SpMat<IT, VT> *Omega,
             DMat<IT, VT> *G, const VT tau, VT &partial_maxdiff) {
  T.Start("Compute Q");
  // D = Omega - Omega_old;
  VT D_dot_G = 0.0, Frobenius_D = 0.0;
  SpMat<IT, VT> *D = new SpMat<IT, VT>[info.num_grids_in_1D];
  partial_maxdiff = 0.0;
  for (int i = 0; i < info.num_grids_in_1D; ++i) {
    D[i].Add(-1.0, Omega_old[i], 1.0, Omega[i]);
    partial_maxdiff = std::max(partial_maxdiff, D[i].Reduce(SPDM3_MAX_ABS));
    
    // Frobenius norm square.
    Frobenius_D += D[i].FrobeniusNormSquare();
    
    // Trace.
    D[i].DotMultiplyI(G[i]);
    D_dot_G += D[i].Reduce(SPDM3_SUM);
  }
  delete [] D;
  
  // partial_q = D.*G + 1/(2*tau) * Frobenius(D)^2.
  VT partial_q = D_dot_G + Frobenius_D / (2.0 * tau);
  T.Stop("Compute Q");
  
  return partial_q;
}

int main(int argc, char *argv[]) {
  MPI_Init(&argc, &argv);
  T.Init(MPI_COMM_WORLD);
  
  // Parses parameters.
  char *input  = read_string(argc, argv, "-i", (char *) "xf.npy");
  char *output = read_string(argc, argv, "-o", NULL);
  VT lambda1 = read_double(argc, argv, "-l1", 0.3);  // 0.1
  VT lambda2 = read_double(argc, argv, "-l2", 0.2);  // 0.2
  VT tau0 = read_double(argc, argv, "-tau", 1.0);
  VT epsilon = read_double(argc, argv, "-eps", 1.0e-5);
  int layers_row = read_int(argc, argv, "-crow", 1);
  int layers_col = read_int(argc, argv, "-ccol", 1);
  int max_inner_iter = read_int(argc, argv, "-max_inner", 20);
  int max_outer_iter = read_int(argc, argv, "-max_outer", 100);
  
  // Starts timer.
  T.Start("Overall");
  
  // Initialization.
  T.Start("Input");
  Comm *blockCol = GetComms(MPI_COMM_WORLD, ONED_BLOCK_COLUMN, 1);
  Comm *blockRow = GetComms(MPI_COMM_WORLD, ONED_BLOCK_ROW, 1);
  Comm *blockColC = GetComms(MPI_COMM_WORLD, ONED_BLOCK_COLUMN, layers_col);
  Comm *blockRowC = GetComms(MPI_COMM_WORLD, ONED_BLOCK_ROW, layers_row);
  Comm *blockRowCcol = GetComms(MPI_COMM_WORLD, ONED_BLOCK_ROW, layers_col);
  
  DistDMat<IT, VT> X(blockColC, DENSE_ROWMAJOR);
  DistDMat<IT, VT> XT(blockRow, DENSE_ROWMAJOR);
  T.Start("LoadNumPy");
  XT.LoadNumPyTranspose(input);
  T.Stop("LoadNumPy");
  T.Start("Replicate XT");
  XT.Replicate(blockRowCcol);
  T.Stop("Replicate XT");
  T.Start("Transpose XT");
  X.Transpose(XT);
  T.Stop("Transpose XT");
  T.Stop("Input");
  
  T.Start("Identity and Lambda1");
  DistSpMat<IT, VT> I(blockRowC, SPARSE_CSR);
  I.SetIdentity(XT.rows());
  
  DistDMat<IT, VT> Lambda1(blockRowC, DENSE_ROWMAJOR);
  // All lambda1s except with 0 diagonal elements.
  Lambda1.Fill(XT.rows(), X.cols(), lambda1);
  createTransposeInfo_inner_shiftc_rotatingB(Lambda1, X, info, T);
  
  // Omega_old is now a lot of spmats.
  SpMat<IT, VT> *Omega_old = GetWeirdLayoutRotatingBSpMats(I, X);
  SpMat<IT, VT> *Omega = new SpMat<IT, VT>[info.num_grids_in_1D];  // Must createTransposeInfo before this line.
  
  DMat<IT, VT> *L1 = InnerShiftCRotatingBShallowDMats(Lambda1, X);
  OpDiagonalWeirdLayoutRotatingB((std::function<VT(VT, VT)>)
                                 [](VT a, VT b)->VT { return 0.0; },
                                 L1, Omega_old, info);
  T.Stop("Identity and Lambda1");

  // Prepares Y, Z, ZT, G.
  DistDMat<IT, VT> Y(blockRowC, DENSE_ROWMAJOR);
  DistDMat<IT, VT> Z(blockRowC, DENSE_ROWMAJOR), ZT(blockRowC, DENSE_ROWMAJOR);
  Y.Fill(XT.rows(), XT.cols(), 0.0);
  Z.Fill(XT.rows(), X.cols(), 0.0);
  ZT.Fill(XT.rows(), X.cols(), 0.0);
  DMat<IT, VT> *G = InnerShiftCRotatingBShallowDMats(Z, X);

  //
  // CONCORD-ISTA
  //
  VT h1, h1_old, Q, partial_h1, partial_q, partial_maxdiff, maxdiff;
  VT nnz_ratio, sum_nnz_ratio = 0.0, avg_nnz_ratio;
  partial_h1 = compute_h1(Omega_old, XT, Y, lambda2);
  MPI_Allreduce(&partial_h1, &h1_old, 1, X.mpi_vt_, MPI_SUM, X.comm_[WORLD].comm);
  X.printr("h1_old = %lf\n", h1_old);
  
  std::function<VT(VT, VT)> findmin =
      [](VT minimum, VT operand)->VT { return std::min(minimum, operand); };
  
  IT diagitr = 0, iter_count = 0;
  while (true) {
    compute_gh1(Omega_old, Y, X, Z, G, ZT, lambda2);
    
    diagitr = 0;
    inner_loop = 0;
    for (VT tau = tau0; true; tau /= 2.0) {
      compute_Omega(Omega_old, G, L1, tau, Omega);
      
      T.Start("Find min elmt");
      VT min_elmt = std::numeric_limits<VT>::max();
      #pragma omp parallel for reduction(min : min_elmt)
      for (int i = 0; i < info.num_grids_in_1D; ++i) {
        min_elmt = std::min(min_elmt,
                            ReduceDiagonalWeirdLayoutRotatingB_min(findmin, std::numeric_limits<VT>::max(),
                                                                   Omega, info));
      }
      MPI_Allreduce(MPI_IN_PLACE, &min_elmt, 1, X.mpi_vt_, MPI_MIN, X.comm_[WORLD].comm);
      T.Stop("Find min elmt");
      if (min_elmt < 1e-08 && diagitr < 10) {
        ++diagitr;
        continue;
      }
      
      partial_h1 = compute_h1(Omega, XT, Y, lambda2);
      partial_q = compute_Q(Omega_old, Omega, G, tau, partial_maxdiff);
      
      T.Start("Allreduce at the end");
      LT partial_nnz_count = 0;
      #pragma omp parallel for reduction(+ : partial_nnz_count)
      for (int i = 0; i < info.num_grids_in_1D; ++i)
        partial_nnz_count += Omega[i].nnz_;
      
      VT temp[3] = {partial_h1, partial_q, (VT) partial_nnz_count};
      MPI_Allreduce(MPI_IN_PLACE, temp, 3, X.mpi_vt_, MPI_SUM, X.comm_[WORLD].comm);
      h1 = temp[0];
      Q = temp[1] + h1_old;
      nnz_ratio = temp[2] * 100.0 / XT.rows() / X.cols();
      sum_nnz_ratio += nnz_ratio;
      T.Stop("Allreduce at the end");
      
      X.printr("Round %03d.%02d [%10.4lf]: tau = %10.4lf, h1 = %10.4lf, Q = %10.4lf, %%nnz = %9.6lf\n",
          T.GetElapsedTime(), outer_loop, inner_loop, tau, h1, Q, nnz_ratio);
      
      ++inner_loop;
      ++iter_count;
      if (h1 <= Q || inner_loop >= max_inner_iter)
        break;
    }
    ++outer_loop;
    
    T.Start("Allreduce maxdiff");
    MPI_Allreduce(&partial_maxdiff, &maxdiff, 1, X.mpi_vt_, MPI_MAX, X.comm_[WORLD].comm);
    X.printr("maxdiff %g\n", maxdiff);
    T.Stop("Allreduce maxdiff");
    
    if (maxdiff < epsilon || outer_loop >= max_outer_iter)
      break;
    
    std::swap(h1, h1_old);
    std::swap(Omega, Omega_old);
  }
  
  // TODO(penpornk): Save output.
  
  avg_nnz_ratio = sum_nnz_ratio / iter_count;
  if (blockRowC[WORLD].rank == 0) {
    printf("%s: input = %s, X = %d x %d, output = %s, l1 = %lf, l2 = %lf, max inner = %d, max outer = %d\n"
        "p = %d, crow = %d, ccol = %d, final nnz ratio = %lf%%, avg nnz ratio = %lf%%\n",
        argv[0], input, X.rows(), X.cols(), output, lambda1, lambda2, max_inner_iter, max_outer_iter,
        blockRowC[WORLD].size, layers_row, layers_col, nnz_ratio, avg_nnz_ratio);
  }
  
  // Deallocation.
  delete [] blockCol;
  delete [] blockRow;
  delete [] blockColC;
  delete [] blockRowC;
  delete [] blockRowCcol;
  delete [] L1;
  FREE_IF_NOT_NULL(Omega);
  FREE_IF_NOT_NULL(Omega_old);
  
  // Finalization.
  T.Stop("Overall");
  T.Report();
  info.destroy();
  MPI_Finalize();
  
  return 0;
}