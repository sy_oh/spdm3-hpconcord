# SpDM^3 and HP-CONCORD version 0.1 #
**SpDM^3:** Sparse-Dense Matrix-Matrix Multiplication at scale  
**HP-CONCORD:** High-performance inverse covariance matrix estimation using the CONCORD-ISTA algorithm.

## Dependencies ##
* C++, MPI, and OpenMP
* (Preferred) BLAS/MKL/ESSL library
* (Provided in the repo) graph500-1.2 (R-MAT graph generator) and SpBLAS.

## Installation ##
If you just want to use HP-CONCORD on Edison at NERSC, skip to the section 'Using the HP-CONCORD module on Edison at NERSC'. Otherwise, you need to compile SpDM^3/HP-CONCORD from the source files. Below is how to set up the environment.

* _git clone <bitbucket link>_
* _cd_ into the folder
* _cd configs_ and choose configuration file
    - **Edison with MKL:** _cp config.mk.edison config.mk_ 
    - **Ubuntu with BLAS:** _cp config.mk.ubuntu config.mk_ 
    - **Ubuntu with MKL:** _cp config.mk.ubuntu.mkl config.mk_ (If Intel MKL is not installed in /opt/intel, please update the INTELROOT variable in the file.)
    - **Mac with BLAS:** _cp config.mk.mac config.mk_ 
    - **Mira with ESSL:** _cp config.mk.mira config.mk_ 
    
The set up is complete. See the next two sections for separate instructions on how to use SpDM^3 and HP-CONCORD.

## How to use SpDM^3##
If you just wanted to use SpDM^3 for distributed sparse-dense matrix-matrix multiplication, see code examples in the _examples_ folder. Please also cite the SpDM^3 paper (see details in the Citation section).

## How to run HP-CONCORD ##
### Building HP-CONCORD ###
From the top directory, _cd bin_ and _make_ to get the executables _ista1_, _ista2_, and _ista5_.

### Using the HP-CONCORD module on NERSC machines ###
If you are on Edison or Cori at NERSC, you can use the custom HP-CONCORD module by running the commands

* module use /global/common/software/m88/<machine_name>/modulefiles
* module load hp-concord/0.1

where _machine_name_ can be _edison_ or _cori_. The module provides you _ista1_, _ista2_, and _ista5_.

### Input format ###
Matrix _X_ of _n_ observations (rows) of _p_ features (columns), stored in NumPy column-major format. Our code assumes _n_ << _p_.

### Which executable to run ###
Please refer to our PMLR'18 paper for algorithm names and notations.

* _ista1_ implements the Cov variant, taking in the observation matrix X as input.
* _ista2_ implements the Obs variant, taking in the obseravtion matrix X as input.
* _ista5_ implements the Cov variant, taking in the sample covariance matrix S as input.

If the expected number of nonzeroes per row is less than _n_, use _ista1_. Otherwise, use _ista2_. In general, _ista2_ outperforms _ista1_ so if you're not sure what to use, use _ista2_.

### Command-line parameters ###
* _-i_: NumPy's .npy input file name. 
    - Assumes column-major format where rows are observations and columns are features.
* _-o_: output file name. (Matrix Market format.)
* _-re_: initial guess for Omega (Matrix Market format.) (Can be used for restarting.)
* _-c_: replication factor (default is 1).
    - If you don't know what this is, it's safe to just ignore this option.
    - c <= sqrt(number of MPI processes)
* _-l1_: lambda1 (default is 0.3).
* _-l2_: lambda2 (default is 0.2).
* _-L1_: Lambda1 .npy input file name. 
    - Takes L1 as an input matrix. Overrides -l1.
    - Row-major. (fortran_order = false)
* _-tau_: starting tau (default is 1.0).
* _-eps_: epsilon (default is 1.0e-5).
* _-max\_inner_: maximum inner iterations (default is 20).
* _-max\_outer_: maximum outer iterations (default is 100).
* _-stop\_inner_: the inner iteration to stop when reaching the last outer iteration (default is -1 = unactivated). Useful for debugging.
* _-outer_offset_: start the outer iteration count from this number (Makes it less confusing when doing checkpoint-restarting)

### Examples ###
The following commands run spdm3-concord on 16 cores (16 MPI processes, 1 thread per process) with input named input.npy and output named out.csr, with replication factor c = 4.

#### Edison ####
> \# To use one MKL thread per MPI process.  
> export OMP_NUM_THREADS=1  
>
> srun -n 16 -c 1 ./ista2 -i input.npy -o out -c 4

#### Ubuntu/Mac ####
> mpirun -n 16 ./ista2 -i input.npy -o out -c 4

### Collecting the output Matrix Market file ###
For better I/O efficiency, each process saves their own part of Omega separately. 
For an output file name "out", process 0 will save "out-00000", process 1 will save "out-00001", and so on.
To get the final Matrix Market file, run
> cat <output_file_name>-* > <matrix_market_file_name>.mkt

For example, if "-o out" is used, the user should call,
> cat out-\* > out.mkt; rm out-\*

### Checkpoint-Restarting ###
The long runs can be split to multiple jobs by combining output writing option (-o) with initial guess option (-re).
The following command saves the output to out-* after 10 outer iterations.
> <launch command> ./ista5 -i input.npy -o out -max_outer 10  
> cat out-\* > out.mkt; rm out-\*

The following command restarts the process with the output as initial guess (= resuming starting from the 11th iteration).
The option -outer_offset tells the program to start the outer iteration counter from 10, 
effectively giving the same round numbers as the non-restarting version.
> <launch command> ./ista5 -i input.npy -re out.mkt -outer_offset 10

## Citation ##
If you use any part of this code, please cite the following paper:

* Penporn Koanantakool, Ariful Azad, Aydın Buluç, Dmitriy Morozov, Sang-Yun Oh, Leonid Oliker, and Katherine Yelick. **Communication-Avoiding Parallel Sparse-Dense Matrix-Matrix Multiplication.** In 2016 IEEE International Parallel and Distributed Processing Symposium (IPDPS), pages 842–853, May 2016.

If you use HP-CONCORD, please also cite the HP-CONCORD and the original CONCORD-ISTA papers:

* Penporn Koanantakool, Alnur Ali, Ariful Azad, Aydın Buluç, Dmitriy Morozov, Leonid Oliker, Katherine Yelick, and Sang-Yun Oh. **Communication-Avoiding Optimization Methods for Distributed Massive-Scale Sparse Inverse Covariance Estimation.** In Proceedings of the Twenty-First International Conference on Artificial Intelligence and Statistics, PMLR 84:1376-1386, 2018.
* Sang Oh, Onkar Dalal, Kshitij Khare, and Bala Rajaratnam. **Optimization methods for sparse pseudo-likelihood graphical model selection.** In NIPS 27, pages 667–675. 2014.

## Contact ##
penpornk _at_ eecs _dot_ berkeley _dot_ edu

## Copyright Notice ##
"HP-CONCORD" Copyright (c) 2017, The Regents of the University of California, through Lawrence Berkeley National Laboratory (subject to receipt of any required approvals from the U.S. Dept. of Energy).  All rights reserved.
 
If you have questions about your rights to use or distribute this software, please contact Berkeley Lab's Innovation & Partnerships Office at  IPO@lbl.gov.
 
NOTICE.  This Software was developed under funding from the U.S. Department of Energy and the U.S. Government consequently retains certain rights. As such, the U.S. Government has been granted for itself and others acting on its behalf a paid-up, nonexclusive, irrevocable, worldwide license in the Software to reproduce, distribute copies to the public, prepare derivative works, and perform publicly and display publicly, and to permit other to do so.
