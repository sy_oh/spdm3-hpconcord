import numpy as np
import sys

input = sys.argv[1]
output = sys.argv[2]
rows = int(sys.argv[3])
cols = int(sys.argv[4])
x = np.load(input)
print x.shape
x = x[np.ix_(range(0, rows), range(0, cols))]
print x.shape
np.save(output, x)
