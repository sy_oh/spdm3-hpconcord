#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <omp.h>
#include <random>
#include "spdm3.h"

using namespace spdm3;

#define IT  int
#define VT  double

bool isDigit(const char c) {
  if ('0' <= c && c <= '9')
    return true;
  return false;
}

int getID(const char *str) {
  printf("%s\n", str);
  int i = strlen(str)-1, id = 0;
  for (; str[i] != '/' && i > 0; --i);
  printf("i=%d, %s\n", i, str + i);
  while (!isDigit(str[i++]));
  i += 3;
  printf("%s\n", str + i);
  while (isDigit(str[i])) {
    id = (id * 10) + str[i] - '0';
    ++i;
  }
  printf("%s\n", str + i);
  i += 3;
  printf("%s\n", str + i);
  assert(isDigit(str[i]));
  id = (id * 10) + str[i] - '0';
  return id;
}

// Input:  File names of Ls (.mtx format) in argv
// Output: Block columns of X.
int main(int argc, char *argv[]) {
  int n = 100;
  double start_time = omp_get_wtime();
  for (int i = 1; i < argc; ++i) {
    // mkl_dcsrmm only handles sparse * dense not dense * sparse.
    // In order to compute X = ZL, we do X^T = L^T * Z^T (all rowmajor)
    // then just swap XT's rows_ and cols_ and save it normally
    // to get X in colmajor.
    SpMat<IT, VT> Lt;
    Lt.LoadMatrixMarket(argv[i], true);
    
    // Sets up the dense matrices.
    int seed = getID(argv[i]);
    DMat<IT, VT> Zt(Lt.rows_, n, DENSE_ROWMAJOR);
    printf("Before GenerateNormal\n");
    Zt.GenerateNormal(0.0, 1.0, seed);
    DMat<IT, VT> Xt(DENSE_ROWMAJOR);
    
    // Multiplication.
    printf("Before MultiplyAdd\n");
    Lt.MultiplyAdd(Zt, Xt);
    printf("After MultiplyAdd\n");
    
    // Make XT colmajor.
    char outfilename[100];
    sprintf(outfilename, "%d.npy", seed);
    Xt.TransposeBySwitchingMajor();
    printf("Before SaveNumPy\n");
    Xt.SaveNumPy(outfilename);
    double elapsed_time = omp_get_wtime() - start_time;
    printf("[%lf] %04d - %s\n", elapsed_time, i, outfilename);
    fflush(stdout);
  }
  return 0;
}
