#include <cstdio>
#include <cstdlib>
#include <omp.h>
#include <random>
#include "spdm3.h"

#define BRNG    VSL_BRNG_MCG31
#define METHOD  VSL_RNG_METHOD_GAUSSIAN_BOXMULLER

using namespace spdm3;

#define IT  int
#define VT  double

int main(int argc, char *argv[]) {
  int n = 100, p;
  p = atoi(argv[1]);
  printf("n, p = %d, %d\n", n, p);
  
  // Fills Z with random samples from N(0, 1).
  int seed = 11;
  double mean = 0.0, sigma = 1.0;
  DMat<IT, VT> Z(n, p, DENSE_ROWMAJOR);
  
  printf("Generating Z.\n");
#ifdef USE_MKL
  // Unfortunately std::normal_distribution compiled with
  // Intel compilers wouldn't work and just blocks.
  // So we have to use MKL's distribution generator instead.
  VSLStreamStatePtr stream;
  int offset = 0;
  int errcode = vslNewStream(&stream, BRNG, seed);
  time_t start_time = time(NULL);
  for (IT i = 0; i < Z.dim1(); ++i) {
    // TODO(penpornk): Use vsRngGaussian if VT is float.
    errcode = vdRngGaussian(METHOD, stream, Z.dim2(),
                            Z.values_ + offset, mean, sigma);
    offset += Z.lda_;
  }
#else
  #pragma omp parallel 
  {
    IT tid = omp_get_thread_num();
    std::mt19937 generator(seed+tid);
    std::normal_distribution<VT> distribution(mean, sigma);
    #pragma omp for
    for (IT i = 0; i < Z.dim1(); ++i) {
      for (IT k = 0; k < Z.dim2(); ++k) {
        Z.values_[i * Z.lda_ + k] = distribution(generator);
      }
    }
  }
#endif /* USE_MKL_ */
  printf("Zsum = %lf\n", Z.Reduce(SPDM3_SUM));
  
  // Generates L and calculates X a 100 columns at a time.
  // Assumes the number of columns is a multiple of 100.
  printf("Generating L.\n");
  int bj = atoi(argv[2]);
  assert(p % bj == 0);
  DMat<IT, VT> L(p, bj, DENSE_ROWMAJOR);
  DMat<IT, VT> X(n, p, DENSE_ROWMAJOR);
  X.Fill(0.0);
  VT accuL = 0.0;
  for (IT j = 0; j < p; j += bj) {
    #pragma omp parallel for
    for (IT k = 0; k < p; ++k) {
      for (IT jj = 0; jj < bj; ++jj) {
        if (j + jj < k)
          L.values_[k * L.lda_ + jj] = 0.0;
        else
          L.values_[k * L.lda_ + jj] = pow(2.0, k-(j+jj));
      }
    }
    
    DMat<IT, VT> subX;
    subX.ShallowSubDMat(X, 0, j, X.rows_, bj);
    Z.MultiplyAdd(L, subX);
  }
  printf("checksum = %lf\n", X.Reduce(SPDM3_SUM));
  
  char filename[20];
  sprintf(filename, "chain%dx%d.quic", n, p);
  X.Save(filename);
  sprintf(filename, "chain%dx%d.npy", n, p);
  X.SaveNumPy(filename);
  
  return 0;
}
