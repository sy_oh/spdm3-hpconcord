#include <cstdio>
#include <cstdlib>
#include "spdm3.h"

using namespace spdm3;

#define IT  int
#define VT  CMDVT

int main(int argc, char *argv[]) {
  if (argc < 2) {
    printf("Usage: %s <input>\n", argv[0]);
    exit(1);
  }
  char *input = argv[1];
  SpMat<IT, VT> X;
  X.LoadMatrixMarket(input);
  X.PrintDense();
  return 0;
}
