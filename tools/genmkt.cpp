#include <cstdio>
#include <cstdlib>
#include "spdm3.h"

using namespace spdm3;

#define IT  int
#define VT  CMDVT

int main(int argc, char *argv[]) {
  if (argc < 4) {
    printf("Usage: %s <rows> <cols> <sparsity> <name>\n", argv[0]);
    exit(1);
  }
  SpMat<IT, VT> O;
  O.Generate(atoi(argv[1]), atoi(argv[2]), atof(argv[3]), time(NULL));
  O.SaveMatrixMarket(argv[4]);
  return 0;
}
