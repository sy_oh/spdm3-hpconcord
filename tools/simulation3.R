## ------------------------------------------------------------------------
library(plyr)
library(igraph)
#library(gconcord)
library(MASS)
library(parallel)
library(ggplot2)
library(Matrix)

## Added by Penporn
## Clear the workspace (pre-existed variables from previous runs).
closeAllConnections()
rm(list = ls())

## ------------------------------------------------------------------------
##First read in the arguments listed at the command line
args=(commandArgs(TRUE))

##args is now a list of character vectors
## First check to see if arguments are passed.
## Then cycle through each element of the list and evaluate the
## expressions.  

if (length(args)>0){
    for(i in 1:length(args)){
        eval(parse(text=args[[i]]))
    }
}


if (exists('seed'))
    set.seed(seed)

if (!exists('p'))
    p = 50

if (!exists('nG'))
    nG = 4

if (exists('pG')){
    if (length(pG)>1){
        nG = length(pG)
        p = sum(pG)
    } else {
        p = nG * pG
        pG = rep(pG, nG)
    }
} else {
    intv = c(0, sort(runif(nG-1)), 1)
    pG = as.numeric(table(cut(runif(p), intv)))
}

cat('p =', p, '\n')
cat('nG=', nG, '\n')
cat('pG=', pG, '\n')

if (!exists('fname')){
    fname = tempfile(tmpdir='.') %>% paste('.npy', sep='')
} else { 
    fname = ifelse(grepl('\\.npy$', fname), fname, 
                   paste(fname, '.npy', sep=''))
}
if (!exists('saveCholesky'))
    saveCholesky = TRUE

## ------------------------------------------------------------------------
getU <- function(p=10, nnz=20, sparse=FALSE){

    if (sparse){
        U = Matrix(0, p, p)
    } else {
        U = matrix(0, p, p)
    }
    Uval = ifelse(runif(nnz)>0.5, 1, -1)
    Uloc = sample.int(p^2, nnz)
    U[Uloc] = Uval
    U

}

getOmega <- function(p=10, nnz=0.01*p*(p-1)/2, sparse=FALSE){

    U = getU(p, round(nnz), sparse)
    A = t(U) %*% U
    if (sparse){
        A = as(A, 'symmetricMatrix')
    }

    d = diag(A)
    if (sparse){
        diag(A) = 0
        A@x = pmax(pmin(A@x, 1), -1)
    } else {
        A = pmax(pmin(A-diag(d), 1), -1)
    }
    A = A + diag(d+1)

    minlambda = eigen(A)$values[p]
    A + max(-1.2*minlambda,10^-4)*diag(p)

    A

}


## ------------------------------------------------------------------------
G.adj = lapply(pG, getOmega, sparse=TRUE)        # get adjacency matrix
G.adj.inv = lapply(G.adj, solve) # solve for block inverses
G.L.inv = lapply(G.adj.inv, chol)
G.adj.nnz = lapply(G.adj, nnzero)

#omega = .bdiag(G.adj)        # construct block diagonal matrix 
#sigma = .bdiag(G.adj.inv)    # construct block diagonal matrix 

## ------------------------------------------------------------------------
if (!interactive()){
    if (saveCholesky){
        for (g in 1:nG){
            repl = paste('-L-',g,'-',pG[g],'-',G.adj.nnz[g],'.mtx', sep='')
            fnameCholesky = gsub('\\.npy', repl, fname)
            writeMM(G.L.inv[[g]], file=fnameCholesky)
        }
    }
}


