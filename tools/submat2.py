import numpy as np
import sys

input = sys.argv[1]
output = sys.argv[2]
row_offset = int(sys.argv[3])
col_offset = int(sys.argv[4])
rows = int(sys.argv[5])
cols = int(sys.argv[6])
x = np.load(input)
print x.shape
x = x[np.ix_(range(row_offset, row_offset + rows),
             range(col_offset, col_offset + cols))]
print x.shape
np.save(output, x)
