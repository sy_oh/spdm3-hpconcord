#include <cstdio>
#include <cstdlib>
#include "spdm3.h"

using namespace spdm3;

#define IT  int
#define VT  CMDVT

int main(int argc, char *argv[]) {
  if (argc < 3) {
    printf("Usage: %s <input> <rows_per_chunk>\n", argv[0]);
    exit(1);
  }
  char *input = argv[1];
  int rows_per_chunk = atoi(argv[2]);
  //printf("Rows per chunk: %d\n", rows_per_chunk);
  
  bool fortran_order = true;
  unsigned int *shape = NULL;
  unsigned int word_size = 0, ndims = 0;
  
  // Reads properties.
  FILE *fp = fopen(input, "rb");
  if (fp == NULL) {
    printf("ERROR! Can't open %s.\n", input);
    exit(1);
  }
  parse_npy_header(fp, word_size, shape, ndims, fortran_order);
  fclose(fp);
  
  // Some sanity checks.
  assert(ndims == 2);     // For now.
  assert(word_size == sizeof(VT));
  assert(fortran_order);  // Only supports column-major ordering for now.
  
  printf("%d %d\n", shape[1], shape[0]);
  for (int row_offset = 0; row_offset < shape[0]; row_offset += rows_per_chunk) {
    DMat<IT, VT> X(DENSE_COLMAJOR);
    IT rows_to_read = std::min(rows_per_chunk, (int) shape[0] - row_offset);
    X.LoadNumPyOffset(input, row_offset, 0, rows_to_read, shape[1]);
    X.Print(true);
  }
  return 0;
}
