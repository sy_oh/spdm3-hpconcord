#include <cstdio>
#include "spdm3.h"

#define IT  int
#define VT  CMDVT

using namespace spdm3;

int main(int argc, char *argv[]) {
  int n = argc - 1;
  SpMat<IT, VT> *M = new SpMat<IT, VT>[n];
  for (int i = 0; i < n; ++i) {
    M[i].LoadMatrixMarket(argv[i+1]);
  }
  
  SpMat<IT, VT> D;
  D.MakeBlockDiagonal(M, n);
  D.SaveMatrixMarket("sol.mkt");

  return 0;
}
