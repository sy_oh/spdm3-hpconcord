#include <stdio.h>
#include <stdlib.h>

#include "omp.h"
#include "spdm3.h"

#define IT  int
#define VT  CMDVT

using namespace spdm3;

// Very simple submatrix generator.
// Matrix file format: npy, column-major
int main(int argc, char *argv[]) {

  if (argc < 5) {
    printf("Usage: %s <input> <output> <rows> <columns>\n", argv[0]);
    exit(1);
  }
  const char *input = argv[1];
  const char *output = argv[2];
  int rows = atoi(argv[3]);
  int cols = atoi(argv[4]);
  double timer, start = omp_get_wtime();
  
  DMat<IT, VT> in(DENSE_COLMAJOR);
  timer = -omp_get_wtime();
  in.LoadNumPy(input, 0, rows, cols);
  timer += omp_get_wtime();
  printf("[%10.4lf] Loaded %s in %lf seconds\n",
        omp_get_wtime() - start, input, timer);
  
  timer = -omp_get_wtime();
  in.SaveNumPy(output);
  timer += omp_get_wtime();
  printf("[%10.4lf] Saved %s in %lf seconds\n",
        omp_get_wtime() - start, output, timer);
  
  return 0;
}
