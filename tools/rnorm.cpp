#include <stdio.h>
#include <stdlib.h>
#include <random>

#define VT double

int main(int argc, char *argv[]) {

  int seed;
  scanf("%d", &seed);
  std::mt19937 generator(seed);
  std::normal_distribution<VT> distribution(0.0, 1.0);
  
  for (int i = 0; i < 10; ++i)
    printf("%lf\n", distribution(generator));
  
  return 0;
}
