import numpy as np
import sys, os

x = np.load("../rg40kx160k.npy");
xrows = x.shape[0]
print "Loaded X, %d x %d" % x.shape

cols = 0
count = 0
countequal = 0
prefix = "part01"
for filename in sorted(os.listdir(prefix)):
  if filename.endswith(".npy"):
    print "Loading %s..." % filename,
    y = np.load(prefix + "/" + filename)
    print "done."
    ycols = y.shape[1]
    endcols = cols + ycols
    print "Allclose:", cols, endcols,
    res = np.allclose(y[:xrows, :], x[:, cols:endcols])
    print res
    cols = endcols
    if res:
      countequal = countequal + 1
    count = count + 1
    if count == 40:
      break

print "%d files out of %d matched." % (countequal, count)

count = 0
checksum = 0.0
set640 = ["part01", "part02", "part03", "part04", "part05", "part06", "part07"]
for prefix in set640[:1]:
  for filename in sorted(os.listdir(prefix)):
    if filename.endswith(".npy"):
      print "Loading %s..." % filename,
      y = np.load(prefix + "/" + filename)
      print "done."
      checksum = checksum + np.sum(y[:10000, :])
      count = count + 1
      if count == 40:
        break

print "%d files: %lf" % (count, checksum)
