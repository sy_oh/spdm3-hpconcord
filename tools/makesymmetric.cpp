#include <cstdio>
#include "spdm3.h"

#define IT  int
#define VT  CMDVT

using namespace spdm3;

int main(int argc, char *argv[]) {
  assert(argc == 3);

  double start = read_timer();
  DMat<IT, VT> A(DENSE_COLMAJOR);
  A.LoadNumPy(argv[1]);
  printf("[%10.4lf] A is read.\n", read_timer() - start);
  A.MakeSymmetric();
  printf("[%10.4lf] A has been made symmetric.\n", read_timer() - start);
  A.SaveNumPy(argv[2]);
  printf("[%10.4lf] Saved A.\n", read_timer() - start);
  return 0;
}
