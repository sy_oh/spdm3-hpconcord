#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "spdm3.h"

using namespace spdm3;

int main(int argc, char *argv[]) {
  // Randomizes seed.
  time_t t;
  srand((unsigned) time(&t));
  
  // Generates the array.
  int len = 20;
  Triplet *array = new Triplet[len];
  for (int i = 0; i < 20; ++i) {
    array[i].row = i / 4;
    array[i].col = i;
    array[i].weight = rand() % 100;
    printf("%d %d %lf\n", array[i].row, array[i].col, array[i].weight);
  }
  qsort(array, len, sizeof(Triplet),
      [] (const void *a, const void *b) -> int {
        Triplet *ta = (Triplet *) a;
        Triplet *tb = (Triplet *) b;
        if (ta->row != tb->row)
          return ((int) ta->row - (int) tb->row);
        return ((int) ta->col - (int) tb->col);
      });
  for (int i = 0; i < len; ++i)
    printf("%lld %lld\n", array[i].row, array[i].col);
  delete [] array;
  return 0;
}
