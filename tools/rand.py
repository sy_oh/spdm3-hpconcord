import numpy as np
import sys

rows = int(sys.argv[1])
cols = int(sys.argv[2])
x = np.random.randn(rows, cols)
np.save('x', x)
np.save('xf', np.asfortranarray(x))
