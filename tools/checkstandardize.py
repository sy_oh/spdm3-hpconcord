import numpy as np
import sys

x = np.load(sys.argv[1])
threshold = 1e-14
for i in range(0, x.shape[1]):
  col = x[:, i]
  if abs(np.var(col) - 1.0) > threshold or abs(np.mean(col) - 0.0) > threshold:
    print i, abs(np.var(col)), abs(np.mean(col))
