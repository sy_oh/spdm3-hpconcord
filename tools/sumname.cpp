#include <cstdio>
#include <cstdlib>
#include <cstring>

bool isDigit(const char c) {
  if ('0' <= c && c <= '9')
    return true;
  return false;
}

// Input:  File names of Ls (.mtx format) in argv
// Output: Sum of nnz at the end of the file names.
int main(int argc, char *argv[]) {
  int total_sum = 0;
  for (int i = 1; i < argc; ++i) {
    int nnz = 0, mult = 1;
    int j = strlen(argv[i]) - 5;
    while (isDigit(argv[i][j])) {
      nnz += (argv[i][j] - '0') * mult;
      mult *= 10;
      --j;
    }
    total_sum += nnz;
    printf("%s\n%d\n", argv[i], nnz);
  }
  printf("Total nnz: %d\n", total_sum);
  printf("Number of files: %d\n", argc-1);
  return 0;
}
