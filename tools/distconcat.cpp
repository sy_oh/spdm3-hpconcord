#include "spdm3.h"

#define IT  int
#define VT  CMDVT

using namespace spdm3;

int main(int argc, char *argv[]) {
  int rank, procs;
  
  // Initializes MPI.
  Timer T;
  MPI_File fh;
  MPI_Init(&argc, &argv);
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  MPI_Comm_size(MPI_COMM_WORLD, &procs);
  
  T.Init(MPI_COMM_WORLD);
  
  MPI_Datatype mpi_it = (sizeof(IT) == 4)? MPI_INT : MPI_LONG_LONG;
  MPI_Datatype mpi_vt = (sizeof(VT) == 4)? MPI_FLOAT : MPI_DOUBLE;

  int files = argc - 1;
  int *file_counts = new int[procs];
  int *file_displs = new int[procs + 1];
  gen_counts_displs_frontloaded(files, procs, file_counts, file_displs);
  
  int my_start = file_displs[rank];
  int my_end   = file_displs[rank + 1];
  
  T.Start("Overall");
  T.Start("Read header");
  unsigned int global_rows = 0, global_cols = 0;
  int *col_counts = new int[files];
  int *col_displs = new int[files + 1];
  for (int i = my_start; i < my_end; ++i) {
    unsigned int *shapes;
    get_npy_shapes(argv[i+1], shapes);
    
    // Number of rows.
    if (global_rows == 0)
      global_rows = shapes[0];
    else
      assert(global_rows == shapes[0]);
  
    // Number of columns.
    col_counts[i] = shapes[1];
    
    delete [] shapes;
  }
  T.Stop("Read header");
  T.Start("Allgather");
  MPI_Allgatherv(MPI_IN_PLACE, 0, mpi_it, col_counts,
                file_counts, file_displs, mpi_it, MPI_COMM_WORLD);
  gen_displs_from_counts(files, col_counts, col_displs);
  global_cols = col_displs[files];
  T.Stop("Allgather");
  
  // Broadcasts global_rows in case
  // some process didn't get to read the files earlier.
  T.Start("Bcast");
  MPI_Bcast(&global_rows, 1, MPI_INT, 0, MPI_COMM_WORLD);
  T.Stop("Bcast");
  
  // Writes NumPy header.
  T.Start("File open");
  const char *output_name = "concat.npy";
  MPI_File_open(MPI_COMM_WORLD, output_name,
                MPI_MODE_CREATE | MPI_MODE_WRONLY,
                MPI_INFO_NULL, &fh);
  T.Stop("File open");
  
  T.Start("Write header");
  int64_t header_offset = 0;
  unsigned int shapes[2] = {global_rows, global_cols};
  if (rank == 0)
    write_npy_header<VT>(output_name, NULL, shapes, 2, true, header_offset);
  T.Stop("Write header");
  T.Start("Bcast2");
  MPI_Bcast(&header_offset, 1, MPI_LONG_LONG, 0, MPI_COMM_WORLD);
  T.Stop("Bcast2");
  
  // Finds the file index to start.
  int i = 0;
  my_start = find_offset_frontloaded(global_cols, procs, rank);
  my_end   = find_offset_frontloaded(global_cols, procs, rank + 1);
  int64_t fpos_offset = (int64_t) header_offset + my_start * global_rows * sizeof(VT);
  T.Start("Set view");
  MPI_File_set_view(fh, fpos_offset, mpi_vt, mpi_vt, "native", MPI_INFO_NULL);
  T.Stop("Set view");
  printf("%d: %d to %d, offset %lld\n", rank, my_start, my_end, fpos_offset);
  for (; col_displs[i+1] <= my_start; ++i);
  for (; col_displs[i] < my_end; ++i) {
    int64_t start_col = std::max(0 , my_start - col_displs[i]) ;
    int64_t end_col   = std::min(col_counts[i], my_end - col_displs[i]);
    int64_t num_cols  = end_col - start_col;
    int64_t elmts_to_write = num_cols * global_rows;
    printf("%d: %s, %d %d, %lld\n", rank, argv[i+1], start_col, num_cols, elmts_to_write);
    
    T.Start("Read input");
    DMat<IT, VT> in(DENSE_COLMAJOR);
    in.LoadNumPy(argv[i+1], start_col, num_cols);
    T.Stop("Read input");
    
    T.Start("Barrier");
    MPI_Barrier(MPI_COMM_WORLD);
    T.Stop("Barrier");
    
    // TODO(penpornk): Split to multiple writes if elmts_to_write is larger than int.
    T.Start("File write");
    MPI_File_write(fh, in.values_, (int) elmts_to_write, mpi_vt, MPI_STATUS_IGNORE);
//    MPI_File_write_at(fh, fpos_offset, in.values_,
//                      (int) elmts_to_write, mpi_vt, MPI_STATUS_IGNORE);
    T.Stop("File write");
    T.PrintCounterLocal("File write");
  }
  
  // Deallocates buffers.
  delete [] file_counts;
  delete [] file_displs;
  delete [] col_counts;
  delete [] col_displs;
  
  T.Stop("Overall");
  T.Report();
  
  // Finalizes MPI.
  MPI_File_close(&fh);
  MPI_Finalize();
  return 0;
}
