#include <cstdio>
#include "spdm3.h"

#define IT  int
#define VT  CMDVT

using namespace spdm3;

int main(int argc, char *argv[]) {
  assert(argc == 4);

  double start = read_timer();
  VT offset = atof(argv[3]);
  DMat<IT, VT> A(DENSE_COLMAJOR);
  A.LoadNumPy(argv[1]);
  printf("[%10.4lf] A is read.\n", read_timer() - start);
  for (int i = 0; i < A.rows_; ++i) {
    A.values_[i * A.lda_ + i] += offset;
  }
  printf("[%10.4lf] Added offset %g\n", read_timer() - start, offset);
  
  A.SaveNumPy(argv[2]);
  printf("[%10.4lf] A is saved\n", read_timer() - start);
  
  return 0;
}
