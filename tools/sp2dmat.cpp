#include "dmat.h"
#include "spmat.h"

#define IT int
#define VT double

using namespace spdm3;

int main(int argc, char *argv[]) {
  SpMat<IT, VT> a;
  a.Load(argv[1]);
  DMat<IT, VT> b(a);
  b.Save(argv[2]);
  return 0;
}