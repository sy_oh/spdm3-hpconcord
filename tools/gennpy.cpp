#include <cstdio>
#include <cstdlib>
#include "spdm3.h"

using namespace spdm3;

#define IT  int
#define VT  CMDVT

int main(int argc, char *argv[]) {
  if (argc < 4) {
    printf("Usage: %s <rows> <cols> <name>\n", argv[0]);
    exit(1);
  }
  int rows = atoi(argv[1]);
  int cols = atoi(argv[2]);
  char *output = argv[3];
  DMat<IT, VT> X(DENSE_COLMAJOR);
  X.Allocate(rows, cols);
  
  int idx = 0;
  for (int j = 0; j < X.dim1(); ++j) {
    for (int i = 0; i < X.dim2(); ++i)
      X.values_[idx++] = j;
  }

  X.SaveNumPy(output);
  return 0;
}
