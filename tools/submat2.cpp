#include <stdio.h>
#include <stdlib.h>

#include "omp.h"
#include "spdm3.h"

#define IT  int
#define VT  CMDVT

using namespace spdm3;

// Very simple submatrix generator.
// Matrix file format: npy, column-major
int main(int argc, char *argv[]) {

  if (argc < 5) {
    printf("Usage: %s <input> <output> <rows> <columns>\n", argv[0]);
    exit(1);
  }
  const char *input = argv[1];
  const char *output = argv[2];
  int rows = atoi(argv[3]);
  int cols = atoi(argv[4]);
  int max_cols = 1000;
  double timer, start = omp_get_wtime();
  
  // Writes NPY header.
  const unsigned int shape[2] = {rows, cols};
  FILE *f_out = fopen(output, "wb");
  write_npy_header<VT>(f_out, NULL, shape, 2, true);
  
  // Reads and writes out max_cols columns at a time.
  for (IT col_offset = 0; col_offset < cols; col_offset += max_cols) {
    DMat<IT, VT> in(DENSE_COLMAJOR);
    timer = -omp_get_wtime();
    in.LoadNumPy(input, col_offset, rows, std::min(max_cols, cols-col_offset));
    timer += omp_get_wtime();
    printf("[%10.4lf] %07d: Loaded %s in %lf seconds\n",
          omp_get_wtime() - start, col_offset, input, timer);
    
    timer = -omp_get_wtime();
    for (int i1 = 0; i1 < in.dim1(); ++i1) {
      fwrite(in.values_ + i1 * in.lda_, sizeof(VT), in.dim2(), f_out);
    }
    timer += omp_get_wtime();
    printf("[%10.4lf] %07d: Saved %s in %lf seconds\n",
          omp_get_wtime() - start, col_offset, output, timer);
  }
  fclose(f_out);
  return 0;
}
