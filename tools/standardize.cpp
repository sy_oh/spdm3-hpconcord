#include <stdio.h>

#include "omp.h"
#include "spdm3.h"

#define IT  int
#define VT  CMDVT

using namespace spdm3;

// Very simple matrix concatenation code.
// Matrix file format: npy, column-major.
// Concatenates columns.
// Usage: ./concat <input_1> ... <input_n> <output>
int main(int argc, char *argv[]) {
  double timer, start = omp_get_wtime();
  int64_t num_elements = 0;
  
  // Writes file-by-file.
  for (int i = 1; i < argc; ++i) {
    unsigned int shapes[2];
    DMat<IT, VT> in(DENSE_COLMAJOR);
    timer = -omp_get_wtime();
    in.LoadNumPy(argv[i]);
    timer += omp_get_wtime();
    printf("[%10.4lf] %03d: Loaded in %lf seconds\n",
        omp_get_wtime() - start, i, timer);
    
    in.Standardize();
  
    timer = -omp_get_wtime();
    char fname[200];
    sprintf(fname, "std_%s", argv[i]);
    in.SaveNumPy(fname);
    timer += omp_get_wtime();
    printf("[%10.4lf] %03d:  Wrote in %lf seconds\n",
        omp_get_wtime() - start, i, timer);
  }
  return 0;
}
