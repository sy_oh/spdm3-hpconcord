#include <stdio.h>
#include <algorithm>
#include "spdm3.h"

#define IT  int
#define VT  CMDVT

using namespace spdm3;

int main(int argc, char *argv[]) {
  SpMat<IT, VT> X, Y;
  X.LoadMatrixMarket(argv[1]);
//  X.PrintDense();
  
  int rows = X.rows_;
  int cols = X.cols_;
  if (argc == 4) {
    rows = std::min(rows, atoi(argv[2]));
    cols = std::min(cols, atoi(argv[3]));
  }
  Y.Submatrix(X, 0, 0, rows, cols);
  printf("%d\n", Y.nnz_);
  return 0;
}
