#include <cstdio>
#include "spdm3.h"

#define IT  int
#define VT  CMDVT

using namespace spdm3;

int main(int argc, char *argv[]) {
  assert(argc == 2);

  double start = read_timer();
  DMat<IT, VT> A;
  A.LoadNumPy(argv[1]);
  printf("[%10.4lf] A is read.\n", read_timer() - start);
  if (A.IsSymmetric())
    printf("[%10.4lf] A is symmetric.\n", read_timer() - start);
  else
    printf("[%10.4lf] A is not symmetric.\n", read_timer() - start);
  return 0;
}
