import numpy as np
import sys

rows = int(sys.argv[1])
cols = int(sys.argv[2])
x = np.arange(rows*cols).reshape((rows, cols), order='F').astype(np.float64)
np.save('x', x)
