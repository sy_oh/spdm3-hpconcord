#include "comm.h"

#include <cstdlib>
#include "utility.h"

namespace spdm3 {

Comm *GetComms(const MPI_Comm &world, spdm3_layout layout, int layers) {
  Comm *comm = new Comm[NUM_COMMS];
  for (int i = 0; i < NUM_COMMS; ++i)
    comm[i].layout = layout;
  
  // Copies comm_world.
  MPI_Comm_dup(world, &(comm[WORLD].comm));
  MPI_Comm_size(world, &(comm[WORLD].size));
  MPI_Comm_rank(world, &(comm[WORLD].rank));
  
  int grid_blk_rows, grid_blk_cols;
  int team_blk_rows, team_blk_cols;
  switch(layout) {
    case ONED_BLOCK_ROW:
      grid_blk_cols = 1;
      grid_blk_rows = comm[WORLD].size;
      team_blk_cols = 1;
      team_blk_rows = layers;
      break;
    case ONED_BLOCK_COLUMN:
      grid_blk_cols = comm[WORLD].size;
      grid_blk_rows = 1;
      team_blk_cols = layers;
      team_blk_rows = 1;
      break;
    case TWOD_ROWMAJOR:
    case TWOD_COLMAJOR:
      find_grid_size(comm[WORLD].size, &grid_blk_rows, &grid_blk_cols);
      find_grid_size(layers, &team_blk_rows, &team_blk_cols);
      break;
    default:
      printf("GetComms: ERROR! Unknown layout!\n");
      exit(1);
  }
  
  int grid_row_id, grid_col_id;
  int team_row_id, team_col_id;
  if (layout != TWOD_COLMAJOR) {
    grid_row_id = comm[WORLD].rank / grid_blk_cols;
    grid_col_id = comm[WORLD].rank % grid_blk_cols;
  } else {
    grid_row_id = comm[WORLD].rank % grid_blk_rows;
    grid_col_id = comm[WORLD].rank / grid_blk_rows;
  }
  
  comm[LAYER_ROW].id   = grid_row_id / team_blk_rows;
  comm[LAYER_COL].id   = grid_col_id / team_blk_cols;
  comm[LAYER_ROW].size = grid_blk_cols / team_blk_cols;
  comm[LAYER_COL].size = grid_blk_rows / team_blk_rows;
  comm[LAYER_ROW].rank = comm[LAYER_COL].id;
  comm[LAYER_COL].rank = comm[LAYER_ROW].id;
  
  team_row_id = grid_row_id % team_blk_rows;
  team_col_id = grid_col_id % team_blk_cols;
  
  if (layout != TWOD_COLMAJOR) {
    comm[TEAM].rank  = team_row_id * team_blk_cols + team_col_id;
    comm[LAYER].rank = comm[LAYER_ROW].id * comm[LAYER_ROW].size + comm[LAYER_COL].id;
  } else {
    comm[TEAM].rank  = team_col_id * team_blk_rows + team_row_id;
    comm[LAYER].rank = comm[LAYER_COL].id * comm[LAYER_COL].size + comm[LAYER_ROW].id;
  }
  comm[TEAM].size  = layers;
  comm[LAYER].size = comm[WORLD].size / layers;
  comm[TEAM].id    = comm[LAYER].rank;
  comm[LAYER].id   = comm[TEAM].rank;
  
  // No actual communicators.
  comm[TEAM_ROW].id = team_row_id;
  comm[TEAM_COL].id = team_col_id;
  comm[TEAM_ROW].size = team_blk_cols;
  comm[TEAM_COL].size = team_blk_rows;
  comm[TEAM_ROW].comm = MPI_COMM_NULL;
  comm[TEAM_COL].comm = MPI_COMM_NULL;
  
  // Creates communicators.
  MPI_Comm_split(comm[WORLD].comm,
                 comm[TEAM].id, comm[TEAM].rank, &(comm[TEAM].comm));
  MPI_Comm_split(comm[WORLD].comm,
                 comm[LAYER].id, comm[LAYER].rank, &(comm[LAYER].comm));
  MPI_Comm_split(comm[LAYER].comm,
                 comm[LAYER_ROW].id, comm[LAYER_ROW].rank, &(comm[LAYER_ROW].comm));
  MPI_Comm_split(comm[LAYER].comm,
                 comm[LAYER_COL].id, comm[LAYER_COL].rank, &(comm[LAYER_COL].comm));
  
  return comm;
}

void GetMpiCommSubset(int nprocs, MPI_Comm input, MPI_Comm *output) {
  int rank = 0;
  MPI_Comm_rank(input, &rank);
  MPI_Comm_split(input, (rank < nprocs), rank, output);
}

int GetLayerRank(Comm *comm, spdm3_layout layout, int row_id, int col_id) {
  if (layout == TWOD_COLMAJOR)
    return row_id * comm[LAYER_ROW].size + col_id;
  return col_id * comm[LAYER_COL].size + row_id;
}

void GetRowColIdFromLayerRank(Comm *comm, spdm3_layout layout,
                              int layer_rank, int *row_id, int *col_id) {
  if (layout == TWOD_COLMAJOR) {
    *col_id = layer_rank / comm[LAYER_COL].size;
    *row_id = layer_rank % comm[LAYER_COL].size;
    return;
  }
  *row_id = layer_rank / comm[LAYER_ROW].size;
  *col_id = layer_rank % comm[LAYER_ROW].size;
}

}  // namespace spdm3