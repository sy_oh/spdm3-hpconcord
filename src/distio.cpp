// Included by distio.h

namespace spdm3 {

template <class IT, class VT>
DistDMat<IT, VT> LoadNumPy(const char *filename) {
  // Reads properties.
  bool fortran_order;
  unsigned int *shape;
  unsigned int word_size, ndims;
  long long int fpos_offset;
  
  int rank;
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  if (rank == 0) {
    parse_npy_header(filename, word_size, shape, ndims, fortran_order, fpos_offset);
    assert(ndims == 2);  // For now.
    assert(word_size == sizeof(VT));
  } else {
    shape = new unsigned int[2];
    word_size = sizeof(VT);
  }
  
  MPI_Bcast(shape, 2, MPI_UNSIGNED, 0, MPI_COMM_WORLD);
  MPI_Bcast(&fpos_offset, 1, MPI_LONG_LONG, 0, MPI_COMM_WORLD);
  MPI_Bcast(&fortran_order, 1, MPI_C_BOOL, 0, MPI_COMM_WORLD);

  // Sets format and layout.
  spdm3_format format;
  spdm3_layout layout;
  if (fortran_order) {
    format = DENSE_COLMAJOR;
    layout = ONED_BLOCK_COLUMN;
  } else {
    format = DENSE_ROWMAJOR;
    layout = ONED_BLOCK_ROW;
  }
  Comm *comm = GetComms(MPI_COMM_WORLD, layout, 1);
  DistDMat<IT, VT> M(comm, format);
  
  // Sets size and calculates row and column distribution.
  M.SetSize(shape[0], shape[1]);  // global_rows_, global_cols_
  delete [] shape;
  
  // Allocates biggest possible size (max of all local matrices)
  // to avoid reallocation during Shift()/Bcast().
  // Index 0 always stores biggest size.
  M.max_local_rows_ = M.row_counts_[0];
  M.max_local_cols_ = M.col_counts_[0];
  M.max_local_lda_  = M.max_ldim2();
  LT max_buffer_size = M.max_ldim1() * M.max_llda();
  M.mat_.Allocate(M.row_counts_[M.comm_[LAYER_ROW].id],
                  M.col_counts_[M.comm_[LAYER_COL].id], max_buffer_size);
  
  // Opens MPI file.
  MPI_File f;
  long long int file_size_in_bytes;
  MPI_File_open(MPI_COMM_WORLD, filename, MPI_MODE_RDONLY, MPI_INFO_NULL, &f);
  MPI_File_get_size(f, &file_size_in_bytes);
  assert(file_size_in_bytes - fpos_offset == M.rows() * M.cols() * word_size);

  // Reads MPI file.
  MPI_Status status;
  MPI_Offset offset =
      (MPI_Offset)(fpos_offset + M.dim1_displs() * M.dim2() * word_size);
  MPI_File_seek(f, offset, MPI_SEEK_SET);
  MPI_File_read(f, M.mat_.values_, M.mat_.dim1() * M.mat_.dim2() * word_size,
                MPI_BYTE, &status);
  
  // Closes MPI file.
  MPI_File_close(&f);
  
  return M;
}

}  // namespace spdm3