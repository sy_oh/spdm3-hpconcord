// Included by utility.h
#include "utility.h"

#include <algorithm>
#include <cassert>
#include <complex>
#include <cstring>
#include <stdexcept>
#include <sys/time.h>
#include <typeinfo>
#include <vector>

#include "mmio.h"

namespace spdm3 {

int find_option(int argc, char *argv[], const char *option) {
  for (int i = 1; i < argc; ++i) {
    if (strcmp(argv[i], option) == 0)
      return i;
  }
  return -1;
}

double read_timer() {
  static bool initialized = false;
  static struct timeval start;
  struct timeval end;
  if (!initialized) {
    gettimeofday(&start, NULL);
    initialized = true;
  }
  gettimeofday(&end, NULL);
  return (end.tv_sec - start.tv_sec) + 1.0e-6 * (end.tv_usec - start.tv_usec);
}

int read_int(int argc, char *argv[],
             const char *option, int default_value) {
  int idx = find_option(argc, argv, option);
  if (0 <= idx && idx < argc-1)
    return atoi(argv[idx+1]);
  return default_value;
}

double read_double(int argc, char *argv[],
                   const char *option, double default_value) {
  int idx = find_option(argc, argv, option);
  if (0 <= idx && idx < argc-1)
    return atof(argv[idx+1]);
  return default_value;
}

char *read_string(int argc, char *argv[],
                  const char *option, char *default_value) {
  int idx = find_option(argc, argv, option);
  if (0 <= idx && idx < argc-1)
    return argv[idx+1];
  return default_value;
}

int reverse_bit(int num, int max_val) {
  int result = 0;
  while (max_val > 0) {
    result <<= 1;
    result |= (num & 1);
    num >>= 1;
    max_val >>= 1;
  }
  return result;
}

int find_count(int n, int p, int rank) {
  int r = n % p;
  int count = n / p;
  if (reverse_bit(rank, p-1) < r)
    ++count;
  return count;
}

int find_count_frontloaded(int n, int p, int rank) {
  return (n / p) + (rank < (n % p));
}
int find_offset_frontloaded(int n, int p, int rank) {
  return (n / p) * rank + std::min(rank, (n % p));
}

void find_grid_size(int size, int *rows, int *cols) {
  for (*rows = sqrt(size); *rows > 1; --(*rows)) {
    if (size % (*rows) == 0)
      break;
  }
  *cols = size / *rows;
}

void parse_npy_header(const char* filename, unsigned int& word_size,
                      unsigned int*& shape, unsigned int& ndims,
                      bool& fortran_order, long long int& fpos_offset) {
  FILE *fp = fopen(filename, "rb");
  if (fp == NULL) {
    printf("parse_npy_header: ERROR! File does not exist (%s).\n", filename);
    exit(1);
  }
  parse_npy_header(fp, word_size, shape, ndims, fortran_order);
  fpos_offset = ftell(fp);
  fclose(fp);
}

void parse_npy_header(FILE* fp, unsigned int& word_size,
                      unsigned int*& shape, unsigned int& ndims,
                      bool& fortran_order) {
  char buffer[256];
  size_t res = fread(buffer,sizeof(char),11,fp);       
  if (res != 11)
    throw std::runtime_error("parse_npy_header: failed fread");
  std::string header = fgets(buffer,256,fp);
  assert(header[header.size()-1] == '\n');

  // Fortran order.
  int loc1, loc2;
  loc1 = header.find("fortran_order")+16;
  fortran_order = (header.substr(loc1,4) == "True" ? true : false);

  // Shape.
  loc1 = header.find("(");
  loc2 = header.find(")");
  std::string str_shape = header.substr(loc1+1,loc2-loc1-1);
  
  if (str_shape[str_shape.size()-1] == ',')
    ndims = 1;
  else
    ndims = std::count(str_shape.begin(),str_shape.end(),',')+1;
  
  shape = new unsigned int[ndims];
  for (unsigned int i = 0; i < ndims; ++i) {
    loc1 = str_shape.find(",");
    shape[i] = atoi(str_shape.substr(0,loc1).c_str());
    str_shape = str_shape.substr(loc1+1);
  }

  // Endian, word size, data type.
  // Byte order code | stands for not applicable.
  // Not sure when this applies except for byte array.
  loc1 = header.find("descr")+9;
  bool littleEndian = (header[loc1] == '<' || header[loc1] == '|' ? true : false);
  assert(littleEndian);

  //char type = header[loc1+1];
  //assert(type == map_type(T));

  std::string str_ws = header.substr(loc1+2);
  loc2 = str_ws.find("'");
  word_size = atoi(str_ws.substr(0,loc2).c_str());
}

void get_npy_shapes(const char* filename, unsigned int*& shape, unsigned int& ndims) {
  FILE *fp = fopen(filename, "rb");
  char buffer[256];
  size_t res = fread(buffer,sizeof(char),11,fp);       
  if (res != 11)
    throw std::runtime_error("parse_npy_header: failed fread");
  std::string header = fgets(buffer,256,fp);
  assert(header[header.size()-1] == '\n');
  fclose(fp);
  
  // Shape.
  int loc1 = header.find("(");
  int loc2 = header.find(")");
  std::string str_shape = header.substr(loc1+1,loc2-loc1-1);
  
  if (str_shape[str_shape.size()-1] == ',')
    ndims = 1;
  else
    ndims = std::count(str_shape.begin(),str_shape.end(),',')+1;
  
  shape = new unsigned int[ndims];
  for (unsigned int i = 0; i < ndims; ++i) {
    loc1 = str_shape.find(",");
    shape[i] = atoi(str_shape.substr(0,loc1).c_str());
    str_shape = str_shape.substr(loc1+1);
  }
}

FILE *parse_matrix_market_header(const char *filename,
                                 int64_t &nrows, int64_t &ncols,
                                 int64_t &nonzeros, int64_t &linesread,
                                 int &type, int &symmetric) {
  FILE *f;
  MM_typecode matcode;
  if ((f = fopen(filename, "r")) == NULL) {
    printf("Matrix-market file can not be found\n");
    exit(1);
  }
  if (mm_read_banner(f, &matcode) != 0) {
    printf("Could not process Matrix Market banner.\n");
    exit(1);
  }
  linesread++;
  
  if (mm_is_complex(matcode)) {
    printf("Sorry, this application does not support complext types");
    printf("Market Market type: [%s]\n", mm_typecode_to_str(matcode));
  } else if(mm_is_real(matcode)) {
    printf("Matrix is Float\n");
    type = 0;
  } else if(mm_is_integer(matcode)) {
    printf("Matrix is Integer\n");
    type = 1;
  } else if(mm_is_pattern(matcode)) {
    printf("Matrix is Boolean\n");
    type = 2;
  }
  if(mm_is_symmetric(matcode)) {
    printf("Matrix is symmetric\n");
    symmetric = 1;
  }
  
  // ABAB: mm_read_mtx_crd_size made 64-bit friendly
  if (mm_read_mtx_crd_size(f, &nrows, &ncols, &nonzeros, &linesread) !=0)
    exit(1);
  
  return f;
}

char BigEndianTest() {
    unsigned char x[] = {1, 0};
    short y = *(short*) x;
    return y == 1 ? '<' : '>';
}

char map_type(const std::type_info& t)
{
    if(t == typeid(float) ) return 'f';
    if(t == typeid(double) ) return 'f';
    if(t == typeid(long double) ) return 'f';

    if(t == typeid(int) ) return 'i';
    if(t == typeid(char) ) return 'i';
    if(t == typeid(short) ) return 'i';
    if(t == typeid(long) ) return 'i';
    if(t == typeid(long long) ) return 'i';

    if(t == typeid(unsigned char) ) return 'u';
    if(t == typeid(unsigned short) ) return 'u';
    if(t == typeid(unsigned long) ) return 'u';
    if(t == typeid(unsigned long long) ) return 'u';
    if(t == typeid(unsigned int) ) return 'u';

    if(t == typeid(bool) ) return 'b';

    if(t == typeid(std::complex<float>) ) return 'c';
    if(t == typeid(std::complex<double>) ) return 'c';
    if(t == typeid(std::complex<long double>) ) return 'c';

    else return '?';
}



}  // namespace spdm3
