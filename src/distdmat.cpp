// Included by distdmat.h
#include <cstdarg>
#include "distmatmul.h"
#include "utility.h"

namespace spdm3 {

//
// Constructors & Destructor.
//
template <class IT, class VT>
DistDMat<IT, VT>::DistDMat(Comm *comm, spdm3_format format)
  : comm_(comm),
    mat_(format),
    global_rows_(0),
    global_cols_(0),
    row_counts_(NULL),
    row_displs_(NULL),
    col_counts_(NULL),
    col_displs_(NULL),
    distance_shifted_(NULL),
    owner_rank_(NULL),
    max_local_rows_(0),
    max_local_cols_(0),
    max_local_lda_(0),
    name_(NULL),
    shift_dir_(NOT_SHIFTING) {
  switch (sizeof(IT)) {
    case 4: mpi_it_ = MPI_INT; break;
    case 8: mpi_it_ = MPI_LONG_LONG; break;
    default:
      printf("DistDMat::ERROR! Wrong mpi_it_ data type.\n");
      exit(1);
  }
  switch (sizeof(LT)) {
    case 4: mpi_lt_ = MPI_INT; break;
    case 8: mpi_lt_ = MPI_LONG_LONG; break;
    default:
      printf("DistDMat::ERROR! Wrong mpi_lt_ data type.\n");
      exit(1);
  }
  switch (sizeof(VT)) {
    case 4: mpi_vt_ = MPI_FLOAT; break;
    case 8: mpi_vt_ = MPI_DOUBLE; break;
    default:
      printf("DistDMat::ERROR! Wrong mpi_vt_ data type.\n");
      exit(1);
  }
  
  AllocateAux();
}

template <class IT, class VT>
DistDMat<IT, VT>::~DistDMat() {
  // TODO: free the counts and displs
  
}

//
// Allocation.
//
template <class IT, class VT>
void DistDMat<IT, VT>::AllocateAux() {
  DeallocateAux();
  row_counts_ = new IT[comm_[LAYER_COL].size];
  col_counts_ = new IT[comm_[LAYER_ROW].size];
  row_displs_ = new IT[comm_[LAYER_COL].size + 1];
  col_displs_ = new IT[comm_[LAYER_ROW].size + 1];
  
  distance_shifted_ = new IT[NUM_COMMS];
  owner_rank_       = new IT[NUM_COMMS];
  fill_vec<IT>(distance_shifted_, NUM_COMMS, 0);
  for (int i = 0; i < NUM_COMMS; ++i)
    owner_rank_[i] = comm_[i].rank;
}

template <class IT, class VT>
void DistDMat<IT, VT>::DeallocateAux() {
  FREE_IF_NOT_NULL(row_counts_);
  FREE_IF_NOT_NULL(row_displs_);
  FREE_IF_NOT_NULL(col_counts_);
  FREE_IF_NOT_NULL(col_displs_);
  FREE_IF_NOT_NULL(distance_shifted_);
  FREE_IF_NOT_NULL(owner_rank_);
}

//
// I/O.
//
template <class IT, class VT>
void DistDMat<IT, VT>::GenerateWithFunction(IT global_rows, IT global_cols, int seed) {
  srand(seed);
  IT row_coef = (rand() % 1009) - 504;
  IT col_coef = (rand() % 1723) - 861;
  
  SetSize(global_rows, global_cols);
  max_local_rows_ = row_counts_[0];
  max_local_cols_ = col_counts_[0];
  max_local_lda_  = max_ldim2();
  LT max_buffer_size = max_ldim1() * max_llda();
  mat_.Allocate(row_counts_[comm_[LAYER_COL].rank],
                col_counts_[comm_[LAYER_ROW].rank], max_buffer_size);
  IT RANGE = 100, MAX_VAL = RANGE >> 1;
  
  switch (mat_.format_) {
    case DENSE_ROWMAJOR:
      #pragma omp parallel for
      for (int i = 0; i < lrows(); ++i) {
        IT global_row_id = row_displs_[comm_[LAYER_COL].rank] + i;
        for (int j = 0; j < lcols(); ++j) {
          IT global_col_id = col_displs_[comm_[LAYER_ROW].rank] + j;
          VT val = MAX_VAL - (global_row_id * global_row_id * row_coef +
                              global_col_id * global_col_id * col_coef) % RANGE;
          if (val <= 0)
            --val;
          mat_.values_[i * mat_.lda_ + j] = val;
        }
      }
      break;
      
    case DENSE_COLMAJOR:
      #pragma omp parallel for
      for (int j = 0; j < lcols(); ++j) {
        IT global_col_id = col_displs_[comm_[LAYER_ROW].rank] + j;
        for (int i = 0; i < lrows(); ++i) {
          IT global_row_id = row_displs_[comm_[LAYER_COL].rank] + i;
          VT val = MAX_VAL - (global_row_id * global_row_id * row_coef +
                              global_col_id * global_col_id * col_coef) % RANGE;
          if (val <= 0)
            --val;
          mat_.values_[i + mat_.lda_ * j] = val;
        }
      }
      break;
      
    default:
      printf("DistDMat::GenerateWithFunction: ERROR! Unknown format.\n");
      exit(1);
  }
}

template <class IT, class VT>
void DistDMat<IT, VT>::Save(const char *filename) {
  // TODO(penpornk): make sure the distance_shifted == 0.
  if (layer_id() == 0)
    GetWholeMatrix().Save(filename);
}

template <class IT, class VT>
void DistDMat<IT, VT>::SaveLocal(const char *filename) {
  mat_.Save(filename, rank());
}

template <class IT, class VT>
void DistDMat<IT, VT>::LoadNumPy(const char *filename) {
  // Reads properties.
  bool fortran_order = false;
  unsigned int *shape = NULL;
  unsigned int word_size = 0, ndims = 0;
  long long int fpos_offset = 0;
  if (rank() == 0) {
    parse_npy_header(filename, word_size, shape, ndims, fortran_order, fpos_offset);
    assert(ndims == 2);  // For now.
    assert(word_size == sizeof(VT));
  } else {
    shape = new unsigned int[2];
    word_size = sizeof(VT);
  }
  
  MPI_Bcast(shape, 2, MPI_UNSIGNED, 0, comm_[WORLD].comm);
  MPI_Bcast(&fpos_offset, 1, MPI_LONG_LONG, 0, comm_[WORLD].comm);
  MPI_Bcast(&fortran_order, 1, MPI_C_BOOL, 0, comm_[WORLD].comm);
  assert(!fortran_order);

  // Normal read works.
  if (layout() == ONED_BLOCK_ROW && mat_.format_ == DENSE_ROWMAJOR && !fortran_order) {
    SetSize(shape[1], shape[0]);
    delete [] shape;
    
    // Allocates biggest possible size (max of all local matrices)
    // to avoid reallocation during Shift()/Bcast().
    // Index 0 always stores biggest size.
    max_local_rows_ = row_counts_[0];
    max_local_cols_ = col_counts_[0];
    max_local_lda_  = max_ldim2();
    LT max_buffer_size = max_ldim1() * max_llda();
    mat_.Allocate(row_counts_[comm_[LAYER_ROW].id],
                  col_counts_[comm_[LAYER_COL].id], max_buffer_size);
  } else {
    printf("ERROR! Variation not supported yet.\n");
    exit(1);
  }
 
  // Opens MPI file.
  MPI_File f;
  long long int file_size_in_bytes, mat_size;
  MPI_File_open(MPI_COMM_WORLD, filename, MPI_MODE_RDONLY, MPI_INFO_NULL, &f);
  MPI_File_get_size(f, &file_size_in_bytes);
  mat_size = rows() * cols();
  assert(file_size_in_bytes - fpos_offset == mat_size * word_size);

  // Reads MPI file.
  MPI_Status status;
  MPI_Offset offset =
      (MPI_Offset)(fpos_offset + dim1_displs() * dim2() * word_size);
  MPI_File_seek(f, offset, MPI_SEEK_SET);
  MPI_File_read(f, mat_.values_, mat_.dim1() * mat_.dim2() * word_size,
                MPI_BYTE, &status);
  
  // Closes MPI file.
  MPI_File_close(&f);
}

template <class IT, class VT>
void DistDMat<IT, VT>::LoadNumPyTranspose(const char *filename) {
  // Reads properties.
  bool fortran_order = true;
  unsigned int *shape = NULL;
  unsigned int word_size = 0, ndims = 0;
  long long int fpos_offset = 0;
  if (rank() == 0) {
    parse_npy_header(filename, word_size, shape, ndims, fortran_order, fpos_offset);
    assert(ndims == 2);  // For now.
    assert(word_size == sizeof(VT));
  } else {
    shape = new unsigned int[2];
    word_size = sizeof(VT);
  }
  
  MPI_Bcast(shape, 2, MPI_UNSIGNED, 0, comm_[WORLD].comm);
  MPI_Bcast(&fpos_offset, 1, MPI_LONG_LONG, 0, comm_[WORLD].comm);
  MPI_Bcast(&fortran_order, 1, MPI_C_BOOL, 0, comm_[WORLD].comm);

  // Normal read works.
  if (layout() == ONED_BLOCK_ROW && mat_.format_ == DENSE_ROWMAJOR && fortran_order) {
    SetSize(shape[1], shape[0]);
    delete [] shape;
    
    // Allocates biggest possible size (max of all local matrices)
    // to avoid reallocation during Shift()/Bcast().
    // Index 0 always stores biggest size.
    max_local_rows_ = row_counts_[0];
    max_local_cols_ = col_counts_[0];
    max_local_lda_  = max_ldim2();
    LT max_buffer_size = max_ldim1() * max_llda();
    mat_.Allocate(row_counts_[comm_[LAYER_ROW].id],
                  col_counts_[comm_[LAYER_COL].id], max_buffer_size);
  } else {
    printf("ERROR! Variation not supported yet.\n");
    exit(1);
  }
 
  // Opens MPI file.
  MPI_File f;
  long long int file_size_in_bytes, mat_size;
  MPI_File_open(MPI_COMM_WORLD, filename, MPI_MODE_RDONLY, MPI_INFO_NULL, &f);
  MPI_File_get_size(f, &file_size_in_bytes);
  mat_size = rows() * cols();
  assert(file_size_in_bytes - fpos_offset == mat_size * word_size);

  // Reads MPI file.
  MPI_Status status;
  MPI_Offset offset =
      (MPI_Offset)(fpos_offset + (int64_t) dim1_displs() * (int64_t) dim2() * (int64_t) word_size);
  MPI_File_seek(f, offset, MPI_SEEK_SET);
  int max_mpi_read_size = 1 << 30;  // Must not exceed 32-bit integer
  int64_t read_size = (int64_t) mat_.dim1() * (int64_t) mat_.dim2() * (int64_t) word_size;
  VT *dest = mat_.values_;
  while (read_size > 0) {
    int actual_read_size = std::min((int64_t) max_mpi_read_size, read_size);
    int ierr  = MPI_File_read(f, dest, actual_read_size, MPI_BYTE, &status);
    
    assert(ierr == MPI_SUCCESS);
    read_size -= max_mpi_read_size;
    dest += actual_read_size / word_size;
  }
  
  // Closes MPI file.
  MPI_File_close(&f);
}

template <class IT, class VT>
void DistDMat<IT, VT>::Transpose(const DistDMat<IT, VT> &X) {
  if ((X.layout() == ONED_BLOCK_ROW && layout() == ONED_BLOCK_COLUMN) ||
      (X.layout() == ONED_BLOCK_COLUMN && layout() == ONED_BLOCK_ROW)) {
    if (X.mat_.format_ == mat_.format_) {
      CommonTranspose(X);
      mat_.Transpose(X.mat_, true);
    } else {
      printr("DistDMat::Transpose: ERROR! Format not supported\n");
      exit(1);
    }
  } else {
    printr("DistDMat::Transpose: ERROR! Layout not supported\n");
    exit(1);
  }
  // TODO(penpornk): Implement other cases.
}

template <class IT, class VT>
void DistDMat<IT, VT>::StoreElmtWise(std::function<VT(VT, VT)> fn,
                                     const DistDMat<IT, VT> &a,
                                     const DistSpMat<IT, VT> &b) {
  CommonCopy(a);
  mat_.StoreElmtWise(fn, a.mat_, b.mat_);
}

template <class IT, class VT>
void DistDMat<IT, VT>::Fill(IT global_rows, IT global_cols, VT val) {
  SetSize(global_rows, global_cols);
  max_local_rows_ = row_counts_[0];
  max_local_cols_ = col_counts_[0];
  max_local_lda_  = max_ldim2();
  LT max_buffer_size = max_ldim1() * max_llda();
  mat_.Allocate(row_counts_[comm_[LAYER_COL].rank],
                col_counts_[comm_[LAYER_ROW].rank], max_buffer_size);
  mat_.Fill(val);
}

template <class IT, class VT>
void DistDMat<IT, VT>::WeirdLayoutBlockColumn(const DistDMat<IT, VT> &X) {
  CommonCopy(X);
  assert(rows() == cols());  // Must be square.
  assert(layout() == ONED_BLOCK_COLUMN);
  
  // Find new local rows size
  int local_rows = 0;
  int c = layers();
  int nteams = teams();
  int nblocks= nteams / c;
  int blk_idx = (team_id() + layer_id()) % c;
  for (int i = 1; i <= nblocks; ++i) {
    local_rows += col_counts_[blk_idx];
    blk_idx += c;
  }
  
  // Allocation.
  mat_.Allocate(local_rows, col_counts_[team_id()]);
  
  // Goes block by block and set submatrix
  DMat<IT, VT> s;
  blk_idx = (team_id() + layer_id()) % c;
  int row_offset = 0;
  for (int i = 0; i < nblocks; ++i) {
    int rows = col_counts_[blk_idx];
    s.ShallowSubDMat(X.mat_, col_displs_[blk_idx], 0,
                             rows, mat_.cols_);
    mat_.SetSubDMat(s, row_offset, 0);
    row_offset += rows;
    blk_idx += c;
  }
}

template <class IT, class VT>
void DistDMat<IT, VT>::WeirdLayoutBlockRow(const DistDMat<IT, VT> &X) {
  CommonCopy(X);
  assert(rows() == cols());  // Must be square.
  assert(layout() == ONED_BLOCK_ROW);
  
  // Find new local rows size
  int local_cols = 0;
  int c = layers();
  int nteams = teams();
  int nblocks= nteams / c;
  int starting_blk_idx = (team_id() + c - (layer_id() % c)) % c;
  int blk_idx = starting_blk_idx;
  for (int i = 0; i < nblocks; ++i) {
    local_cols += row_counts_[blk_idx];
    blk_idx += c;
  }
  
  // Allocation.
  mat_.Allocate(row_counts_[team_id()], local_cols);
  
  // Goes block by block and set submatrix
  DMat<IT, VT> s;
  blk_idx = starting_blk_idx;
  int col_offset = 0;
  for (int i = 0; i < nblocks; ++i) {
    int cols = row_counts_[blk_idx];
    s.ShallowSubDMat(X.mat_, 0, row_displs_[blk_idx],
                             mat_.rows_, cols);
    mat_.SetSubDMat(s, 0, col_offset);
    col_offset += cols;
    blk_idx += c;
  }
}

template <class IT, class VT>
void DistDMat<IT, VT>::WeirdLayoutBlockRowRotatingB(const DistDMat<IT, VT> &X,
                                                    const DistDMat<IT, VT> &B) {
  CommonCopy(X);
  assert(rows() == cols());  // Must be square.
  assert(layout() == ONED_BLOCK_ROW);
  assert(X.layout() == ONED_BLOCK_ROW);
  assert(B.layout() == ONED_BLOCK_COLUMN);
  
  // Find new local rows size.
  int local_cols = 0;
  int c = layers();
  int nteams = B.teams();
  int nblocks = nteams / c;
  int blk_col_size = std::max(1, c / B.layers());
  int starting_blk_idx = (B.team_id() + layer_id() * blk_col_size) % c;
  
//  print("starting blk idx = %d (%d %d %d)\n", starting_blk_idx, B.team_id(), layer_id(), blk_col_size);
  int blk_idx = starting_blk_idx;
  for (int i = 0; i < nblocks; ++i) {
    local_cols += B.col_counts_[blk_idx];
    blk_idx += c;
  }
  
  // Allocation.
  mat_.Allocate(row_counts_[team_id()], local_cols);
  
  // Goes block by block and set submatrix.
  DMat<IT, VT> s;
  blk_idx = starting_blk_idx;
  int values_offset = 0;
  for (int i = 0; i < nblocks; ++i) {
    int cols = B.col_counts_[blk_idx];
    s.ShallowSubDMat(X.mat_, 0, B.col_displs_[blk_idx],
                             mat_.rows_, cols);
    DMat<IT, VT> output(s.rows_, s.cols_, mat_.format_,
                        mat_.values_ + values_offset);
    output.SetSubDMat(s, 0, 0);
    values_offset += cols * s.rows_;
    blk_idx += c;
  }
}

template <class IT, class VT>
void DistDMat<IT, VT>::WeirdLayoutBlockRowTransposed(const DistDMat<IT, VT> &X) {
  CommonCopy(X);
  assert(rows() == cols());  // Must be square.
  assert(layout() == ONED_BLOCK_ROW);
  
  // Find new local rows size
  int local_cols = 0;
  int c = layers();
  int nteams = teams();
  int nblocks= nteams / c;
  int starting_blk_idx = (team_id() + layer_id()) % c;
  int blk_idx = starting_blk_idx;
  for (int i = 0; i < nblocks; ++i) {
    local_cols += row_counts_[blk_idx];
    blk_idx += c;
  }
  
  // Allocation.
  mat_.Allocate(row_counts_[team_id()], local_cols);
  
  // Goes block by block and set submatrix
  DMat<IT, VT> s;
  blk_idx = starting_blk_idx;
  int col_offset = 0;
  for (int i = 0; i < nblocks; ++i) {
    int cols = row_counts_[blk_idx];
    s.ShallowSubDMat(X.mat_, 0, row_displs_[blk_idx],
                             mat_.rows_, cols);
    mat_.SetSubDMat(s, 0, col_offset);
    col_offset += cols;
    blk_idx += c;
  }
}

template <class IT, class VT>
void DistDMat<IT, VT>::print(const char *fmt, ...) {
  char *content;
  IT len = strlen(fmt);
  content = new char [len + 20];
  
  if (name_ == NULL)
    sprintf(content, "%d: %s", rank(), fmt);
  else
    sprintf(content, "%d: (%s) %s\n", rank(), name_, fmt);
  
  va_list args;
  va_start(args, fmt);
  vprintf(content, args);
  va_end(args);
  
  delete [] content;
}

template <class IT, class VT>
void DistDMat<IT, VT>::printr(const char *fmt, ...) {
  if (rank() != 0) return;
  char *content;
  IT len = strlen(fmt);
  content = new char [len + 20];
  
  if (name_ == NULL)
    sprintf(content, "%d: %s", rank(), fmt);
  else
    sprintf(content, "%d: (%s) %s\n", rank(), name_, fmt);
  
  va_list args;
  va_start(args, fmt);
  vprintf(content, args);
  va_end(args);
  
  delete [] content;
}


//
// Operations.
//
template <class IT, class VT>
void DistDMat<IT, VT>::SetSize(IT global_rows, IT global_cols) {
  global_rows_ = global_rows;
  global_cols_ = global_cols;
  
  // Finds row and column distribution.
  assert(row_counts_ != NULL && col_counts_ != NULL);
  gen_counts_displs_balanced(global_rows_, comm_[LAYER_COL].size,
                             row_counts_, row_displs_);
  gen_counts_displs_balanced(global_cols_, comm_[LAYER_ROW].size,
                             col_counts_, col_displs_);
  mat_.rows_ = row_counts_[comm_[LAYER_ROW].id];
  mat_.cols_ = col_counts_[comm_[LAYER_COL].id];
  mat_.lda_ = mat_.dim2();
  
  // Rank 0 always get the most count.
  max_local_rows_ = row_counts_[0];
  max_local_cols_ = col_counts_[0];
}

template <class IT, class VT>
void DistDMat<IT, VT>::CheckCommsAreInitialized() const {
  if (comm_ == NULL) {
    printf("DistDMat::rank() ERROR! Communicators aren't initialized yet.\n");
    exit(1);
  }
}

template <class IT, class VT>
void DistDMat<IT, VT>::ElmtWiseOp(std::function<VT(VT, VT)> fn, const VT &op) {
  mat_.ElmtWiseOp(fn, op);
}

template <class IT, class VT>
void DistDMat<IT, VT>::ElmtWiseOp(std::function<VT(VT, VT)> fn,
                                  const DistDMat<IT, VT> &Op) {
  mat_.ElmtWiseOp(fn, Op.mat_);
}

template <class IT, class VT>
void DistDMat<IT, VT>::ElmtWiseOp(std::function<VT(VT, VT)> fn,
                                  const DistSpMat<IT, VT> &Op) {
  mat_.ElmtWiseOp(fn, Op.mat_);
}

template <class IT, class VT>
void DistDMat<IT, VT>::ElmtWiseOpNzOnly(std::function<VT(VT, VT)> fn,
                                        const DistSpMat<IT, VT> &Op) {
  mat_.ElmtWiseOpNzOnly(fn, Op.mat_);
}

template <class IT, class VT>
void DistDMat<IT, VT>::WeirdLayoutDiagOp(std::function<VT(VT, VT)> fn,
                                         const DistSpMat<IT, VT> &D) {
  if (layer_id() != 0)
    return;
  IT tid = team_id();
  IT block_idx = tid / layers();
  IT col_offset = col_displs_[tid];
  SpMat<IT, VT> submat;
  submat.Submatrix(D.mat_, 0, col_offset, D.lrows(), lcols());
}

template <class IT, class VT>
void DistDMat<IT, VT>::SwapBuffers() {
  // To be used by Shift. Shift will handle rows_, cols_, and lda_.
  // format_ must be the same anyawy.
  // lib_, name_, and epsilon_ don't matter.
  std::swap(mat_.values_, tmp_mat_.values_);
  std::swap(mat_.buffer_size_, tmp_mat_.buffer_size_);
  std::swap(mat_.free_buffer_, tmp_mat_.free_buffer_);
}

template <class IT, class VT>
void DistDMat<IT, VT>::CommonCopy(const DistDMat<IT, VT> &X) {
  // comm_ and layout() must be set since in the constructor.
  global_rows_ = X.global_rows_;
  global_cols_ = X.global_cols_;
  
  max_local_rows_ = X.max_local_rows_;
  max_local_cols_ = X.max_local_cols_;
  max_local_lda_  = X.max_local_lda_;
  
  memcpy(row_counts_, X.row_counts_, comm_[LAYER_COL].size * sizeof(IT));
  memcpy(col_counts_, X.col_counts_, comm_[LAYER_ROW].size * sizeof(IT));
  memcpy(row_displs_, X.row_displs_, (comm_[LAYER_COL].size+1) * sizeof(IT));
  memcpy(col_displs_, X.col_displs_, (comm_[LAYER_ROW].size+1) * sizeof(IT));
  memcpy(distance_shifted_, X.distance_shifted_, NUM_COMMS * sizeof(IT));
  memcpy(owner_rank_, X.owner_rank_, NUM_COMMS * sizeof(IT));
  
  mpi_it_ = X.mpi_it_;
  mpi_lt_ = X.mpi_lt_;
  mpi_vt_ = X.mpi_vt_;
  
  mat_.StructuralCopy(X.mat_);
}

template <class IT, class VT>
void DistDMat<IT, VT>::CommonTranspose(const DistDMat<IT, VT> &X) {
    // comm_ and layout() must be set since in the constructor.
  global_rows_ = X.global_cols_;
  global_cols_ = X.global_rows_;
  
  max_local_rows_ = X.max_local_cols_;
  max_local_cols_ = X.max_local_rows_;
  max_local_lda_  = X.max_local_lda_;
  
  memcpy(row_counts_, X.col_counts_, comm_[LAYER_COL].size * sizeof(IT));
  memcpy(col_counts_, X.row_counts_, comm_[LAYER_ROW].size * sizeof(IT));
  memcpy(row_displs_, X.col_displs_, (comm_[LAYER_COL].size+1) * sizeof(IT));
  memcpy(col_displs_, X.row_displs_, (comm_[LAYER_ROW].size+1) * sizeof(IT));
  memcpy(distance_shifted_, X.distance_shifted_, NUM_COMMS * sizeof(IT));
  memcpy(owner_rank_, X.owner_rank_, NUM_COMMS * sizeof(IT));
  
  std::swap(distance_shifted_[LAYER_ROW], distance_shifted_[LAYER_COL]);
  std::swap(owner_rank_[LAYER_ROW], owner_rank_[LAYER_COL]);
  
  mpi_it_ = X.mpi_it_;
  mpi_lt_ = X.mpi_lt_;
  mpi_vt_ = X.mpi_vt_;
}

template <class IT, class VT>
void DistDMat<IT, VT>::DeepCopy(const DistDMat<IT, VT> &X) {
  CommonCopy(X);
  mat_.DeepCopy(X.mat_);
}

template <class IT, class VT>
void DistDMat<IT, VT>::ShallowCopy(const DistDMat<IT, VT> &X) {
  CommonCopy(X);
  mat_.ShallowCopy(X.mat_);
}

template <class IT, class VT>
void DistDMat<IT, VT>::TakeOwnership(DistDMat<IT, VT> &X) {
  mat_.TakeOwnership(X.mat_);
}

template <class IT, class VT>
void DistDMat<IT, VT>::MultiplyAdd(DistDMat<IT, VT> &B, DistDMat<IT, VT> &C) {
  if (layout() == ONED_BLOCK_ROW &&
      B.layout() == ONED_BLOCK_COLUMN &&
      C.layout() == ONED_BLOCK_COLUMN) {
      innerABC_shift1(*this, B, C);
  } else {
    printf("DistDMat::MultiplyAdd: Variation not supported right now.\n");
    exit(1);
  }
}

template <class IT, class VT>
VT DistDMat<IT, VT>::LocalTeamOpThenReduce(std::function<VT(VT)> fn,
                                           spdm3_op op) {
  int num_elmts = lrows() * lcols();
  int count = find_count_frontloaded(num_elmts, layers(), layer_id());
  int offset = find_offset_frontloaded(num_elmts, layers(), layer_id());
  int start_i, end_i, start_j, end_j;
  start_i = offset / lcols();
  start_j = offset % lcols();
  end_i = (offset + count) / lcols();
  end_j = (offset + count) % lcols();
  
  assert(op == SPDM3_SUM);
  VT res = 0.0;
  for (int j = start_j; j < lcols(); ++j)
    res += fn(mat_.values_[start_i * mat_.lda_ + j]);
  #pragma omp parallel for schedule(guided) reduction(+ : res)
  for (int i = start_i + 1; i < end_i; ++i) {
    for (int j = 0; j < lcols(); ++j)
      res += fn(mat_.values_[i * mat_.lda_ + j]);
  }
  for (int j = 0; j < end_j; ++j) {
    res += fn(mat_.values_[end_i * mat_.lda_ + j]);
  }
  return res;
}

template <class IT, class VT>
VT DistDMat<IT, VT>::Checksum(spdm3_comm comm) {
  VT temp = mat_.Reduce(SPDM3_SUM);
  MPI_Allreduce(MPI_IN_PLACE, &temp, 1, mpi_vt_, MPI_SUM, comm_[comm].comm);
  return temp;
}

template <class IT, class VT>
VT DistDMat<IT, VT>::LocalTrace() {
  VT trace = 0.0;
  if (lrows() < lcols()) {
    for (IT i = 0; i < lrows(); ++i) {
      IT global_row_id = i + row_displs();
      IT j = global_row_id - col_displs();
      if (mat_.format_ == DENSE_ROWMAJOR) {
        trace += mat_.values_[i * mat_.lda_ + j];
      } else {
        trace += mat_.values_[i + mat_.lda_ * j];
      }
    }
  } else {
    for (IT j = 0; j < lcols(); ++j) {
      IT global_col_id = j + col_displs();
      IT i = global_col_id - row_displs();
      if (mat_.format_ == DENSE_ROWMAJOR)  {
        trace += mat_.values_[i * mat_.lda_ +j];
      } else {
        trace += mat_.values_[i + mat_.lda_ * j];
      }
    }
  }
  return trace;
}

//
// Communication.
//
//
template <class IT, class VT>
void DistDMat<IT, VT>::Shift(const spdm3_comm &comm, const IT &distance) {
  if (distance == 0)
    return;
    
  int src = (comm_[comm].rank + comm_[comm].size + distance) % comm_[comm].size;
  int dest = (comm_[comm].rank + comm_[comm].size - distance) % comm_[comm].size;
  
  distance_shifted_[comm] =
      (distance_shifted_[comm] + comm_[comm].size + distance) % comm_[comm].size;
  owner_rank_[comm] =
      (owner_rank_[comm] + comm_[comm].size + distance) % comm_[comm].size;
  
  // Sends data.
  MPI_Request mpi_send_request;
  MPI_Isend(mat_.values_, mat_.dim1() * mat_.lda_, mpi_vt_,
            dest, 0, comm_[comm].comm, &mpi_send_request);
  
  // Receives data. Expands buffer if necessary.
  LT max_buffer_size = max_ldim1() * max_ldim2();  // Assuming ldim2() == llda() for now.
  tmp_mat_.ExpandBuffer(max_buffer_size, false);
  MPI_Status mpi_stats;
  MPI_Recv(tmp_mat_.values_, max_buffer_size, mpi_vt_,
           src, 0, comm_[comm].comm, &mpi_stats);
  
  // Waits for Isend to be done.
  MPI_Wait(&mpi_send_request, &mpi_stats);
  SwapBuffers();
  
  // Figures out the new #cols and #rows.
  switch (comm) {
    case LAYER_ROW:
      mat_.cols_ = col_counts_[owner_rank_[comm]];
      break;
    case LAYER_COL:
      mat_.rows_ = row_counts_[owner_rank_[comm]];
      break;
    default:
      if (layout() == ONED_BLOCK_ROW)
        mat_.rows_ = row_counts_[owner_rank_[comm]];
      else
        mat_.cols_ = col_counts_[owner_rank_[comm]];
      break;
  }
  mat_.lda_ = mat_.dim2();
}

template <class IT, class VT>
void DistDMat<IT, VT>::ShiftAsync(const spdm3_comm &comm, const IT &distance) {
  if (distance == 0)
    return;
    
  int src = (comm_[comm].rank + comm_[comm].size + distance) % comm_[comm].size;
  int dest = (comm_[comm].rank + comm_[comm].size - distance) % comm_[comm].size;
  
  distance_shifted_[comm] =
      (distance_shifted_[comm] + comm_[comm].size + distance) % comm_[comm].size;
  owner_rank_[comm] =
      (owner_rank_[comm] + comm_[comm].size + distance) % comm_[comm].size;

  // Sends data.
  shift_dir_ = comm;
  MPI_Isend(mat_.values_, mat_.dim1() * mat_.lda_, mpi_vt_,
            dest, 0, comm_[comm].comm, &mpi_send_req_);
  
  // Receives data. Expands buffer if necessary.
  LT max_buffer_size = max_ldim1() * max_ldim2();  // Assuming ldim2() == llda() for now.
  tmp_mat_.ExpandBuffer(max_buffer_size, false);
  
  MPI_Irecv(tmp_mat_.values_, max_buffer_size, mpi_vt_,
           src, 0, comm_[comm].comm, &mpi_recv_req_);
}

template <class IT, class VT>
void DistDMat<IT, VT>::ShiftWait() {
  // Does nothing if we aren't shifting.
  if (shift_dir_ == NOT_SHIFTING) {
//    print("returning\n");
    return;
  }
  
//  print("I got here\n");
  
  // Blocks for MPI_Irecv and MPI_Isend earlier.
  MPI_Wait(&mpi_recv_req_, MPI_STATUS_IGNORE);
  MPI_Wait(&mpi_send_req_, MPI_STATUS_IGNORE);
  
  // Makes the content received the real content.
  SwapBuffers();
  
  // Figures out the new #cols and #rows.
  switch (shift_dir_) {
    case LAYER_ROW:
      mat_.cols_ = col_counts_[owner_rank_[shift_dir_]];
      break;
    case LAYER_COL:
      mat_.rows_ = row_counts_[owner_rank_[shift_dir_]];
      break;
    default:
      if (layout() == ONED_BLOCK_ROW)
        mat_.rows_ = row_counts_[owner_rank_[shift_dir_]];
      else
        mat_.cols_ = col_counts_[owner_rank_[shift_dir_]];
      break;
  }
  mat_.lda_ = mat_.dim2();
  
  // Officially states that shifting is done.
  shift_dir_ = NOT_SHIFTING;
}

template <class IT, class VT>
void DistDMat<IT, VT>::Bcast(const spdm3_comm &comm, const IT &root) {
  // Figures out the rank in layer
  // to get #rows and #cols of the broadcasting matrix.
  int row_id, col_id;  // comm_[LAYER]'s rank.
  switch(comm) {
    case TEAM:
      GetRowColIdFromLayerRank(comm_, layout(), comm_[comm].id,
                               &row_id, &col_id);
      break;
    case LAYER:
      GetRowColIdFromLayerRank(comm_, layout(), root, &row_id, &col_id);
      break;
    case LAYER_ROW:
      row_id = comm_[comm].id;
      col_id = root;
      break;
    case LAYER_COL:
      row_id = root;
      col_id = comm_[comm].id;
      break;
  }
  
  // Expands buffer if too small.
  // Assumes lda = dim2.
  IT new_rows = row_counts_[row_id];
  IT new_cols = col_counts_[col_id];
  LT buffer_size = new_rows * new_cols;
  if (comm_[comm].rank != root)
    mat_.ExpandBuffer(buffer_size, false);
  
  // Broadcasts.
  MPI_Bcast(mat_.values_, buffer_size, mpi_vt_, root, comm_[comm].comm);
  
  // Updates matrix size.
  mat_.rows_ = new_rows;
  mat_.cols_ = new_cols;
  mat_.lda_  = mat_.dim2();
}

template <class IT, class VT>
void DistDMat<IT, VT>::Reduce(const spdm3_comm &comm, const IT &root) {
  // Supports only comm == TEAM for now.
  assert(comm == TEAM);
  
  // Reduces in-place.
  if (comm_[comm].rank == root) {
    MPI_Reduce(MPI_IN_PLACE, mat_.values_, mat_.dim1() * mat_.lda_,
               mpi_vt_, MPI_SUM, root, comm_[comm].comm);
  } else {
    MPI_Reduce(mat_.values_, NULL, mat_.dim1() * mat_.lda_,
               mpi_vt_, MPI_SUM, root, comm_[comm].comm);
  }
}

template <class IT, class VT>
void DistDMat<IT, VT>::Allreduce(const spdm3_comm &comm) {
  // Supports only comm == TEAM for now.
  assert(comm == TEAM);
  
  // Reduces in-place.
  MPI_Allreduce(MPI_IN_PLACE, mat_.values_, mat_.dim1() * mat_.lda_,
             mpi_vt_, MPI_SUM, comm_[comm].comm);
}

template <class IT, class VT>
void DistDMat<IT, VT>::ResetShift(const spdm3_comm &comm) {
  Shift(comm, -distance_shifted_[comm]);
  assert(distance_shifted_[comm] == 0);
  assert(owner_rank_[comm] == comm_[comm].rank);
}

template <class IT, class VT>
void DistDMat<IT, VT>::ResetShifts() {
  ResetShift(LAYER);
  ResetShift(LAYER_ROW);
  ResetShift(LAYER_COL);
}

// Getters.
//
template <class IT, class VT>
IT DistDMat<IT, VT>::rank() const {
  CheckCommsAreInitialized();
  return comm_[WORLD].rank;
}

template <class IT, class VT>
IT DistDMat<IT, VT>::nprocs() const {
  CheckCommsAreInitialized();
  return comm_[WORLD].size;
}

template <class IT, class VT>
IT DistDMat<IT, VT>::rows() const {
  return global_rows_;
}

template <class IT, class VT>
IT DistDMat<IT, VT>::cols() const {
  return global_cols_;
}

template <class IT, class VT>
IT DistDMat<IT, VT>::row_displs() const {
  return row_displs_[comm_[LAYER_ROW].id];
}

template <class IT, class VT>
IT DistDMat<IT, VT>::col_displs() const {
  return col_displs_[comm_[LAYER_COL].id];
}

template <class IT, class VT>
IT DistDMat<IT, VT>::dim1() const {
  switch (mat_.format_) {
    case DENSE_ROWMAJOR: return global_rows_;
    case DENSE_COLMAJOR: return global_cols_;
    default:             return global_rows_;
  }
}

template <class IT, class VT>
IT DistDMat<IT, VT>::dim2() const {
  switch (mat_.format_) {
    case DENSE_ROWMAJOR: return global_cols_;
    case DENSE_COLMAJOR: return global_rows_;
    default:             return global_cols_;
  }
}

template <class IT, class VT>
IT DistDMat<IT, VT>::dim1_displs() const {
  CheckCommsAreInitialized();
  switch (mat_.format_) {
    case DENSE_ROWMAJOR: return row_displs_[comm_[LAYER_COL].rank];
    case DENSE_COLMAJOR: return col_displs_[comm_[LAYER_ROW].rank];
    default:             return row_displs_[comm_[LAYER_COL].rank];
  }
}

template <class IT, class VT>
IT DistDMat<IT, VT>::dim2_displs() const {
  CheckCommsAreInitialized();
  switch (mat_.format_) {
    case DENSE_ROWMAJOR: return col_displs_[comm_[LAYER_ROW].rank];
    case DENSE_COLMAJOR: return row_displs_[comm_[LAYER_COL].rank];
    default:             return col_displs_[comm_[LAYER_ROW].rank];
  }
}

template <class IT, class VT>
IT DistDMat<IT, VT>::lrows() const {
  return mat_.rows_;
}

template <class IT, class VT>
IT DistDMat<IT, VT>::lcols() const {
  return mat_.cols_;
}

template <class IT, class VT>
IT DistDMat<IT, VT>::llda() const {
  return mat_.lda_;
}

template <class IT, class VT>
IT DistDMat<IT, VT>::max_ldim1() const {
  switch (mat_.format_) {
    case DENSE_ROWMAJOR: return max_local_rows_;
    case DENSE_COLMAJOR: return max_local_cols_;
    default:             return max_local_rows_;
  }
}

template <class IT, class VT>
IT DistDMat<IT, VT>::max_ldim2() const {
  switch (mat_.format_) {
    case DENSE_ROWMAJOR: return max_local_cols_;
    case DENSE_COLMAJOR: return max_local_rows_;
    default:             return max_local_cols_;
  }
}

template <class IT, class VT>
IT DistDMat<IT, VT>::max_llda() const {
  return max_local_lda_;
}

template <class IT, class VT>
IT DistDMat<IT, VT>::lrank() const {
  CheckCommsAreInitialized();
  return comm_[LAYER].rank;
}

template <class IT, class VT>
IT DistDMat<IT, VT>::trank() const {
  CheckCommsAreInitialized();
  return comm_[TEAM].rank;
}

template <class IT, class VT>
IT DistDMat<IT, VT>::layer_id() const {
  CheckCommsAreInitialized();
  return comm_[LAYER].id;
}

template <class IT, class VT>
IT DistDMat<IT, VT>::team_id() const {
  CheckCommsAreInitialized();
  return comm_[TEAM].id;
}

template <class IT, class VT>
IT DistDMat<IT, VT>::layers() const {
  return comm_[TEAM].size;
}

template <class IT, class VT>
IT DistDMat<IT, VT>::teams() const {
  return comm_[LAYER].size;
}

template <class IT, class VT>
spdm3_layout DistDMat<IT, VT>::layout() const {
  return comm_[WORLD].layout;
}

//
// Communication.
//
template <class IT, class VT>
DMat<IT, VT> *DistDMat<IT, VT>::GetLocalDMatsFromComm(const Comm& comm) {
  IT *row_counts, *row_displs;
  IT *col_counts, *col_displs;
  
  // Must be int because of Allgatherv.
  int *recvcounts, *recvdispls;
  
  // Allocation.
  row_counts = new IT[comm.size];
  col_counts = new IT[comm.size];
  recvcounts = new int[comm.size];
  recvdispls = new int[comm.size + 1];
  
  // Gets row and column counts.
  MPI_Allgather(&(mat_.rows_), 1, mpi_it_,
                row_counts, 1, mpi_it_, comm.comm);
  MPI_Allgather(&(mat_.cols_),  1, mpi_it_,
                col_counts, 1, mpi_it_, comm.comm);
  
  // Calculates receive counts and displacements.
  for (int i = 0; i < comm.size; ++i)
    recvcounts[i] = row_counts[i] * col_counts[i];
  gen_displs_from_counts(comm.size, recvcounts, recvdispls);
  
  // Allocates a buffer and gathers.
  VT *values = new VT[recvdispls[comm.size]];
  MPI_Allgatherv(mat_.values_, lrows() * lcols(), mpi_vt_,
                values, recvcounts, recvdispls, mpi_vt_, comm.comm);
  
  // Creates array of DMats.
  IT *lda = (mat_.format_ == DENSE_ROWMAJOR)? col_counts : row_counts;
  DMat<IT, VT> *M = new DMat<IT, VT>[comm.size];
  for (int i = 0; i < comm.size; ++i) {
    M[i].PointerToDMat(row_counts[i], col_counts[i], lda[i],
                       mat_.format_, values + recvdispls[i], (i == 0));
  }
  
  // Deallocation.
  delete [] row_counts;
  delete [] col_counts;
  delete [] recvcounts;
  delete [] recvdispls;
  
  return M;
}

template <class IT, class VT>
DMat<IT, VT> DistDMat<IT, VT>::GetWholeMatrix() {
  ResetShifts();
  DMat<IT, VT> *M = GetLocalDMatsFromComm(comm_[LAYER]);
  DMat<IT, VT> mat(M, comm_[LAYER_COL].size, comm_[LAYER_ROW].size, layout());
  return mat;
}

template <class IT, class VT>
void DistDMat<IT, VT>::Replicate(Comm* new_comm) {
  DMat<IT, VT> *M = GetLocalDMatsFromComm(new_comm[TEAM]);
  mat_.Concatenate(M, new_comm[TEAM_COL].size, new_comm[TEAM_ROW].size, new_comm[WORLD].layout);
  
  // Updates counts and displs.
  int team_rows = new_comm[LAYER_COL].size;
  int team_cols = new_comm[LAYER_ROW].size;
  int crows = comm_[LAYER_COL].size / team_rows;
  int ccols = comm_[LAYER_ROW].size / team_cols;
  
  // Row counts.
  for (int i = 0; i < team_rows; ++i) {
    int ic = i * crows;
    row_counts_[i] = row_counts_[ic];
    row_displs_[i] = row_displs_[ic];
    for (int j = 1; j < crows; ++j)
      row_counts_[i] += row_counts_[ic + j];
    
    // Updates max local rows.
    if (row_counts_[i] > max_local_rows_)
      max_local_rows_ = row_counts_[i];
  }
  row_displs_[team_rows] = row_displs_[comm_[LAYER_COL].size];
  
  // Col counts.
  for (int i = 0; i < team_cols; ++i) {
    int ic = i * ccols;
    col_counts_[i] = col_counts_[ic];
    col_displs_[i] = col_displs_[ic];
    for (int j = 1; j < ccols; ++j)
      col_counts_[i] += col_counts_[ic + j];
    
    // Updates max local cols.
    if (col_counts_[i] > max_local_cols_)
      max_local_cols_ = col_counts_[i];
  }
  col_displs_[team_cols] = col_displs_[comm_[LAYER_ROW].size];

  // Updates communicators.
  comm_ = new_comm;
  
  // Reset counters.
  fill_vec<IT>(distance_shifted_, NUM_COMMS, 0);
  for (int i = 0; i < NUM_COMMS; ++i)
    owner_rank_[i] = comm_[i].rank;
}

}  // namespace spdm3
