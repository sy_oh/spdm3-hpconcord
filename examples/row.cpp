//
// Very simple 1.5D all-block-row sparse-dense matmul example
// (This variant is not mentioned in the paper
// "Communication-Avoiding Sparse-Dense Matrix-Matrix Multiplication"
// because it rotates the dense matrix B which is expensive.
// It can still be useful if A is dense, i.e., change A to DistDMat.)
// A, B, C all have block row layout.
// B is rotated to compute the multiplication.
//
#include <stdio.h>
#include <mpi.h>
#include "common.h"  // To read parameters.
#include "spdm3.h"   // SpDM^3.

// Defines the data types to use.
#define IT  int
#define VT  double

using namespace spdm3;

//
// Performs C = A * B where A is sparse, B and C are dense.
// All A, B, and C are n-by-n. A and B are generated.
//
int main(int argc, char *argv[]) {

  // Parses command-line parameters.
  int m, k, n;          // Matrix dimensions (when generated).
  int cA, cB;           // Replication factors of A and B.
  double percent_nnz;   // Percent nonzeroes of A (when generated).
  char *filename_A;     // Input filename for A.
  char *filename_B;     // Input filename for B.
  char *filename_C;     // Output filename for C.
  read_params(argc, argv, m, k, n, cA, cB, percent_nnz,
              filename_A, filename_B, filename_C);
  
  // Declares Timer.
  Timer T;
  
  // Initializations.
  MPI_Init(&argc, &argv);     // Initializes MPI.
  T.Init(MPI_COMM_WORLD);     // Initializes Timer.
  print_help(argc, argv, T);  // Prints help and terminates if "-h"
  
  // Creates SpDM^3 communicators:
  // 1D block-row layout with replication factors cA and cB.
  // A and C are partitioned into submatrices of size cA*n/p-by-n.
  // B is partitioned into submatrices of size cB*n/p-by-n.
  // (p is the number of processors.)
  Comm *commA = GetComms(MPI_COMM_WORLD, ONED_BLOCK_ROW, cA);
  Comm *commB = GetComms(MPI_COMM_WORLD, ONED_BLOCK_ROW, cB);
  
  // Declares the matrices.
  DistSpMat<IT, VT> A(commA);
  DistDMat<IT, VT>  B(commB);
  DistDMat<IT, VT>  C(commA);
  
  // Have to read both inputs first before generating
  // unsupplied matrix/matrices to set the appropriate m, n, k.
  // Reads A/B if not null.
  if (filename_A != NULL) {
    A.LoadMatrixMarket(filename_A);
    m = A.global_rows_;  // Updates m and k to be
    k = A.global_cols_;  // consistent with the actual dimensionality.
  }
  if (filename_B != NULL) {
    B.LoadNumPy(filename_B);
    n = B.global_cols_;  // Updates n to be consistent with the actual matrix.
    
    // #rows of B must match #columns of A. Abort if not.
    assert (filename_A == NULL || k == B.global_rows_);
    k = B.global_rows_;  // Sets k in case A == NULL.
  }
  
  // Generates A if null.
  if (filename_A == NULL) {
    A.GenerateUniform(m, k, percent_nnz/100.0);
  }
  if (filename_B == NULL) {
    B.GenerateWithFunction(k, n);
  }
  
  // Multiplies the matrices.
  // The routine put some timing info in the Timer T.
  MMRow(A, B, C, T);
  
  // Saving matrices:
  // DistSpMat has a distributed save function for block row layout.
  // For efficiency, each process writes to a separate file and
  // the user has to concatenate the files themselves.
  // The command below will create files A-00000, A-00001, etc.
  // To get the final, single Matrix Market file, run 'cat A-* > A.mtx'.
  A.SaveMatrixMarket("A");
  
  // Currently there is no distributed save function for DistDMat.
  // The user has to gather the matrix in a process first then
  // calls a local DMat::Save function.
  DMat<IT, VT> wholeB = B.GetWholeMatrix();
  DMat<IT, VT> wholeC = C.GetWholeMatrix();
  wholeB.SaveNumPy("B.npy");
  wholeC.SaveNumPy(filename_C);
  
  // Note: One can also do the same with A.
  // Use the following commands to gather A in a process and save it locally.
  // SpMat<IT, VT> wholeA = A.GetWholeMatrix();
  // wholeA.SaveMatrixMarket("A.mtx");
  
  // Checks the results by doing local multiplication.
  SpMat<IT, VT> wholeA = A.GetWholeMatrix();
  DMat<IT, VT> answer;
  wholeA.MultiplyAdd(wholeB, answer);
  
  // T.printr makes only one process print.
  // Usage is the same as printf.
  if (wholeC.IsEquivalent(answer)) {
    T.printr("The multiplication is correct!\n");
  } else {
    T.printr("NOOO! The results are wrong. Please report bug.\n");
  }
  
  // Finalizations.
  MPI_Barrier(MPI_COMM_WORLD);
  T.printr("%s: m = %d, k = %d, n = %d, percent_nnz = %f, "
           "p = %d, cA = %d, cB = %d\n"
           "A = %s, B = %s, c = %s\n",
           argv[0], m, k, n, percent_nnz,
           A.nprocs(), cA, cB,
           filename_A, filename_B, filename_C);
  
  // Reports timings.
  T.Report();
  
  delete [] commA;
  delete [] commB;
  MPI_Finalize();
  return 0;
}
