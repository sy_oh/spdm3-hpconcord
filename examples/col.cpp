//
// Very simple 1.5D all-block-column sparse-dense matmul example
// (1.5D ColABC in the paper
// "Communication-Avoiding Parallel Sparse-Dense Matrix-Matrix Multiplication".)
// A, B, C all have block column layouts.
// A is rotated to compute the multiplication.
// cA is the replication factor of A.
// cB is the replication factor of B and C.
// Use cB = 1 to get 1.5D ColA.
//
#include <stdio.h>
#include <mpi.h>
#include "common.h"  // To read parameters.
#include "spdm3.h"   // SpDM^3.

// Defines the data types to use.
#define IT  int
#define VT  double

using namespace spdm3;

//
// Performs C = A * B where A is sparse, B and C are dense.
// A is m-by-k. B is k-by-n. C is m-by-n.
// A and B are generated or read from files.
//
int main(int argc, char *argv[]) {

  // Parses command-line parameters.
  int m, k, n;          // Matrix dimensions (when generated).
  int cA, cB;           // Replication factors of A and B.
  double percent_nnz;   // Percent nonzeroes of A (when generated).
  char *filename_A;     // Input filename for A.
  char *filename_B;     // Input filename for B.
  char *filename_C;     // Output filename for C.
  read_params(argc, argv, m, k, n, cA, cB, percent_nnz,
              filename_A, filename_B, filename_C);
  
  // Declares Timer.
  Timer T;
  
  // Initializations.
  MPI_Init(&argc, &argv);     // Initializes MPI.
  T.Init(MPI_COMM_WORLD);     // Initializes Timer.
  print_help(argc, argv, T);  // Prints help and terminates if "-h"
  
  // Creates SpDM^3 communicators:
  // 1D block-column layout with replication factors cA and cB.
  // A is partitioned into submatrices of size m-by-cA*k/p.
  // B is partitioned into submatrices of size k-by-cB*n/p.
  // C is partitioned into submatrices of size m-by-cB*n/p.
  // (p is the number of processors.)
  Comm *commA = GetComms(MPI_COMM_WORLD, ONED_BLOCK_COLUMN, cA);
  Comm *commB = GetComms(MPI_COMM_WORLD, ONED_BLOCK_COLUMN, cB);
  
  // Declares the matrices.
  DistSpMat<IT, VT> A(commA);
  DistDMat<IT, VT>  B(commB);
  DistDMat<IT, VT>  C(commB);
  
  // Have to read both inputs first before generating
  // unsupplied matrix/matrices to set the appropriate m, n, k.
  // Reads A/B if not null.
  if (filename_A != NULL) {
    A.LoadMatrixMarket(filename_A);
    m = A.global_rows_;  // Updates m and k to be
    k = A.global_cols_;  // consistent with the actual dimensionality.
  }
  if (filename_B != NULL) {
    B.LoadNumPy(filename_B);
    n = B.global_cols_;  // Updates n to be consistent with the actual matrix.
    
    // #rows of B must match #columns of A. Abort if not.
    assert (filename_A == NULL || k == B.global_rows_);
    k = B.global_rows_;  // Sets k in case A == NULL.
  }
  
  // Generates A if null.
  if (filename_A == NULL) {
    A.GenerateUniform(m, k, percent_nnz/100.0);
  }
  if (filename_B == NULL) {
    B.GenerateWithFunction(k, n);
  }
  
  // Multiplies the matrices.
  // The routine put some timing info in the Timer T.
  MMCol(A, B, C, T);

  // Saving matrices:
  // There is no distributed save function for DistSpMat with block column layout.
  // Simply gather all matrix parts to a single process and save locally.
  // SaveMatrixMarket saves the sparse matrix in the Matrix Market format.
  SpMat<IT, VT> wholeA = A.GetWholeMatrix();
  wholeA.SaveMatrixMarket("A.mtx");

  // Currently there is no distributed save function for DistDMat.
  // The user has to gather the matrix in a process first then
  // calls a local DMat::Save function.
  // SaveNumPy saves the matrix in the Python NumPy format.
  DMat<IT, VT> wholeB = B.GetWholeMatrix();
  DMat<IT, VT> wholeC = C.GetWholeMatrix();
  wholeB.SaveNumPy("B.npy");
  wholeC.SaveNumPy(filename_C);
  
  // Checks the results by doing local multiplication.
  DMat<IT, VT> answer;
  wholeA.MultiplyAdd(wholeB, answer);
  
  // T.printr makes only one process print.
  // Usage is the same as printf.
  if (wholeC.IsEquivalent(answer)) {
    T.printr("The multiplication is correct!\n");
  } else {
    T.printr("NOOO! The results are wrong. Please report bug.\n");
  }

  // Finalizations.
  MPI_Barrier(MPI_COMM_WORLD);
  T.printr("%s: m = %d, k = %d, n = %d, percent_nnz = %f, "
           "p = %d, cA = %d, cB = %d\n"
           "A = %s, B = %s, c = %s\n",
           argv[0], m, k, n, percent_nnz,
           A.nprocs(), cA, cB,
           filename_A, filename_B, filename_C);

  // Reports timings.
  T.Report();
  
  delete [] commA;
  delete [] commB;
  MPI_Finalize();
  return 0;
}
