#include "dmat.h"

#include <functional>
#include <iomanip>
#include <iostream>
#include <list>
#include <string>
#include <utility>

#include "spmat.h"

#define IT  int
#define VT  double

namespace spdm3 {

bool TestDefaultConstructor() {
  DMat<IT, VT> a;
  return true;
}

bool TestFill() {
  DMat<IT, VT> a(5, 5, DENSE_ROWMAJOR);
  a.Fill(55);
  if (a.rows_ != 5 || a.cols_ != 5) return false;
  for (int i = 0; i < a.rows_; ++i) {
    for (int j = 0; j < a.cols_; ++j)
      if (a.values_[i] != 55) return false;
  }
  return true;
}

bool TestFillSeries() {
  DMat<IT, VT> a(5, 5, DENSE_ROWMAJOR);
  VT start = 1.0;
  a.FillSeries(start);
  for (IT i = 0; i < a.dim1(); ++i) {
    for (IT j = 0; j < a.dim2(); ++j) {
      if (a.values_[i * a.lda_ + j] != start + i * a.lda_ + j)
        return false;
    }
  }
  return true;
}

bool TestGenerate() {
  DMat<IT, VT> a(7, 7, DENSE_ROWMAJOR);
  a.Generate();
  if (a.rows_ != 7 || a.cols_ != 7) return false;
  for (int i = 0; i < a.rows_; ++i) {
    for (int j = 0; j < a.cols_; ++j)
      if (a.values_[i * a.lda_ + j] == 0) return false;
  }
  return true;
}

bool TestIO() {
  DMat<IT, VT> a(11, 11, DENSE_ROWMAJOR);
  a.Generate();
  a.Save("A");
  DMat<IT, VT> b;
  b.Load("A");
  system("rm -f A");
  return b.IsEquivalent(a);
}

bool TestElmtWiseOp() {
  // Element-wise multiplication by a scalar
  DMat<IT, VT> a(13, 13, DENSE_ROWMAJOR);
  a.Fill(5.0);
  a.ElmtWiseOp([](VT a, VT b)->VT { return a * b; }, 7 );
  
  // Constructs answer.
  DMat<IT, VT> b(13, 13, DENSE_ROWMAJOR);
  b.Fill(35.0);
  if (!a.IsEquivalent(b))
    return false;
 
  // Element-wise addition by a DMat.
  a.ElmtWiseOp([](VT a, VT b)->VT { return a + b; }, b);
  DMat<IT, VT> c(13, 13, DENSE_ROWMAJOR);
  c.Fill(70.0);
  return a.IsEquivalent(c);
}

bool TestSetSubDMat() {
  VT mat[]    = { 1, 2, 3,
                  4, 5, 6,
                  7, 8, 9 };
  VT submat[] = { 1, 2,
                  3, 4 };
  VT newmat[] = { 1, 2, 3,
                  4, 1, 2,
                  7, 3, 4 };
  
  DMat<IT, VT> a(3, 3, DENSE_ROWMAJOR, mat);
  DMat<IT, VT> b(2, 2, DENSE_ROWMAJOR, submat);
  DMat<IT, VT> c(3, 3, DENSE_ROWMAJOR, newmat);
  
  a.SetSubDMat(b, 1, 1);
  return a.IsEquivalent(c);
}

bool TestSubDMat() {
  VT mat[]    = { 1, 2, 3, 10,
                  4, 5, 6, 11,
                  7, 8, 9, 12,
                  13, 14, 15, 16};
  VT submat[] = { 5, 6,
                  8, 9 };
  
  DMat<IT, VT> a(4, 4, DENSE_ROWMAJOR, mat);
  DMat<IT, VT> b(2, 2, DENSE_ROWMAJOR, submat);
  DMat<IT, VT> c;

  c.SubDMat(a, 1, 1, 2, 2);
  return b.IsEquivalent(c);
}

bool TestShallowSubDMat() {
  VT mat[]    = { 1, 2, 3, 10,
                  4, 5, 6, 11,
                  7, 8, 9, 12,
                  13, 14, 15, 16};
  VT submat[] = { 5, 6,
                  8, 9 };
  
  DMat<IT, VT> a(4, 4, DENSE_ROWMAJOR, mat);
  DMat<IT, VT> b(2, 2, DENSE_ROWMAJOR, submat);
  DMat<IT, VT> c;

  c.ShallowSubDMat(a, 1, 1, 2, 2);
  return b.IsEquivalent(c);
}

bool TestMultiplyAdd() {
  VT mat[] = { 1, 2, 3,
               4, 5, 6,
               7, 8, 9 };
  VT res[] = { 30, 36, 42,
               66, 81, 96,
               102, 126, 150 };
  
  DMat<IT, VT> a(3, 3, DENSE_ROWMAJOR, mat);
  DMat<IT, VT> b(3, 3, DENSE_ROWMAJOR, mat);
  DMat<IT, VT> c(3, 3, DENSE_ROWMAJOR);
  DMat<IT, VT> d(3, 3, DENSE_ROWMAJOR, res);
  
  c.Fill(0.0);
  a.MultiplyAdd(b, c);
  return c.IsEquivalent(d);
}

bool TestTranspose() {
  VT mat[] = { 1, 2, 3,
               4, 5, 6 };
  VT transposed [] = { 1, 4,
                      2, 5,
                      3, 6 };
  DMat<IT, VT> a(2, 3, DENSE_ROWMAJOR, mat);
  DMat<IT, VT> b;
  b.Transpose(a);
  DMat<IT, VT> c(3, 2, DENSE_ROWMAJOR, transposed);
  return c.IsEquivalent(b);
}

bool TestConvertFromCSR() {
  VT mat[] = { 1, 0, 3,
               4, 5, 0,
               7, 0, 9,
               0, 8, 0 };
  LT heads[] = {0, 2, 4, 6, 7};
  LT indices[] = {0, 2, 0, 1, 0, 2, 1};
  VT values[]  = {1, 3, 4, 5, 7, 9, 8};
  SpMat<IT, VT> a;
  a.Set(4, 3, 7,
        5, heads,
        7, indices, values, false);
  
  DMat<IT, VT> b(a);
  DMat<IT, VT> c(4, 3, DENSE_ROWMAJOR, mat);
  return c.IsEquivalent(b);
}

bool TestElmtWiseOpWithSpMat() {
  VT mat[] = { 9, 10, 7,
               6,  5, 10,
               3, 10, 1,
               10, 2, 10 };
  LT heads[] = {0, 2, 4, 6, 7};
  LT indices[] = {0, 2, 0, 1, 0, 2, 1};
  VT values[]  = {1, 3, 4, 5, 7, 9, 8};
  DMat<IT, VT> a(4, 3, DENSE_ROWMAJOR, mat);
  SpMat<IT, VT> b;
  b.Set(4, 3, 7,
        5, heads,
        7, indices, values, false);
  a.ElmtWiseOp([] (VT x, VT y) -> VT { return x + y; }, b);
  DMat<IT, VT> c(4, 3, DENSE_ROWMAJOR);
  c.Fill(10.0);
  return c.IsEquivalent(a);
}

bool TestConcatenate() {
  DMat<IT, VT> *mats = new DMat<IT, VT>[4];
  for (int i = 0; i < 4; ++i) {
    mats[i].Allocate(2, 3);
    mats[i].Fill(i);
  }
  
  DMat<IT, VT> a(mats, 1, 4, ONED_BLOCK_COLUMN);
  DMat<IT, VT> b(mats, 4, 1, ONED_BLOCK_ROW);
  DMat<IT, VT> c(mats, 2, 2, TWOD_ROWMAJOR);
  DMat<IT, VT> d(mats, 2, 2, TWOD_COLMAJOR);
  
  // Tests replacing existing matrix.
  DMat<IT, VT> f(mats, 2, 2, TWOD_COLMAJOR);
  f.Concatenate(mats, 1, 4, ONED_BLOCK_COLUMN);
  
  VT ans_a [] = { 0, 0, 0, 1, 1, 1, 2, 2, 2, 3, 3, 3,
                  0, 0, 0, 1, 1, 1, 2, 2, 2, 3, 3, 3 };
  VT ans_b [] = { 0, 0, 0,
                  0, 0, 0,
                  1, 1, 1,
                  1, 1, 1,
                  2, 2, 2,
                  2, 2, 2,
                  3, 3, 3,
                  3, 3, 3 };
  VT ans_c [] = { 0, 0, 0, 1, 1, 1,
                  0, 0, 0, 1, 1, 1,
                  2, 2, 2, 3, 3, 3,
                  2, 2, 2, 3, 3, 3 };
  VT ans_d [] = { 0, 0, 0, 2, 2, 2,
                  0, 0, 0, 2, 2, 2,
                  1, 1, 1, 3, 3, 3,
                  1, 1, 1, 3, 3, 3 };
  
  DMat<IT, VT> mat_a(2, 12, DENSE_ROWMAJOR, ans_a);
  DMat<IT, VT> mat_b(8,  3, DENSE_ROWMAJOR, ans_b);
  DMat<IT, VT> mat_c(4,  6, DENSE_ROWMAJOR, ans_c);
  DMat<IT, VT> mat_d(4,  6, DENSE_ROWMAJOR, ans_d);
  
  delete [] mats;
  
  bool res = true;
  res &= mat_a.IsEquivalent(a);
  res &= mat_b.IsEquivalent(b);
  res &= mat_c.IsEquivalent(c);
  res &= mat_d.IsEquivalent(d);
  res &= mat_a.IsEquivalent(f);
  return res;
}

bool TestDeepCopy() {
  DMat<IT, VT> a(7, 7, DENSE_ROWMAJOR);
  a.Generate();
  DMat<IT, VT> b;
  b.DeepCopy(a);
  b.DeepCopy(a);
  b.DeepCopy(a);  // Copies thrice just to check the Deallocate issue.
  return a.IsEquivalent(b) && (a.values_ != b.values_);
}

bool TestShallowCopy() {
  DMat<IT, VT> a(7, 7, DENSE_ROWMAJOR);
  a.Generate();
  DMat<IT, VT> b;
  b.ShallowCopy(a);
  b.ShallowCopy(a);
  b.ShallowCopy(a);  // Copies thrice just to check the Deallocate issue.
  return a.IsEquivalent(b) && (a.values_ == b.values_);
}

bool TestReduce() {
  VT mat [] = { 1, -4,  3,
                4,  5, -2,
                7,  6,  9,
                3,  8,  1 };
  DMat<IT, VT> a(4, 3, DENSE_ROWMAJOR, mat);
  VT res = a.Reduce(SPDM3_SUM);
  return (res == 41);
}

bool TestNnzCount() {
  // The rightmost column is padding.
  VT mat [] = { 1, -4,  3, 0,
                0,  5, -2, 0,
                7,  0,  9, 4,
                3,  8,  1, 0 };
  DMat<IT, VT> a(4, 3, 4, DENSE_ROWMAJOR, mat);
  return (a.nnz() == 10);
}

bool TestSaveNumPy() {
  DMat<IT, VT> a(7, 7, DENSE_COLMAJOR);
  a.Generate();
  a.SaveNumPy("a.npy");
  DMat<IT, VT> b(DENSE_COLMAJOR);
  b.LoadNumPy("a.npy");
  system("rm -f a.npy");
  return b.IsEquivalent(a);
}

bool TestSaveNumPy2() {
  const char *test_input = "testdata/x.npy";
  
  DMat<IT, VT> a(DENSE_COLMAJOR);
  a.LoadNumPy(test_input);
  a.SaveNumPy("a.npy");
  
  FILE *fp, *fa;
  fp = fopen(test_input, "rb");
  fa = fopen("a.npy", "rb");
  
  char bp[100], ba[100];
  while(!feof(fp) && !feof(fa)) {
    int np = fread(bp, sizeof(char), 100, fp);
    int na = fread(ba, sizeof(char), 100, fa);
    if (np != na) {
      printf("np = %d != %d = na\n", np, na);
      return false;
    }
    // TODO(penpornk): Fix this testcase. It fails right now.
    for (int i = 0; i < np; ++i) {
      if (bp[i] != ba[i]) {
        printf("%d: %d != %d\n", i, bp[i], ba[i]);
        return false;
      }
    }
  }
  fclose(fp);
  fclose(fa);
  
  system("rm -f a.npy");
  return true;
}

bool TestTransposeBySwitchingMajor() {
  DMat<IT, VT> a(5, 5, DENSE_ROWMAJOR);
  a.FillSeries(0.0);
  a.TransposeBySwitchingMajor();
  a.SaveNumPy("a.npy");
  // TODO(penpornk): Actually check the result by code, not manually.
  system("rm -f a.npy");
  return true;
}

bool TestGenerateNormal() {
  DMat<IT, VT> a(5, 5, DENSE_ROWMAJOR);
  a.GenerateNormal(0.0, 1.0, 1001);
//  a.Print();
//  printf("\n");
//  a.GenerateNormal(0.0, 1.0, 1002);
//  a.Print();
  // TODO(penpornk): Actually check the results.
  return true;
}

bool TestIsSymmetric() {
  VT mat [] = { 1,  0,  7, 3, 0,
                0,  5, -2, 8, 0,
                7, -2,  9, 4, 0,
                3,  8,  4, 0, 0 };
  DMat<IT, VT> A(4, 4, 5, DENSE_ROWMAJOR, mat);
  if (!A.IsSymmetric())
    return false;
  
  VT mat2 [] = { 1, 2, 3,
                 2, 4, 5,
                 3, 6, 7 };
  DMat<IT, VT> B(3, 3, 3, DENSE_ROWMAJOR, mat2);
  if (B.IsSymmetric())
    return false;
  return true;
}

bool TestMakeSymmetric() {
  VT mat [] = { 1,  0,  7, 3, 0,
                3,  5, -2, 8, 0,
                4, -2,  9, 4, 0,
                9,  3,  4, 0, 0 };
  DMat<IT, VT> A(4, 4, 5, DENSE_ROWMAJOR, mat);
  A.MakeSymmetric();
  return A.IsSymmetric();
}

}  // namespace spdm3

#define ADDTEST(fn, name) tests.push_back(std::make_pair(fn, name));

int main(int argc, char* argv[]) {
  // Registers test cases.
  std::list< std::pair<std::function<bool()>, std::string> > tests;
  ADDTEST(spdm3::TestDefaultConstructor, "TestDefaultConstructor");
  ADDTEST(spdm3::TestFill, "TestFill");
  ADDTEST(spdm3::TestGenerate, "TestGenerate");
  ADDTEST(spdm3::TestIO, "TestIO");
  ADDTEST(spdm3::TestElmtWiseOp, "TestElmtWiseOp");
  ADDTEST(spdm3::TestSetSubDMat, "TestSetSubDMat");
  ADDTEST(spdm3::TestSubDMat, "TestSubDMat");
  ADDTEST(spdm3::TestShallowSubDMat, "TestShallowSubDMat");
  ADDTEST(spdm3::TestMultiplyAdd, "TestMultiplyAdd");
  ADDTEST(spdm3::TestTranspose, "TestTranspose");
  ADDTEST(spdm3::TestConvertFromCSR, "TestConvertFromCSR");
  ADDTEST(spdm3::TestElmtWiseOpWithSpMat, "TestElmtWiseOpWithSpMat");
  ADDTEST(spdm3::TestConcatenate, "TestConcatenate");
  ADDTEST(spdm3::TestDeepCopy, "TestDeepCopy");
  ADDTEST(spdm3::TestShallowCopy, "TestShallowCopy");
  ADDTEST(spdm3::TestReduce, "TestReduce");
  ADDTEST(spdm3::TestNnzCount, "TestNnzCount");
  ADDTEST(spdm3::TestSaveNumPy, "TestSaveNumPy");
  ADDTEST(spdm3::TestSaveNumPy2, "TestSaveNumPy2");
  ADDTEST(spdm3::TestFillSeries, "TestFillSeries");
  ADDTEST(spdm3::TestTransposeBySwitchingMajor, "TestTransposeBySwitchingMajor");
  ADDTEST(spdm3::TestGenerateNormal, "TestGenerateNormal");
  ADDTEST(spdm3::TestIsSymmetric, "TestIsSymmetric");
  ADDTEST(spdm3::TestMakeSymmetric, "TestMakeSymmetric");
  
  // Runs tests.
  int failed = 0;
  for (auto test : tests) {
    bool res = test.first();
    std::cout << std::setw(50) << test.second << ": ";
    if (res) {
      std::cout << "PASSED." << std::endl;
    } else {
      ++failed;
      std::cout << "FAILED." << std::endl;
    }
  }
  
  // Summarizes.
  if (!failed)
    std::cout << "All " << tests.size() << " tests passed." << std::endl;
  else
    std::cout << failed << " out of " << tests.size()
              << " tests failed." << std::endl;
  return 0;
}
