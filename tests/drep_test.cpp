#include "distdmat.h"

#include <cstdio>
#include <functional>
#include <iomanip>
#include <iostream>
#include <list>
#include <sstream>
#include <string>
#include <tuple>
#include <utility>

#include "distio.h"
#include "utility.h"

#define IT  int
#define VT  float

namespace spdm3 {

bool Test1DRow(int layers) {
  Comm *c1 = GetComms(MPI_COMM_WORLD, ONED_BLOCK_ROW, 1);
  Comm *cnew = GetComms(MPI_COMM_WORLD, ONED_BLOCK_ROW, layers);
  DistDMat<IT, VT> a(c1);
  DistDMat<IT, VT> sol(cnew);
  a.GenerateWithFunction(123, 111);
  sol.GenerateWithFunction(123, 111);

  a.Replicate(cnew);
  
  delete [] c1;
  delete [] cnew;
  
  return a.mat_.IsEquivalent(sol.mat_);
}

bool Test1DCol(int layers) {
  Comm *c1 = GetComms(MPI_COMM_WORLD, ONED_BLOCK_COLUMN, 1);
  Comm *cnew = GetComms(MPI_COMM_WORLD, ONED_BLOCK_COLUMN, layers);
  DistDMat<IT, VT> a(c1);
  DistDMat<IT, VT> sol(cnew);
  a.GenerateWithFunction(123, 111);
  sol.GenerateWithFunction(123, 111);

  a.Replicate(cnew);
  
  delete [] c1;
  delete [] cnew;
  
  return a.mat_.IsEquivalent(sol.mat_);
}

bool Test2DRow(int layers) {
  Comm *c1 = GetComms(MPI_COMM_WORLD, TWOD_ROWMAJOR, 1);
  Comm *cnew = GetComms(MPI_COMM_WORLD, TWOD_ROWMAJOR, layers);
  DistDMat<IT, VT> a(c1);
  DistDMat<IT, VT> sol(cnew);
  a.GenerateWithFunction(123, 111);
  sol.GenerateWithFunction(123, 111);

  a.Replicate(cnew);
  
  delete [] c1;
  delete [] cnew;
  
  return a.mat_.IsEquivalent(sol.mat_);
}

bool Test2DCol(int layers) {
  Comm *c1 = GetComms(MPI_COMM_WORLD, TWOD_COLMAJOR, 1);
  Comm *cnew = GetComms(MPI_COMM_WORLD, TWOD_COLMAJOR, layers);
  DistDMat<IT, VT> a(c1);
  DistDMat<IT, VT> sol(cnew);
  a.GenerateWithFunction(123, 111);
  sol.GenerateWithFunction(123, 111);

  a.Replicate(cnew);
  
  delete [] c1;
  delete [] cnew;
  
  return a.mat_.IsEquivalent(sol.mat_);
}

bool TestWeirdLayoutBlockColumn(int layers) {
  Comm *c1 = GetComms(MPI_COMM_WORLD, ONED_BLOCK_COLUMN, 1);
  Comm *cnew = GetComms(MPI_COMM_WORLD, ONED_BLOCK_COLUMN, layers);
  DistDMat<IT, VT> a(c1);
  a.GenerateWithFunction(123, 123);  // a must be square
  a.Replicate(cnew);
  
  DistDMat<IT, VT> b(cnew);
  b.WeirdLayoutBlockColumn(a);

  int nteams = a.teams();
  int nblocks = nteams / layers;
  int col_id = a.team_id();
  
  // Forms displs array.
  IT *counts = new IT[nblocks];
  IT *displs = new IT[nblocks+1];
  int blk_idx = (a.team_id() + a.layer_id()) % layers;
  displs[0] = 0;
  for (int i = 0; i < nblocks; ++i) {
    counts[i] = a.col_counts_[blk_idx];
    displs[i+1] = displs[i] + counts[i];
    blk_idx += layers;
  }
  
  // Actually compares subblocks (in a different way from WeirdLayout()).
  bool res = true;
  int checked_subblocks = 0;
  DMat<IT, VT> suba;
  DMat<IT, VT> subb;
  for (int row_id = 0; row_id < nteams; ++row_id) {
    int layer_id = (row_id - col_id + nteams) % layers;
    if (layer_id != a.layer_id())
      continue;
    
    int inner = (row_id - col_id + nteams) % nteams;
    blk_idx = row_id / layers;
    suba.ShallowSubDMat(a.mat_, a.col_displs_[row_id], 0,
                                a.col_counts_[row_id], a.col_counts_[col_id]);
    subb.ShallowSubDMat(b.mat_, displs[blk_idx], 0,
                                counts[blk_idx], a.col_counts_[col_id]);
    res &= subb.IsEquivalent(suba);
    ++checked_subblocks;
  }
  res &= (checked_subblocks == nblocks);
  
  // Deallocation.
  delete [] counts;
  delete [] displs;
  delete [] c1;
  delete [] cnew;
  
  return res;
}

bool TestWeirdLayoutBlockRow(int layers) {
  Comm *c1 = GetComms(MPI_COMM_WORLD, ONED_BLOCK_ROW, 1);
  Comm *cnew = GetComms(MPI_COMM_WORLD, ONED_BLOCK_ROW, layers);
  DistDMat<IT, VT> a(c1);
  a.GenerateWithFunction(159, 159);  // a must be square
  a.Replicate(cnew);
  
  DistDMat<IT, VT> b(cnew);
  b.WeirdLayoutBlockRow(a);

  int nteams = a.teams();
  int nblocks = nteams / layers;
  int row_id = a.team_id();
  
  // Forms displs array.
  IT *counts = new IT[nblocks];
  IT *displs = new IT[nblocks+1];
  int blk_idx = (a.team_id() + layers - (a.layer_id() % layers)) % layers;
  displs[0] = 0;
  for (int i = 0; i < nblocks; ++i) {
    counts[i] = a.row_counts_[blk_idx];
    displs[i+1] = displs[i] + counts[i];
    blk_idx += layers;
  }
  
  // Actually compares subblocks (in a different way from WeirdLayout()).
  bool res = true;
  int checked_subblocks = 0;
  DMat<IT, VT> suba;
  DMat<IT, VT> subb;
  for (int col_id = 0; col_id < nteams; ++col_id) {
    int layer_id = (row_id - col_id + nteams) % layers;
    if (layer_id != a.layer_id())
      continue;
    
    blk_idx = col_id / layers;
    suba.ShallowSubDMat(a.mat_, 0, a.row_displs_[col_id],
                                a.row_counts_[row_id], a.row_counts_[col_id]);
    subb.ShallowSubDMat(b.mat_, 0, displs[blk_idx],
                                a.row_counts_[row_id], counts[blk_idx]);
    res &= subb.IsEquivalent(suba);
    ++checked_subblocks;
  }
  res &= (checked_subblocks == nblocks);
  
  // Deallocation.
  delete [] counts;
  delete [] displs;
  delete [] c1;
  delete [] cnew;
  
  return res;
}

bool TestWeirdLayoutBlockRowTransposed(int layers) {
  Comm *c1 = GetComms(MPI_COMM_WORLD, ONED_BLOCK_ROW, 1);
  Comm *cnew = GetComms(MPI_COMM_WORLD, ONED_BLOCK_ROW, layers);
  DistDMat<IT, VT> a(c1);
  a.GenerateWithFunction(157, 157);  // a must be square
  a.Replicate(cnew);
  
  DistDMat<IT, VT> b(cnew);
  b.WeirdLayoutBlockRowTransposed(a);

  int nteams = a.teams();
  int nblocks = nteams / layers;
  int row_id = a.team_id();
  
  // Forms displs array.
  IT *counts = new IT[nblocks];
  IT *displs = new IT[nblocks+1];
  int blk_idx = (a.team_id() + a.layer_id()) % layers;
  displs[0] = 0;
  for (int i = 0; i < nblocks; ++i) {
    counts[i] = a.row_counts_[blk_idx];
    displs[i+1] = displs[i] + counts[i];
    blk_idx += layers;
  }
  
  // Actually compares subblocks (in a different way from WeirdLayout()).
  bool res = true;
  int checked_subblocks = 0;
  DMat<IT, VT> suba;
  DMat<IT, VT> subb;
  for (int col_id = 0; col_id < nteams; ++col_id) {
    int layer_id = (col_id - row_id + nteams) % layers;
    if (layer_id != a.layer_id())
      continue;
    
    blk_idx = col_id / layers;
    suba.ShallowSubDMat(a.mat_, 0, a.row_displs_[col_id],
                                a.row_counts_[row_id], a.row_counts_[col_id]);
    subb.ShallowSubDMat(b.mat_, 0, displs[blk_idx],
                                a.row_counts_[row_id], counts[blk_idx]);
    res &= subb.IsEquivalent(suba);
    ++checked_subblocks;
  }
  res &= (checked_subblocks == nblocks);
  
  // Deallocation.
  delete [] counts;
  delete [] displs;
  delete [] c1;
  delete [] cnew;
  
  return res;
}

}  // namespace spdm3

#define ADDTEST(fn, name, layers) \
    tests.push_back(std::make_tuple((std::function<bool(int)>) fn, name, layers));

int main(int argc, char* argv[]) {
  // Registers test cases.
  std::list< std::tuple<std::function<bool(int)>, const char*, int> > tests;

  // 1D block row.
  ADDTEST(spdm3::Test1DRow, "Test1DRow", 2);
  ADDTEST(spdm3::Test1DRow, "Test1DRow", 4);
  ADDTEST(spdm3::Test1DRow, "Test1DRow", 8);
  ADDTEST(spdm3::Test1DRow, "Test1DRow", 16);

  // 1D block col.
  ADDTEST(spdm3::Test1DCol, "Test1DCol", 2);
  ADDTEST(spdm3::Test1DCol, "Test1DCol", 4);
  ADDTEST(spdm3::Test1DCol, "Test1DCol", 8);
  ADDTEST(spdm3::Test1DCol, "Test1DCol", 16);
  
  // 2D row major.
  ADDTEST(spdm3::Test2DRow, "Test2DRow", 2);
  ADDTEST(spdm3::Test2DRow, "Test2DRow", 4);
  ADDTEST(spdm3::Test2DRow, "Test2DRow", 8);
  ADDTEST(spdm3::Test2DRow, "Test2DRow", 16);
  
  // 2D col major.
  ADDTEST(spdm3::Test2DCol, "Test2DCol", 2);
  ADDTEST(spdm3::Test2DCol, "Test2DCol", 4);
  ADDTEST(spdm3::Test2DCol, "Test2DCol", 8);
  ADDTEST(spdm3::Test2DCol, "Test2DCol", 16);
  
  // Weird layout.
  ADDTEST(spdm3::TestWeirdLayoutBlockColumn, "TestWeirdLayoutBlockColumn", 2);
  ADDTEST(spdm3::TestWeirdLayoutBlockColumn, "TestWeirdLayoutBlockColumn", 4);
  
  ADDTEST(spdm3::TestWeirdLayoutBlockRow, "TestWeirdLayoutBlockRow", 1);
  ADDTEST(spdm3::TestWeirdLayoutBlockRow, "TestWeirdLayoutBlockRow", 2);
  ADDTEST(spdm3::TestWeirdLayoutBlockRow, "TestWeirdLayoutBlockRow", 4);
  
  ADDTEST(spdm3::TestWeirdLayoutBlockRowTransposed, "TestWeirdLayoutBlockRowTransposed", 1);
  ADDTEST(spdm3::TestWeirdLayoutBlockRowTransposed, "TestWeirdLayoutBlockRowTransposed", 2);
  ADDTEST(spdm3::TestWeirdLayoutBlockRowTransposed, "TestWeirdLayoutBlockRowTransposed", 4);
  
  MPI_Init(&argc, &argv);
  int p = 1, rank = 0;
  MPI_Comm_size(MPI_COMM_WORLD, &p);
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  if (p != 16) {
    std::cout << "This test must be run with 16 MPI processes. "
              << "Terminating..." << std::endl;
    exit(1);
  }
  
  // Runs tests.
  int failed = 0;
  for (auto test : tests) {
    int layers = std::get<2>(test);
    bool res = std::get<0>(test)(layers);
    if (rank == 0) {
      MPI_Reduce(MPI_IN_PLACE, &res, 1, MPI_BYTE, MPI_BAND, 0, MPI_COMM_WORLD);
      std::stringstream ss;
      ss << std::get<1>(test) << "C" << layers;
      std::cout << std::setw(50) << ss.str() << ": ";
      if (res) {
        std::cout << "PASSED." << std::endl;
      } else {
        ++failed;
        std::cout << "FAILED." << std::endl;
      }
    } else {
      MPI_Reduce(&res, NULL, 1, MPI_BYTE, MPI_BAND, 0, MPI_COMM_WORLD);
    }
  }
  
  // Summarizes.
  if (rank == 0) {
    if (!failed)
      std::cout << "All " << tests.size() << " tests passed." << std::endl;
    else
      std::cout << failed << " out of " << tests.size()
                << " tests failed." << std::endl;
  }

  MPI_Finalize();
  return 0;
}