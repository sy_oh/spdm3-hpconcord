#include "spmat.h"

#include <functional>
#include <iostream>
#include <list>
#include <string>
#include <utility>

#include "dmat.h"

#define IT  int
#define VT  double

namespace spdm3 {

bool TestDefaultConstructor() {
  SpMat<IT, VT> a;
  return true;
}

bool TestGenerate() {
  SpMat<IT, VT> a;
  a.Generate(20, 30, 0.4);
  a.Save("A");
  system("rm A");
  return true;
}

bool TestFrobeniusNorm() {
  LT heads[] = {0, 2, 4, 6, 7};
  LT indices[] = {0, 2, 0, 1, 0, 2, 1};
  VT values[]  = {1, 3, 4, 5, 7, 9, 8};
  SpMat<IT, VT> a;
  a.Set(4, 3, 7,
        5, heads,
        7, indices, values, false);
  if (!FEQ(a.FrobeniusNormSquare(), 245))
    return false;
  if (sizeof(VT) == 8)
    return TEQ(a.FrobeniusNorm(), 15.6524758425, 1e-10);
  else if (sizeof(VT) == 4)
    return TEQ(a.FrobeniusNorm(), 15.6524758425, 1e-05);
  
  printf("ERROR: shouldn't have reached here\n");
  return false;
}

bool TestElmtWiseOp() {
  LT heads[] = {0, 2, 4, 6, 7};
  LT indices[] = {0, 2, 0, 1, 0, 2, 1};
  VT values[]  = {1, 3, 4, 5, 7, 9, 8};
  VT values2[] = {2, 6, 8, 10, 14, 18, 16};
  VT values3[] = {-1, 3, 5, 7, 11, 15, 13};
  SpMat<IT, VT> a, b, c;
  a.Set(4, 3, 7,
        5, heads,
        7, indices, values, false);
  b.Set(4, 3, 7,
        5, heads,
        7, indices, values2, false);
  c.Set(4, 3, 7,
        5, heads,
        7, indices, values3, false);
  a.ElmtWiseOp([] (VT x, VT y) -> VT { return x * y; }, 2.0);
  bool res = a.IsEquivalent(b);
  
  a.ElmtWiseOp([] (VT x)->VT { return x - 3; });
  res &= a.IsEquivalent(c);
  
  return res;
}

bool TestFill() {
  // 0  1 0 3 0
  // 2 -1 4 0 0
  // 0  0 0 7 9
  // 6  0 5 0 0
  Triplet triplets [] = {{0, 1, 1}, {0, 3, 3},
                         {1, 0, 2}, {1, 1, -1}, {1, 2, 4},
                         {2, 3, 7}, {2, 4, 9},
                         {3, 0, 6}, {3, 2, 5}};
  LT heads []   = {0, 2, 5, 7, 9};
  LT indices [] = {1, 3, 0,  1, 2, 3, 4, 0, 2};
  VT values []  = {1, 3, 2, -1, 4, 7, 9, 6, 5};
  SpMat<IT, VT> a, b;
  a.Allocate(4, 5, 9);
  a.Fill(triplets, 9);
  b.Set(4, 5, 9,
        5, heads, 9, indices, values, false);
  return a.IsEquivalent(b);
}

bool TestConcatenation() {
  bool res = true;
  
  // Mat 1:
  // 0 1 0 9
  // 2 0 4 0
  // 3 5 0 1
  LT a_heads []   = {0, 2, 4, 7};
  LT a_indices [] = {1, 3, 0, 2, 0, 1, 3};
  VT a_values []  = {1, 9, 2, 4, 3, 5, 1};
  
  // Mat 2:
  // 3 0 2 7
  // 4 9 0 0
  // 2 0 5 0
  LT b_heads []   = {0, 3, 5, 7};
  LT b_indices [] = {0, 2, 3, 0, 1, 0, 2};
  VT b_values []  = {3, 2, 7, 4, 9, 2, 5};
  
  SpMat<IT, VT> mats[4];
  mats[0].PointersToSpMat(3, 4, 7, SPARSE_CSR, 0,
                          a_heads, a_indices, a_values, 0);
  mats[1].PointersToSpMat(3, 4, 7, SPARSE_CSR, 0,
                          a_heads, a_indices, a_values, 0);
  mats[2].PointersToSpMat(3, 4, 7, SPARSE_CSR, 0,
                          b_heads, b_indices, b_values, 0);
  mats[3].PointersToSpMat(3, 4, 7, SPARSE_CSR, 0,
                          b_heads, b_indices, b_values, 0);
  
  // 1D Block Row layout.
  // 0 1 0 9
  // 2 0 4 0
  // 3 5 0 1
  // 0 1 0 9
  // 2 0 4 0
  // 3 5 0 1
  // 3 0 2 7
  // 4 9 0 0
  // 2 0 5 0
  // 3 0 2 7
  // 4 9 0 0
  // 2 0 5 0
  SpMat<IT, VT> oned_row(mats, 4, 1, ONED_BLOCK_ROW);
  LT sol1_heads []   = {0, 2, 4, 7, 9, 11, 14, 17, 19, 21, 24, 26, 28};
  LT sol1_indices [] = {1, 3, 0, 2, 0, 1, 3,
                        1, 3, 0, 2, 0, 1, 3,
                        0, 2, 3, 0, 1, 0, 2,
                        0, 2, 3, 0, 1, 0, 2};
  VT sol1_values []  = {1, 9, 2, 4, 3, 5, 1,
                        1, 9, 2, 4, 3, 5, 1,
                        3, 2, 7, 4, 9, 2, 5,
                        3, 2, 7, 4, 9, 2, 5};
  SpMat<IT, VT> sol1;
  sol1.PointersToSpMat(12, 4, 28, SPARSE_CSR, 0,
                       sol1_heads, sol1_indices, sol1_values, 0);
  res &= oned_row.IsEquivalent(sol1);
  
  // 1D Block Column layout.
  // 0 1 0 9 0 1 0 9 3 0 2 7 3 0 2 7
  // 2 0 4 0 2 0 4 0 4 9 0 0 4 9 0 0
  // 3 5 0 1 3 5 0 1 2 0 5 0 2 0 5 0
  SpMat<IT, VT> oned_col(mats, 1, 4, ONED_BLOCK_COLUMN);
  LT sol2_heads []   = {0, 10, 18, 28};
  LT sol2_indices [] = {1, 3, 5, 7, 8, 10, 11, 12, 14, 15,
                        0, 2, 4, 6, 8, 9, 12, 13,
                        0, 1, 3, 4, 5, 7, 8, 10, 12, 14};
  VT sol2_values []  = {1, 9, 1, 9, 3, 2, 7, 3, 2, 7,
                        2, 4, 2, 4, 4, 9, 4, 9,
                        3, 5, 1, 3, 5, 1, 2, 5, 2, 5};
  SpMat<IT, VT> sol2;
  sol2.PointersToSpMat(3, 16, 28, SPARSE_CSR, 0,
                       sol2_heads, sol2_indices, sol2_values, 0);
  res &= oned_col.IsEquivalent(sol2);
  
  // 2D Row Major layout.
  // 0 1 0 9 0 1 0 9
  // 2 0 4 0 2 0 4 0
  // 3 5 0 1 3 5 0 1
  // 3 0 2 7 3 0 2 7
  // 4 9 0 0 4 9 0 0
  // 2 0 5 0 2 0 5 0
  SpMat<IT, VT> twod_row(mats, 2, 2, TWOD_ROWMAJOR);
  LT sol3_heads []   = {0, 4, 8, 14, 20, 24, 28};
  LT sol3_indices [] = {1, 3, 5, 7,
                        0, 2, 4, 6,
                        0, 1, 3, 4, 5, 7,
                        0, 2, 3, 4, 6, 7,
                        0, 1, 4, 5,
                        0, 2, 4, 6};
  VT sol3_values [] = {1, 9, 1, 9,
                       2, 4, 2, 4,
                       3, 5, 1, 3, 5, 1,
                       3, 2, 7, 3, 2, 7,
                       4, 9, 4, 9,
                       2, 5, 2, 5};
  SpMat<IT, VT> sol3;
  sol3.PointersToSpMat(6, 8, 28, SPARSE_CSR, 0,
                       sol3_heads, sol3_indices, sol3_values, 0);
  res &= twod_row.IsEquivalent(sol3);
  
  // 2D Col Major layout.
  // 0 1 0 9 3 0 2 7
  // 2 0 4 0 4 9 0 0
  // 3 5 0 1 2 0 5 0
  // 0 1 0 9 3 0 2 7
  // 2 0 4 0 4 9 0 0
  // 3 5 0 1 2 0 5 0
  SpMat<IT, VT> twod_col(mats, 2, 2, TWOD_COLMAJOR);
  LT sol4_heads []   = {0, 5, 9, 14, 19, 23, 28};
  LT sol4_indices [] = {1, 3, 4, 6, 7,
                        0, 2, 4, 5,
                        0, 1, 3, 4, 6,
                        1, 3, 4, 6, 7,
                        0, 2, 4, 5,
                        0, 1, 3, 4, 6};
  VT sol4_values []  = {1, 9, 3, 2, 7,
                        2, 4, 4, 9,
                        3, 5, 1, 2, 5,
                        1, 9, 3, 2, 7,
                        2, 4, 4, 9,
                        3, 5, 1, 2, 5};
  SpMat<IT, VT> sol4;
  sol4.PointersToSpMat(6, 8, 28, SPARSE_CSR, 0,
                       sol4_heads, sol4_indices, sol4_values, 0);
  res &= twod_col.IsEquivalent(sol4);
  
  // Tests replacing existing matrix.
  SpMat<IT, VT> edit(mats, 2, 2, TWOD_COLMAJOR);
  edit.Concatenate(mats, 4, 1, ONED_BLOCK_ROW);
  res &= edit.IsEquivalent(sol1);
  
  return res;
}

bool TestGetShallowCopy() {
  SpMat<IT, VT> a;
  a.Generate(14, 17, 0.4);
  SpMat<IT, VT> b = a.GetShallowCopy(true);
  SpMat<IT, VT> c = a.GetShallowCopy(false);
  
  bool res = true;
  res &= a.IsEquivalent(b);
  res &= (a.headptrs_ == b.headptrs_);
  res &= (a.indices_ == b.indices_);
  res &= (a.values_ == b.values_);
  
  res &= a.IsEquivalent(c);
  res &= (a.headptrs_ == c.headptrs_);
  res &= (a.indices_ == c.indices_);
  res &= (a.values_ == c.values_);
  
  return res;
}

bool TestGetStructure() {
  SpMat<IT, VT> a;
  a.Generate(13, 13, 0.7);
  SpMat<IT, VT> b = a.GetStructure();
  return true;
}

bool TestGetDeepCopy() {
  SpMat<IT, VT> a;
  a.Generate(15, 13, 0.6);
  SpMat<IT, VT> c = a.GetStructure();
  SpMat<IT, VT> b = a.GetDeepCopy();
  
  bool res = true;
  res &= a.IsEquivalent(b);
  res &= (a.headptrs_ != b.headptrs_);
  res &= (a.indices_ != b.indices_);
  res &= (a.values_ != b.values_);
  
  return res;
}

bool TestDeepCopy() {
  SpMat<IT, VT> a;
  a.Generate(15, 13, 0.6);
  SpMat<IT, VT> b;
  b.DeepCopy(a);
  
  bool res = true;
  res &= a.IsEquivalent(b);
  res &= (a.headptrs_ != b.headptrs_);
  res &= (a.indices_ != b.indices_);
  res &= (a.values_ != b.values_);
  
  return res;
}

bool TestSwap() {
  SpMat<IT, VT> a, b;
  a.Generate(16, 17, 0.8);
  b.Generate(15, 19, 0.6);
  
  SpMat<IT, VT> c, d;
  c.DeepCopy(a);
  d.DeepCopy(b);
  a.Swap(b);
  
  bool res = true;
  res &= c.IsEquivalent(b);
  res &= d.IsEquivalent(a);
  return res;
}

bool TestExpandBuffers() {
  bool res = true;
  SpMat<IT, VT> a;
  a.Generate(23, 24, 0.5);
  SpMat<IT, VT> b = a.GetDeepCopy();
  res &= b.IsEquivalent(a);
  b.ExpandBuffers(900, false);

  SpMat<IT, VT> c = a.GetDeepCopy();
  res &= c.IsEquivalent(a);
  c.ExpandBuffers(1000);
  res &= c.IsEquivalent(a);
  
  return res;
}

bool TestLoadSave() {
  SpMat<IT, VT> a;
  a.Generate(33, 23, 0.7);
  a.Save("a");
  SpMat<IT, VT> b;
  b.Load("a");
  system("rm a");
  return b.IsEquivalent(a);
}

bool TestMultiplyAdd() {
  bool res = true;
  SpMat<IT, VT> a;
  LT heads []   = {0, 2, 5, 7, 9};
  LT indices [] = {1, 3, 0,  1, 2, 3, 4, 0, 2};
  VT values []  = {1, 3, 2, -1, 4, 7, 9, 6, 5};
  a.Set(4, 5, 9,
        5, heads, 9, indices, values, false);
  
  VT dense [] = {3, -2,  4, 1, -1,  7,
                -1,  3,  6, 2,  1,  5,
                 2,  1, -3, 4,  8,  6,
                 4,  5,  9, 2, -1, -3,
                -4, -1, -2, 5,  3,  1};
  DMat<IT, VT> b(5, 6, DENSE_ROWMAJOR, dense);
  DMat<IT, VT> c;
  
  VT ans[] = { 11.000,  18.000,  33.000,   8.000,  -2.000,  -4.000,
               15.000,  -3.000, -10.000,  16.000,  29.000,  33.000,
               -8.000,  26.000,  45.000,  59.000,  20.000, -12.000,
               28.000,  -7.000,   9.000,  26.000,  34.000,  72.000 };
  DMat<IT, VT> d(4, 6, DENSE_ROWMAJOR, ans);
  
  // Tests default lib (LIB_BLAS or LIB_MKL).
  a.MultiplyAdd(b, c);
  res &= c.IsEquivalent(d);
  
  // Tests the naive lib.
  c.Fill(0.0);
  a.SetMathLib(LIB_NAIVE);
  a.MultiplyAdd(b, c);
  res &= c.IsEquivalent(d);
  
  return res;
}

bool TestSubmatrix() {
  //   7  -25   -5   16   48   43    0    0    0    0    0  -50    0
  //  -9  -41    0   27    0   -4    0    0    0   42    0  -19    0 
  //   0    4    0   47    7   34    0  -32    0   23   -9    0    0 
  //   0   38    0   -9  -49    0  -20    0    0    0   13   30    0 
  //   0    0   25    0    0   13   25    0    1   47    0  -36    0 
  //   0  -46    0   39   41    0    0   48   35    0  -21   24   14 
  // -41   28    8  -28  -18   49   -7  -44    0    3   39  -21  -19 
  //   0    0    0  -43    0    0    0  -29    0    0  -29   37    0 
  //   0    0    0    0    0  -29    0    0    7    0    0    0    5 
  //   0    0   35    0   26    0    0  -38   -1    0  -16   49    0 
  //   0    0  -18  -15    0    0    0   33    0  -10    0    0    0
  LT heads [] = {0, 7, 13, 20, 26, 32, 40, 52, 56, 59, 65, 69};
  LT indices [] = {0, 1, 2, 3, 4, 5, 11,
                   0, 1, 3, 5, 9, 11,
                   1, 3, 4, 5, 7, 9, 10,
                   1, 3, 4, 6, 10, 11,
                   2, 5, 6, 8, 9, 11,
                   1, 3, 4, 7, 8, 10, 11, 12,
                   0, 1, 2, 3, 4, 5, 6, 7, 9, 10, 11, 12,
                   3, 7, 10, 11,
                   5, 8, 12,
                   2, 4, 7, 8, 10, 11,
                   2, 3, 7, 9};
  VT values [] = { 7, -25, -5, 16, 48, 43, -50,
                  -9, -41, 27, -4, 42, -19,
                   4, 47, 7, 34, -32, 23, -9,
                  38, -9, -49, -20, 13, 30,
                  25, 13, 25, 1, 47, -36,
                 -46, 39, 41, 48, 35, -21, 24, 14,
                 -41, 28, 8, -28, -18, 49, -7, -44, 3, 39, -21, -19,
                 -43, -29, -29, 37,
                 -29, 7, 5,
                  35, 26, -38, -1, -16, 49,
                 -18, -15, 33, -10};
  
  SpMat<IT, VT> a;
  a.PointersToSpMat(11, 13, 69, SPARSE_CSR, 0,
                    heads, indices, values, false);
  
  SpMat<IT, VT> b;
  b.Submatrix(a, 4, 3, 6, 5);
  
  //    0    0   13   25    0
  //   39   41    0    0   48
  //  -28  -18   49   -7  -44
  //  -43    0    0    0  -29
  //    0    0  -29    0    0
  //    0   26    0    0  -38
  LT sub_heads [] = {0, 2, 5, 10, 12, 13, 15};
  LT sub_indices [] = {2, 3,
                       0, 1, 4,
                       0, 1, 2, 3, 4,
                       0, 4,
                       2,
                       1, 4};
  VT sub_values [] = {13, 25,
                      39, 41, 48,
                     -28, -18, 49, -7, -44,
                     -43, -29,
                     -29,
                      26, -38};
  SpMat<IT, VT> c;
  c.PointersToSpMat(6, 5, 15, SPARSE_CSR, 0,
                    sub_heads, sub_indices, sub_values, false);
  return c.IsEquivalent(b);
}

bool TestDotMultiplyI() {
  // 0 1 0 9
  // 2 0 4 0
  // 3 5 0 1
  LT heads []   = {0, 2, 4, 7};
  LT indices [] = {1, 3, 0, 2, 0, 1, 3};
  VT values []  = {1, 9, 2, 4, 3, 5, 1};
  SpMat<IT, VT> a;
  a.PointersToSpMat(3, 4, 7, SPARSE_CSR, 0,
                    heads, indices, values, false);
  
  // 2 5 5 9
  // 2 4 1 6
  // 7 9 2 1
  VT dense [] = { 2, 5, 5, 9,
                  2, 4, 1, 6,
                  7, 9, 2, 1 };
  DMat<IT, VT> b(3, 4, DENSE_ROWMAJOR, dense);
  
  a.DotMultiplyI(b);
  
  VT newvalues [] = {5, 81, 4, 4, 21, 45, 1};
  SpMat<IT, VT> c;
  c.PointersToSpMat(3, 4, 7, SPARSE_CSR, 0,
                    heads, indices, newvalues, false);
  return a.IsEquivalent(c);
}

bool TestDotMultiplyTransposeI() {
  // 0 1 0 9
  // 2 0 4 0
  // 3 5 0 1
  LT heads []   = {0, 2, 4, 7};
  LT indices [] = {1, 3, 0, 2, 0, 1, 3};
  VT values []  = {1, 9, 2, 4, 3, 5, 1};
  SpMat<IT, VT> a;
  a.PointersToSpMat(3, 4, 7, SPARSE_CSR, 0,
                    heads, indices, values, false);
  
  // 2 2 7
  // 5 4 9
  // 5 1 2
  // 9 6 1
  VT dense [] = { 2, 2, 7,
                  5, 4, 9,
                  5, 1, 2,
                  9, 6, 1 };
  DMat<IT, VT> b(4, 3, DENSE_ROWMAJOR, dense);
  
  a.DotMultiplyTransposeI(b);
  
  VT newvalues [] = {5, 81, 4, 4, 21, 45, 1};
  SpMat<IT, VT> c;
  c.PointersToSpMat(3, 4, 7, SPARSE_CSR, 0,
                    heads, indices, newvalues, false);
  return a.IsEquivalent(c);
}

bool TestReduce() {
  // 0 1 0 9
  // 2 0 4 0
  // 3 5 0 1
  LT heads []   = {0, 2, 4, 7};
  LT indices [] = {1, 3, 0, 2, 0, 1, 3};
  VT values []  = {1, 9, 2, 4, 3, 5, 1};
  SpMat<IT, VT> a;
  a.PointersToSpMat(3, 4, 7, SPARSE_CSR, 0,
                    heads, indices, values, false);
  VT res = a.Reduce(SPDM3_SUM);
  return (res == 25);
}

bool TestGetElmt() {
  // 0 1 0 9
  // 2 0 4 0
  // 3 5 0 1
  LT heads []   = {0, 2, 4, 7};
  LT indices [] = {1, 3, 0, 2, 0, 1, 3};
  VT values []  = {1, 9, 2, 4, 3, 5, 1};
  SpMat<IT, VT> a;
  a.PointersToSpMat(3, 4, 7, SPARSE_CSR, 0,
                    heads, indices, values, false);
  bool res = true;
  res &= FEQ(a.GetElmt(0, 1), 1.0);
  res &= FEQ(a.GetElmt(2, 2), 0.0);
  res &= FEQ(a.GetElmt(1, 2), 4.0);
  res &= FEQ(a.GetElmt(2, 0), 3.0);
  res &= FEQ(a.GetElmt(2, 3), 1.0);
  
  return res;
}

bool TestIsSymmetric() {
  bool res = true;
  
  // 0 1 0 9
  // 2 0 4 0
  // 3 5 0 1
  LT heads []   = {0, 2, 4, 7};
  LT indices [] = {1, 3, 0, 2, 0, 1, 3};
  VT values []  = {1, 9, 2, 4, 3, 5, 1};
  SpMat<IT, VT> a;
  a.PointersToSpMat(3, 4, 7, SPARSE_CSR, 0,
                    heads, indices, values, false);
  res &= !a.IsSymmetric();
  
  // 0 1 0 9
  // 2 0 4 0
  // 3 5 0 1
  // 9 0 1 3
  LT heads2 []   = {0, 2, 4, 7, 10};
  LT indices2 [] = {1, 3, 0, 2, 0, 1, 3, 0, 2, 3};
  VT values2 []  = {1, 9, 2, 4, 3, 5, 1, 9, 1, 3};
  SpMat<IT, VT> b;
  b.PointersToSpMat(4, 4, 10, SPARSE_CSR, 0,
                    heads2, indices2, values2, false);
  res &= !b.IsSymmetric();

  // 0 1 0 9
  // 1 0 4 0
  // 0 4 0 1
  // 9 0 1 3
  LT heads3 []   = {0, 2, 4, 6, 9};
  LT indices3 [] = {1, 3, 0, 2, 1, 3, 0, 2, 3};
  VT values3 []  = {1, 9, 1, 4, 4, 1, 9, 1, 3};
  SpMat<IT, VT> c;
  c.PointersToSpMat(4, 4, 9, SPARSE_CSR, 0,
                    heads3, indices3, values3, false);
  res &= c.IsSymmetric();
  
  return res;
}

bool TestDiagonal() {
  // 0 1 0 9
  // 2 7 4 0
  // 3 5 0 1
  // 9 0 1 3
  LT heads []   = {0, 2, 5, 8, 11};
  LT indices [] = {1, 3, 0, 1, 2, 0, 1, 3, 0, 2, 3};
  VT values []  = {1, 9, 2, 7, 4, 3, 5, 1, 9, 1, 3};
  SpMat<IT, VT> a, b;
  a.PointersToSpMat(4, 4, 11, SPARSE_CSR, 0,
                    heads, indices, values, false);
  b.Diagonal(a);
  
  // Converts to dense matrix to check.
  DMat<IT, VT> c(a), d(b);
  if (c.rows_ != d.rows_ || c.cols_ != d.cols_ || c.rows_ != c.cols_)
    return false;
  
  bool res = true;
  for (int i = 0; i < c.rows_; ++i) {
    res &= (c.values_[i * c.lda_ + i] == d.values_[i * d.lda_ + i]);
  }
  return res;
}

bool TestAddSparse() {
  // 0 1 0 0
  // 2 7 0 2
  // 0 5 0 1
  // 8 0 1 3
  LT heads []   = {0, 1, 4, 6, 9};
  LT indices [] = {1, 0, 1, 3, 1, 3, 0, 2, 3};
  VT values []  = {1, 2, 7, 2, 5, 1, 8, 1, 3};
  SpMat<IT, VT> a;
  a.PointersToSpMat(4, 4, 9, SPARSE_CSR, 0,
                    heads, indices, values, false);
  
  // 0 1 0 7
  // 1 0 4 0
  // 0 4 0 1
  // 9 0 1 5
  LT heads2 []   = {0, 2, 4, 6, 9};
  LT indices2 [] = {1, 3, 0, 2, 1, 3, 0, 2, 3};
  VT values2 []  = {1, 7, 1, 4, 4, 1, 9, 1, 5};
  SpMat<IT, VT> b;
  b.PointersToSpMat(4, 4, 9, SPARSE_CSR, 0,
                    heads2, indices2, values2, false);
  
  SpMat<IT, VT> c;
  c.Add(2.0, a, -1.0, b);
  
  // 0  1  0 -7
  // 3 14 -4  4
  // 0  6  0  1
  // 7  0  1  1
  LT heads3 []   = {0, 2, 6, 8, 11};
  LT indices3 [] = {1, 3, 0, 1, 2, 3, 1, 3, 0, 2, 3};
  VT values3 []  = {1, -7, 3, 14, -4, 4, 6, 1, 7, 1, 1};
  SpMat<IT, VT> d;
  d.PointersToSpMat(4, 4, 11, SPARSE_CSR, 0,
                    heads3, indices3, values3, false);
  
  return c.IsEquivalent(d);
}

bool TestConvertFromDense() {
  // 0 1 0 0
  // 2 7 0 2
  // 0 5 0 1
  // 8 0 1 3
  VT dense [] = {0, 1, 0, 0,
                 2, 7, 0, 2,
                 0, 5, 0, 1,
                 8, 0, 1, 3};
  DMat<IT, VT> d(4, 4, DENSE_ROWMAJOR, dense);
  
  SpMat<IT, VT> a;
  a.Convert(d);
  
  LT heads []   = {0, 1, 4, 6, 9};
  LT indices [] = {1, 0, 1, 3, 1, 3, 0, 2, 3};
  VT values []  = {1, 2, 7, 2, 5, 1, 8, 1, 3};
  SpMat<IT, VT> b;
  b.PointersToSpMat(4, 4, 9, SPARSE_CSR, 0,
                    heads, indices, values, false);
  
  return a.IsEquivalent(b);
}

//
// Does not test correctness.
// Just tests if we can save .ppm without crashing.
//
bool TestSavePPM() {
  SpMat<IT, VT> a;
  double initiator[4] = {0.1, 0.2, 0.3, 0.4};
  a.GenerateWithG500(initiator, 6, 0.4);
  a.SavePPM("a.ppm");
  
  LT heads []   = {0, 1, 4, 6, 9};
  LT indices [] = {1, 0, 1, 3, 1, 3, 0, 2, 3};
  VT values []  = {1, 2, 7, 2, 5, 1, 8, 1, 3};
  SpMat<IT, VT> b;
  b.PointersToSpMat(4, 4, 9, SPARSE_CSR, 0,
                    heads, indices, values, false);
  b.SavePPM("b.ppm");
  system("rm a.ppm b.ppm");
  return true;
}

//
// Just tests if the images can be saved with crashing.
// The correctness must be checked by eyes (as of now).
//
bool TestSavePNG() {
  SpMat<IT, VT> a;
  double initiator[4] = {0.1, 0.2, 0.3, 0.4};
  a.GenerateWithG500(initiator, 6, 0.4);
  a.SavePNG("a.png");
  
  LT heads []   = {0, 1, 4, 6, 9};
  LT indices [] = {1, 0, 1, 3, 1, 3, 0, 2, 3};
  VT values []  = {1, 2, 7, 2, 5, 1, 8, 1, 3};
  SpMat<IT, VT> b;
  b.PointersToSpMat(4, 4, 9, SPARSE_CSR, 0,
                    heads, indices, values, false);
  b.SavePNG("b.png");
  system("rm a.png b.png");
  return true;
}

bool TestGenerateWithG500() {
  SpMat<IT, VT> a;
  double initiator[4] = {0.25, 0.25, 0.25, 0.25};
  a.GenerateWithG500(initiator, 3, 0.4);
  a.SaveDense("a");
  
  system("rm a");
  return true;
}

bool TestLoadMatrixMarket() {
  SpMat<IT, VT> a;
  a.LoadMatrixMarket("testdata/general.mtx");
  
  LT heads []   = {0, 1, 4, 6, 9};
  LT indices [] = {1, 0, 1, 3, 1, 3, 0, 2, 3};
  VT values []  = {1, 2, 7, 2, 5, 1, 8, 1, 3};
  SpMat<IT, VT> b;
  b.PointersToSpMat(4, 4, 9, SPARSE_CSR, 0,
                    heads, indices, values, false);
  
  return a.IsEquivalent(b);
}

bool TestLoadMatrixMarketSymmetric() {
  SpMat<IT, VT> a;
  a.LoadMatrixMarket("testdata/sym.mtx");
  
  // 0 1 0 9
  // 1 0 4 0
  // 0 4 0 1
  // 9 0 1 3
  LT heads []   = {0, 2, 4, 6, 9};
  LT indices [] = {1, 3, 0, 2, 1, 3, 0, 2, 3};
  VT values []  = {1, 9, 1, 4, 4, 1, 9, 1, 3};
  SpMat<IT, VT> b;
  b.PointersToSpMat(4, 4, 9, SPARSE_CSR, 0,
                    heads, indices, values, false);
  
  return a.IsEquivalent(b);
}

bool TestSetIdentity() {
  SpMat<IT, VT> I;
  I.SetIdentity(5);
  
  DMat<IT, VT> a(5, 5, DENSE_ROWMAJOR);
  a.Generate();
  DMat<IT, VT> b(DENSE_ROWMAJOR);
  I.MultiplyAdd(a, b);
  
  return b.IsEquivalent(a);
}

bool TestPartitionToSubmatrices() {
  // 2D Row Major layout.
  // 0 1 0 9 0 1 0 9
  // 2 0 4 0 2 0 4 0
  // 3 5 0 1 3 5 0 1
  // 3 0 2 7 3 0 2 7
  // 4 9 0 0 4 9 0 0
  // 2 0 5 0 2 0 5 0
  LT heads []   = {0, 4, 8, 14, 20, 24, 28};
  LT indices [] = {1, 3, 5, 7,
                   0, 2, 4, 6,
                   0, 1, 3, 4, 5, 7,
                   0, 2, 3, 4, 6, 7,
                   0, 1, 4, 5,
                   0, 2, 4, 6};
  VT values [] = {1, 9, 1, 9,
                  2, 4, 2, 4,
                  3, 5, 1, 3, 5, 1,
                  3, 2, 7, 3, 2, 7,
                  4, 9, 4, 9,
                  2, 5, 2, 5};
  SpMat<IT, VT> big;
  big.PointersToSpMat(6, 8, 28, SPARSE_CSR, 0,
                      heads, indices, values, 0);
  
  bool res = true;
  SpMat<IT, VT> *submats;
  
  //
  // Block columns.
  //
  IT row_counts1 [] = {6};
  IT row_displs1 [] = {0, 6};
  IT col_counts1 [] = {2, 3, 3};
  IT col_displs1 [] = {0, 2, 5, 8};
  submats = big.PartitionToSubmatrices(1, row_counts1, row_displs1,
                                       3, col_counts1, col_displs1);
  
  // 0 1 | 0 9 0 | 1 0 9
  // 2 0 | 4 0 2 | 0 4 0
  // 3 5 | 0 1 3 | 5 0 1
  // 3 0 | 2 7 3 | 0 2 7
  // 4 9 | 0 0 4 | 9 0 0
  // 2 0 | 5 0 2 | 0 5 0
  LT heads1_1 []   = {0, 1, 2, 4, 5, 7, 8};
  LT indices1_1 [] = {1, 0, 0, 1, 0, 0, 1, 0};
  VT values1_1 []  = {1, 2, 3, 5, 3, 4, 9, 2};
  SpMat<IT, VT> sol1_1;
  sol1_1.PointersToSpMat(6, 2, 8, SPARSE_CSR, 0,
                         heads1_1, indices1_1, values1_1, false);
  res &= sol1_1.IsEquivalent(submats[0]);
  
  LT heads1_2[]   = {0, 1, 3, 5, 8, 9, 11};
  LT indices1_2[] = {1, 0, 2, 1, 2, 0, 1, 2, 2, 0, 2};
  VT values1_2[]  = {9, 4, 2, 1, 3, 2, 7, 3, 4, 5, 2};
  SpMat<IT, VT> sol1_2;
  sol1_2.PointersToSpMat(6, 3, 11, SPARSE_CSR, 0,
                         heads1_2, indices1_2, values1_2, false);
  res &= sol1_2.IsEquivalent(submats[1]);
  
  LT heads1_3[]   = {0, 2, 3, 5, 7, 8, 9};
  LT indices1_3[] = {0, 2, 1, 0, 2, 1, 2, 0, 1};
  VT values1_3[]  = {1, 9, 4, 5, 1, 2, 7, 9, 5};
  SpMat<IT, VT> sol1_3;
  sol1_3.PointersToSpMat(6, 3, 9, SPARSE_CSR, 0,
                         heads1_3, indices1_3, values1_3, false);
  res &= sol1_3.IsEquivalent(submats[2]);
  
  delete [] submats;
  
  //
  // Block rows.
  //
  IT row_counts2 [] = {2, 1, 3};
  IT row_displs2 [] = {0, 2, 3, 6};
  IT col_counts2 [] = {8};
  IT col_displs2 [] = {0, 8};
  submats = big.PartitionToSubmatrices(3, row_counts2, row_displs2,
                                       1, col_counts2, col_displs2);
  
  // 0 1 0 9 0 1 0 9
  // 2 0 4 0 2 0 4 0
  // ---------------
  // 3 5 0 1 3 5 0 1
  // ---------------
  // 3 0 2 7 3 0 2 7
  // 4 9 0 0 4 9 0 0
  // 2 0 5 0 2 0 5 0
  LT heads2_1[]   = {0, 4, 8};
  LT indices2_1[] = {1, 3, 5, 7, 0, 2, 4, 6};
  VT values2_1[]  = {1, 9, 1, 9, 2, 4, 2, 4};
  SpMat<IT, VT> sol2_1;
  sol2_1.PointersToSpMat(2, 8, 8, SPARSE_CSR, 0,
                         heads2_1, indices2_1, values2_1, false);
  res &= sol2_1.IsEquivalent(submats[0]);
  
  LT heads2_2[]   = {0, 6};
  LT indices2_2[] = {0, 1, 3, 4, 5, 7};
  VT values2_2[]  = {3, 5, 1, 3, 5, 1};
  SpMat<IT, VT> sol2_2;
  sol2_2.PointersToSpMat(1, 8, 6, SPARSE_CSR, 0,
                         heads2_2, indices2_2, values2_2, false);
  res &= sol2_2.IsEquivalent(submats[1]);
  
  LT heads2_3[]   = {0, 6, 10, 14};
  LT indices2_3[] = {0, 2, 3, 4, 6, 7, 0, 1, 4, 5, 0, 2, 4, 6};
  VT values2_3[]  = {3, 2, 7, 3, 2, 7, 4, 9, 4, 9, 2, 5, 2, 5};
  SpMat<IT, VT> sol2_3;
  sol2_3.PointersToSpMat(3, 8, 14, SPARSE_CSR, 0,
                         heads2_3, indices2_3, values2_3, false);
  res &= sol2_3.IsEquivalent(submats[2]);
  delete [] submats;
  
  //
  // 2D
  //
  IT row_counts3 [] = {1, 3, 2};
  IT row_displs3 [] = {0, 1, 4, 6};
  IT col_counts3 [] = {3, 2, 3};
  IT col_displs3 [] = {0, 3, 5, 8};
  submats = big.PartitionToSubmatrices(3, row_counts3, row_displs3,
                                       3, col_counts3, col_displs3);
  
  // 0 1 0 | 9 0 | 1 0 9
  // -------------------
  // 2 0 4 | 0 2 | 0 4 0
  // 3 5 0 | 1 3 | 5 0 1
  // 3 0 2 | 7 3 | 0 2 7
  // -------------------
  // 4 9 0 | 0 4 | 9 0 0
  // 2 0 5 | 0 2 | 0 5 0
  LT heads3_1 []   = {0, 1};
  LT indices3_1 [] = {1};
  VT values3_1 []  = {1};
  SpMat<IT, VT> sol3_1;
  sol3_1.PointersToSpMat(1, 3, 1, SPARSE_CSR, 0,
                         heads3_1, indices3_1, values3_1, false);
  res &= sol3_1.IsEquivalent(submats[0]);
  
  LT heads3_2 []   = {0, 1};
  LT indices3_2 [] = {0};
  VT values3_2 []  = {9};
  SpMat<IT, VT> sol3_2;
  sol3_2.PointersToSpMat(1, 2, 1, SPARSE_CSR, 0,
                         heads3_2, indices3_2, values3_2, false);
  res &= sol3_2.IsEquivalent(submats[1]);
  
  LT heads3_3 []   = {0, 2};
  LT indices3_3 [] = {0, 2};
  VT values3_3 []  = {1, 9};
  SpMat<IT, VT> sol3_3;
  sol3_3.PointersToSpMat(1, 3, 2, SPARSE_CSR, 0,
                         heads3_3, indices3_3, values3_3, false);
  res &= sol3_3.IsEquivalent(submats[2]);

  LT heads3_4 []   = {0, 2, 4, 6};
  LT indices3_4 [] = {0, 2, 0, 1, 0, 2};
  VT values3_4 []  = {2, 4, 3, 5, 3, 2};
  SpMat<IT, VT> sol3_4;
  sol3_4.PointersToSpMat(3, 3, 6, SPARSE_CSR, 0,
                         heads3_4, indices3_4, values3_4, false);
  res &= sol3_4.IsEquivalent(submats[3]);
  
  LT heads3_5 []   = {0, 1, 3, 5};
  LT indices3_5 [] = {1, 0, 1, 0, 1};
  VT values3_5 []  = {2, 1, 3, 7, 3};
  SpMat<IT, VT> sol3_5;
  sol3_5.PointersToSpMat(3, 2, 5, SPARSE_CSR, 0,
                         heads3_5, indices3_5, values3_5, false);
  res &= sol3_5.IsEquivalent(submats[4]);

  LT heads3_6 []   = {0, 1, 3, 5};
  LT indices3_6 [] = {1, 0, 2, 1, 2};
  VT values3_6 []  = {4, 5, 1, 2, 7};
  SpMat<IT, VT> sol3_6;
  sol3_6.PointersToSpMat(3, 3, 5, SPARSE_CSR, 0,
                         heads3_6, indices3_6, values3_6, false);
  res &= sol3_6.IsEquivalent(submats[5]);

  LT heads3_7 []   = {0, 2, 4};
  LT indices3_7 [] = {0, 1, 0, 2};
  VT values3_7 []  = {4, 9, 2, 5};
  SpMat<IT, VT> sol3_7;
  sol3_7.PointersToSpMat(2, 3, 4, SPARSE_CSR, 0,
                         heads3_7, indices3_7, values3_7, false);
  res &= sol3_7.IsEquivalent(submats[6]);
  
  LT heads3_8 []   = {0, 1, 2};
  LT indices3_8 [] = {1, 1};
  VT values3_8 []  = {4, 2};
  SpMat<IT, VT> sol3_8;
  sol3_8.PointersToSpMat(2, 2, 2, SPARSE_CSR, 0,
                         heads3_8, indices3_8, values3_8, false);
  res &= sol3_8.IsEquivalent(submats[7]);
  
  LT heads3_9 []   = {0, 1, 2};
  LT indices3_9 [] = {0, 1};
  VT values3_9 []  = {9, 5};
  SpMat<IT, VT> sol3_9;
  sol3_9.PointersToSpMat(2, 3, 2, SPARSE_CSR, 0,
                         heads3_9, indices3_9, values3_9, false);
  res &= sol3_9.IsEquivalent(submats[8]);
  delete [] submats;
  
  return res;
}

bool TestPartitionToSubmatrices2() {
  SpMat<IT, VT> A;
  A.Generate(6, 100, 0.3);
  
  IT row_counts [] = {6};
  IT row_displs [] = {0, 6};
  IT col_counts [] = {7, 6, 6, 6,
                      7, 6, 6, 6,
                      7, 6, 6, 6,
                      7, 6, 6, 6};
  IT col_displs [] = {0,  7, 13, 19, 25,
                         32, 38, 44, 50,
                         57, 63, 69, 75,
                         82, 88, 94, 100};
  SpMat<IT, VT> *submats =
      A.PartitionToSubmatrices(1, row_counts, row_displs,
                               16, col_counts, col_displs);
  return true;
}

bool TestMultiply() {
  SpMat<IT, VT> a, b;
  int m, n, k;
  m = 345; n = 456; k = 543;
//  m = n = k = 10;
  a.Generate(m, k, 0.3, 11);  // Generates with
  b.Generate(k, n, 0.4, 12);  // different seeds.

  SpMat<IT, VT> c, d;
  // Tests the default library (LIB_BLAS or LIB_MKL).
  a.Multiply(b, c);
  c.RemoveZeroes();  // mkl_dcsrmultcsr can produce zero 'nonzero' elements.

  // Tests the naive library.
  a.SetMathLib(LIB_NAIVE);
  a.Multiply(b, d);
  
  // TODO(penpornk): See why this part segfaulted.
//  DMat<IT, VT> ad(a);
//  DMat<IT, VT> bd(b);
//  DMat<IT, VT> cd;
//  ad.MultiplyAdd(bd, cd);
//
//  SpMat<IT, VT> cs;
//  cs.Convert(cd);
  
  bool res = true;
  res &= c.IsEquivalent(d);
//  res &= c.IsEquivalent(cs);
//  res &= d.IsEquivalent(cs);
  return res;
}

bool TestMakeBlockDiagonal() {
  // TODO(penpornk): Automate the check process. Try larger n.
  int n = 3;
  SpMat<IT, VT> *M = new SpMat<IT, VT>[n];
  for (int i = 0; i < n; ++i) {
    M[i].Generate(2 + rand() % 5, 2 + rand() % 5, 0.3);
//    M[i].PrintDense();
  }
  
  SpMat<IT, VT> res;
  res.MakeBlockDiagonal(M, n);
  
//  res.PrintDense();
  
  delete [] M;
  return true;
}

bool TestRemoveZeroes() {
  int m, n;
  m = 237; n = 329;
  
  SpMat<IT, VT> a;
  a.Generate(m, n, 0.4);
  IT old_nnz = a.nnz_;
  
  DMat<IT, VT> d(a);
  
  // Randomly zeroes out some elements.
  for (int k = 0; k < 20; ++k) {
    IT idx = rand() % old_nnz;
    a.values_[idx] = 0.0;
    
    // Binary search for row index.
    IT min = 0, max = a.dim1();
    IT mid = (min + max)/2;
    while (!(a.headptrs_[mid] <= idx && idx < a.headptrs_[mid+1])) {
      if (idx < a.headptrs_[mid])
        max = mid;
      else if (idx >= a.headptrs_[mid+1])
        min = mid;
      mid = (min + max)/2;
    }
    d.values_[mid * d.lda_ + a.indices_[idx]] = 0.0;
  }
  
  a.RemoveZeroes();
  
  SpMat<IT, VT> s;
  s.Convert(d);
  bool res = (a.nnz_ == old_nnz - 20);
  res &= a.IsEquivalent(s);
  return res;
}

}  // namespace spdm3

#define ADDTEST(fn, name) tests.push_back(std::make_pair(fn, name));

int main(int argc, char *argv[]) {
  // Registers test cases.
  std::list< std::pair<std::function<bool()>, std::string> > tests;
  ADDTEST(spdm3::TestDefaultConstructor, "TestDefaultConstructor");
  ADDTEST(spdm3::TestGenerate, "TestGenerate");
  ADDTEST(spdm3::TestFrobeniusNorm, "TestFrobeniusNorm");
  ADDTEST(spdm3::TestElmtWiseOp, "TestElmtWiseOp");
  ADDTEST(spdm3::TestFill, "TestFill");
  ADDTEST(spdm3::TestConcatenation, "TestConcatenation");
  ADDTEST(spdm3::TestGetShallowCopy, "TestGetShallowCopy");
  ADDTEST(spdm3::TestGetStructure, "TestGetStructure");
  ADDTEST(spdm3::TestGetDeepCopy, "TestGetDeepCopy");
  ADDTEST(spdm3::TestDeepCopy, "TestDeepCopy");
  ADDTEST(spdm3::TestSwap, "TestSwap");
  ADDTEST(spdm3::TestExpandBuffers, "TestExpandBuffers");
  ADDTEST(spdm3::TestLoadSave, "TestLoadSave");
  ADDTEST(spdm3::TestMultiplyAdd, "TestMultiplyAdd");
  ADDTEST(spdm3::TestSubmatrix, "TestSubmatrix");
  ADDTEST(spdm3::TestDotMultiplyI, "TestDotMultiplyI");
  ADDTEST(spdm3::TestDotMultiplyTransposeI, "TestDotMultiplyTransposeI");
  ADDTEST(spdm3::TestReduce, "TestReduce");
  ADDTEST(spdm3::TestGetElmt, "TestGetElmt");
  ADDTEST(spdm3::TestIsSymmetric, "TestIsSymmetric");
  ADDTEST(spdm3::TestDiagonal, "TestDiagonal");
  ADDTEST(spdm3::TestAddSparse, "TestAddSparse");
  ADDTEST(spdm3::TestConvertFromDense, "TestConvertFromDense");
  ADDTEST(spdm3::TestSavePPM, "TestSavePPM");
  ADDTEST(spdm3::TestGenerateWithG500, "TestGenerateWithG500");
  ADDTEST(spdm3::TestLoadMatrixMarket, "TestLoadMatrixMarket");
  ADDTEST(spdm3::TestLoadMatrixMarketSymmetric, "TestLoadMatrixMarketSymmetric");
  ADDTEST(spdm3::TestSetIdentity, "TestSetIdentity");
  ADDTEST(spdm3::TestPartitionToSubmatrices, "TestPartitionToSubmatrices");
  ADDTEST(spdm3::TestPartitionToSubmatrices2, "TestPartitionToSubmatrices2");
  ADDTEST(spdm3::TestMultiply, "TestMultiply");
  ADDTEST(spdm3::TestMakeBlockDiagonal, "TestBlockDiagonal");
  ADDTEST(spdm3::TestRemoveZeroes, "TestRemoveZeroes");
  
#ifdef USE_PNG
  ADDTEST(spdm3::TestSavePNG, "TestSavePNG");
#endif  /* USE_PNG */
  
  // Runs tests.
  int failed = 0;
  for (auto test : tests) {
    bool res = test.first();
    std::cout << std::setw(50) << test.second << ": ";
    if (res) {
      std::cout << "PASSED." << std::endl;
    } else {
      ++failed;
      std::cout << "FAILED." << std::endl;
    }
  }
  
  // Summarizes.
  if (!failed)
    std::cout << "All " << tests.size() << " tests passed." << std::endl;
  else
    std::cout << failed << " out of " << tests.size()
              << " tests failed." << std::endl;
  return 0;
}
