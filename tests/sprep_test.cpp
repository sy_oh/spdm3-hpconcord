#include "distspmat.h"

#include <functional>
#include <iostream>
#include <list>
#include <sstream>
#include <tuple>
#include <utility>

#include "distio.h"
#include "utility.h"

#define IT  int
#define VT  double

namespace spdm3 {

bool Test1DRow(int layers) {
  Comm *c1 = GetComms(MPI_COMM_WORLD, ONED_BLOCK_ROW, 1);
  Comm *cnew = GetComms(MPI_COMM_WORLD, ONED_BLOCK_ROW, layers);
  DistSpMat<IT, VT> a(c1);
  DistSpMat<IT, VT> sol(cnew);
  a.GenerateUniform(123, 111, 0.5);
  sol.GenerateUniform(123, 111, 0.5);
  
  a.Replicate(cnew);

  delete [] c1;
  delete [] cnew;
  
  return a.mat_.IsEquivalent(sol.mat_);
}

bool Test1DCol(int layers) {
  Comm *c1 = GetComms(MPI_COMM_WORLD, ONED_BLOCK_COLUMN, 1);
  Comm *cnew = GetComms(MPI_COMM_WORLD, ONED_BLOCK_COLUMN, layers);
  DistSpMat<IT, VT> a(c1);
  DistSpMat<IT, VT> sol(cnew);
  a.GenerateUniform(123, 111, 0.5);
  sol.GenerateUniform(123, 111, 0.5);
  
  a.Replicate(cnew);

  delete [] c1;
  delete [] cnew;
  
  return a.mat_.IsEquivalent(sol.mat_);
}

bool Test2DRow(int layers) {
  Comm *c1 = GetComms(MPI_COMM_WORLD, TWOD_ROWMAJOR, 1);
  Comm *cnew = GetComms(MPI_COMM_WORLD, TWOD_ROWMAJOR, layers);
  DistSpMat<IT, VT> a(c1);
  DistSpMat<IT, VT> sol(cnew);
  a.GenerateUniform(123, 111, 0.5);
  sol.GenerateUniform(123, 111, 0.5);
  
  a.Replicate(cnew);

  delete [] c1;
  delete [] cnew;
  
  return a.mat_.IsEquivalent(sol.mat_);
}

bool Test2DCol(int layers) {
  Comm *c1 = GetComms(MPI_COMM_WORLD, TWOD_COLMAJOR, 1);
  Comm *cnew = GetComms(MPI_COMM_WORLD, TWOD_COLMAJOR, layers);
  DistSpMat<IT, VT> a(c1);
  DistSpMat<IT, VT> sol(cnew);
  a.GenerateUniform(123, 111, 0.5);
  sol.GenerateUniform(123, 111, 0.5);
  
  a.Replicate(cnew);

  delete [] c1;
  delete [] cnew;
  
  return a.mat_.IsEquivalent(sol.mat_);
}

bool Test1DRowShift(int layers) {
  Comm *c1 = GetComms(MPI_COMM_WORLD, ONED_BLOCK_ROW, 1);
  Comm *cnew = GetComms(MPI_COMM_WORLD, ONED_BLOCK_ROW, layers);
  DistSpMat<IT, VT> a(c1);
  a.GenerateUniform(123, 111, 0.5);
  a.Replicate(cnew);
  a.SaveTeam("a");
  MPI_Barrier(MPI_COMM_WORLD);
  
  bool res = true;
  int check = a.team_id();
  for (int i = 0; i < a.teams() + 2; ++i) {
    a.Shift(LAYER_COL, 1);
    check = (check + 1) % a.teams();
    SpMat<IT, VT> b;
    b.Load("a", check);
    res &= b.IsEquivalent(a.mat_);
  }

  // Cleans up.
  MPI_Barrier(MPI_COMM_WORLD);
  if (a.layer_id() == 0) {
    char cmd[20];
    sprintf(cmd, "rm a-%05d", a.lrank());
    system(cmd);
  }
  delete [] c1;
  delete [] cnew;
  
  return res;
}

bool Test1DRowShiftPiggyback(int layers) {
  Comm *c1 = GetComms(MPI_COMM_WORLD, ONED_BLOCK_ROW, 1);
  Comm *cnew = GetComms(MPI_COMM_WORLD, ONED_BLOCK_ROW, layers);
  DistSpMat<IT, VT> a(c1);
  a.GenerateUniform(123, 111, 0.5);
  a.Replicate(cnew);
  a.SaveTeam("a");
  VT extras[2], sum[2] = {0.0, 0.0};
  extras[0] = 1.0;
  extras[1] = a.team_id();
  a.AddPiggyback(extras, 2);
  MPI_Barrier(MPI_COMM_WORLD);
  
  bool res = true;
  int check = a.team_id();
  for (int i = 0; i < a.teams(); ++i) {
    a.ShiftPiggyback(LAYER_COL, 1, 2);
    sum[0] += a.GetPiggyback()[0];
    sum[1] += a.GetPiggyback()[1];
    check = (check + 1) % a.teams();
    SpMat<IT, VT> b;
    b.Load("a", check);
    res &= b.IsEquivalent(a.mat_);
  }
  
  res &= (FEQ(sum[0], a.teams()));
  res &= (FEQ(sum[1], ((a.teams()-1) * a.teams()) / 2.0));

  // Cleans up.
  MPI_Barrier(MPI_COMM_WORLD);
  if (a.layer_id() == 0) {
    char cmd[20];
    sprintf(cmd, "rm a-%05d", a.lrank());
    system(cmd);
  }
  delete [] c1;
  delete [] cnew;
  
  return res;
}

bool TestWeirdLayoutBlockColumn(int layers) {
  Comm *c1 = GetComms(MPI_COMM_WORLD, ONED_BLOCK_COLUMN, 1);
  Comm *cnew = GetComms(MPI_COMM_WORLD, ONED_BLOCK_COLUMN, layers);
  
  DistSpMat<IT, VT> a(c1);
  a.GenerateUniform(123, 123, 0.6);
  a.Replicate(cnew);
  
  DistSpMat<IT, VT> b(cnew);
  b.WeirdLayoutBlockColumn(a);
  
  // Info.
  int nteams = a.teams();
  int nblocks = nteams / layers;
  int col_id = a.team_id();
  
  // Forms displs array.
  IT *counts = new IT[nblocks];
  IT *displs = new IT[nblocks + 1];
  int blk_idx = (a.team_id() + a.layer_id()) % nteams;
  displs[0] = 0;
  for (int i = 0; i < nblocks; ++i) {
    counts[i] = a.col_counts_[blk_idx];
    displs[i+1] = displs[i] + counts[i];
    blk_idx = (blk_idx + layers) % nteams;
  }
  
  // Actually compares subblocks (in a different way from WeirdLayout()).
  bool res = true;
  int checked_subblocks = 0;
  for (int row_id = 0; row_id < nteams; ++row_id) {
    int layer_id = (row_id - col_id + nteams) % layers;
    if (layer_id != a.layer_id())
      continue;
    
    int inner = (row_id - col_id + nteams) % nteams;
    blk_idx = (nblocks + inner / layers) % nblocks;
    
    // Checks if submatrices are equal manually.
    // TODO(penpornk): Minus column offset (in case both matrices
    // have different number of columns.
    bool equal = true;
    int num_rows = a.col_counts_[row_id];
    for (int i = 0; i < num_rows; ++i) {
      int i_a = a.col_displs_[row_id] + i;
      int i_b = displs[blk_idx] + i;
      int k_a = a.mat_.headptrs_[i_a];
      int k_b = b.mat_.headptrs_[i_b];
      while (k_a < a.mat_.headptrs_[i_a + 1]) {
        if (a.mat_.indices_[k_a] != b.mat_.indices_[k_b] ||
            !FEQ(a.mat_.values_[k_a], b.mat_.values_[k_b])) {
          a.print("[%d, %d]: %d != %d or %lf != %lf\n",
              k_a, k_b, a.mat_.indices_[k_a], b.mat_.indices_[k_b],
              a.mat_.values_[k_a], b.mat_.values_[k_b]);
          res = false;
          break;
        }
        ++k_a;
        ++k_b;
      }
      res &= (k_b == b.mat_.headptrs_[i_b + 1]);
    }
    ++checked_subblocks;
  }
  res &= (checked_subblocks == nblocks);
  
  // Deallocation.
  delete [] c1;
  delete [] cnew;
  delete [] counts;
  delete [] displs;
  
  return res;
}

bool TestWeirdLayoutBlockRow(int layers) {
  Comm *c1 = GetComms(MPI_COMM_WORLD, ONED_BLOCK_ROW, 1);
  Comm *cnew = GetComms(MPI_COMM_WORLD, ONED_BLOCK_ROW, layers);
  
  DistSpMat<IT, VT> A(c1);
  A.GenerateUniform(151, 151, 0.6);
  A.Replicate(cnew);
  
  DistSpMat<IT, VT> B(cnew);
  B.WeirdLayoutBlockRow(A);
  
  // Info.
  int nteams = A.teams();
  int nblocks = nteams / layers;
  int col_id = A.team_id();
  
  // Checking.
  bool res = true;
  int nrows = A.lrows();
  int col_offset = 0;
  int blk_idx = (A.team_id() + layers - (A.layer_id() % layers)) % layers;
  for (int i = 0; i < nblocks; ++i) {
    SpMat<IT, VT> subA, subB;
    int ncols = A.row_counts_[blk_idx];
    subA.Submatrix(A.mat_, 0, A.row_displs_[blk_idx], nrows, ncols);
    subB.Submatrix(B.mat_, 0, col_offset, nrows, ncols);
    res &= subA.IsEquivalent(subB);
    blk_idx += layers;
    col_offset += ncols;
  }
  
  // Deallocation.
  delete [] c1;
  delete [] cnew;
  
  return res;
}

bool TestWeirdLayoutBlockRowTransposed(int layers) {
  Comm *c1 = GetComms(MPI_COMM_WORLD, ONED_BLOCK_ROW, 1);
  Comm *cnew = GetComms(MPI_COMM_WORLD, ONED_BLOCK_ROW, layers);
  
  DistSpMat<IT, VT> A(c1);
  A.GenerateUniform(157, 157, 0.6);
  A.Replicate(cnew);
  
  DistSpMat<IT, VT> B(cnew);
  B.WeirdLayoutBlockRowTransposed(A);
  
  // Info.
  int nteams = A.teams();
  int nblocks = nteams / layers;
  int col_id = A.team_id();
  
  // Checking.
  bool res = true;
  int nrows = A.lrows();
  int col_offset = 0;
  int blk_idx = (A.team_id() + A.layer_id()) % layers;
  for (int i = 0; i < nblocks; ++i) {
    SpMat<IT, VT> subA, subB;
    int ncols = A.row_counts_[blk_idx];
    subA.Submatrix(A.mat_, 0, A.row_displs_[blk_idx], nrows, ncols);
    subB.Submatrix(B.mat_, 0, col_offset, nrows, ncols);
    res &= subA.IsEquivalent(subB);
    blk_idx += layers;
    col_offset += ncols;
  }
  
  // Deallocation.
  delete [] c1;
  delete [] cnew;
  
  return res;
}

bool TestWeirdLayoutDiagonal(int layers) {
  Comm *c1 = GetComms(MPI_COMM_WORLD, ONED_BLOCK_ROW, 1);
  Comm *cnew = GetComms(MPI_COMM_WORLD, ONED_BLOCK_ROW, layers);
  
  DistSpMat<IT, VT> A(c1);
  A.GenerateUniform(171, 171, 0.6);
  A.Replicate(cnew);
  
  DistSpMat<IT, VT> B(cnew);
  B.WeirdLayoutBlockRow(A);
  
  DistSpMat<IT, VT> C(cnew);
  C.WeirdLayoutDiagonal(B);

  DistSpMat<IT, VT> D(cnew);
  D.Diagonal(A);
  
  DistSpMat<IT, VT> E(cnew);
  E.WeirdLayoutBlockRow(D);
  
  bool res = C.mat_.IsEquivalent(E.mat_);
  
  // Deallocation.
  delete [] c1;
  delete [] cnew;
  
  return res;
}

bool TestWeirdLayoutDiagonalTransposed(int layers) {
  Comm *c1 = GetComms(MPI_COMM_WORLD, ONED_BLOCK_ROW, 1);
  Comm *cnew = GetComms(MPI_COMM_WORLD, ONED_BLOCK_ROW, layers);
  
  DistSpMat<IT, VT> A(c1);
  A.GenerateUniform(171, 171, 0.6);
  A.Replicate(cnew);
  
  DistSpMat<IT, VT> B(cnew);
  B.WeirdLayoutBlockRowTransposed(A);
  
  DistSpMat<IT, VT> C(cnew);
  C.WeirdLayoutDiagonalTransposed(B);

  DistSpMat<IT, VT> D(cnew);
  D.Diagonal(A);
  
  DistSpMat<IT, VT> E(cnew);
  E.WeirdLayoutBlockRowTransposed(D);
  
  bool res = C.mat_.IsEquivalent(E.mat_);
  
  // Deallocation.
  delete [] c1;
  delete [] cnew;
  
  return res;
}

bool TestLoadMatrixMarket(int layers) {
  SpMat<IT, VT> a;
  a.Generate(99, 99, 0.4);
  a.SaveMatrixMarket("a.mkt");
  
  Comm *c1D = GetComms(MPI_COMM_WORLD, ONED_BLOCK_ROW, layers);
  DistSpMat<IT, VT> b(c1D);
  b.LoadMatrixMarket("a.mkt");
  
  SpMat<IT, VT> wb = b.GetWholeMatrix();
  
  delete [] c1D;
  system("rm -f a.mkt");
  return wb.IsEquivalent(a);
}

}  // namespace spdm3

#define ADDTEST(fn, name, layers) \
    tests.push_back(std::make_tuple((std::function<bool(int)>) fn, name, layers));

int main(int argc, char* argv[]) {
  // Registers test cases.
  std::list< std::tuple<std::function<bool(int)>, const char*, int> > tests;
  
  // 1D block row.
  ADDTEST(spdm3::Test1DRow, "Test1DRow", 2);
  ADDTEST(spdm3::Test1DRow, "Test1DRow", 4);
  ADDTEST(spdm3::Test1DRow, "Test1DRow", 8);
  ADDTEST(spdm3::Test1DRow, "Test1DRow", 16);
  
  // 1D block column.
  ADDTEST(spdm3::Test1DCol, "Test1DCol", 2);
  ADDTEST(spdm3::Test1DCol, "Test1DCol", 4);
  ADDTEST(spdm3::Test1DCol, "Test1DCol", 8);
  ADDTEST(spdm3::Test1DCol, "Test1DCol", 16);
  
  // 2D row major.
  ADDTEST(spdm3::Test2DRow, "Test2DRow", 2);
  ADDTEST(spdm3::Test2DRow, "Test2DRow", 4);
  ADDTEST(spdm3::Test2DRow, "Test2DRow", 8);
  ADDTEST(spdm3::Test2DRow, "Test2DRow", 16);
  
  // 2D col major.
  ADDTEST(spdm3::Test2DCol, "Test2DCol", 2);
  ADDTEST(spdm3::Test2DCol, "Test2DCol", 4);
  ADDTEST(spdm3::Test2DCol, "Test2DCol", 8);
  ADDTEST(spdm3::Test2DCol, "Test2DCol", 16);
  
  // Shifting.
  ADDTEST(spdm3::Test1DRowShift, "Test1DRowShift", 2);
  ADDTEST(spdm3::Test1DRowShift, "Test1DRowShift", 4);
  ADDTEST(spdm3::Test1DRowShiftPiggyback, "Test1DRowShiftPiggyback", 2);
  ADDTEST(spdm3::Test1DRowShiftPiggyback, "Test1DRowShiftPiggyback", 4);
  
  // Weird layout.
  ADDTEST(spdm3::TestWeirdLayoutBlockColumn, "TestWeirdLayoutBlockColumn", 2);
  ADDTEST(spdm3::TestWeirdLayoutBlockColumn, "TestWeirdLayoutBlockColumn", 4);
  
  ADDTEST(spdm3::TestWeirdLayoutBlockRow, "TestWeirdLayoutBlockRow", 1);
  ADDTEST(spdm3::TestWeirdLayoutBlockRow, "TestWeirdLayoutBlockRow", 2);
  ADDTEST(spdm3::TestWeirdLayoutBlockRow, "TestWeirdLayoutBlockRow", 4);

  ADDTEST(spdm3::TestWeirdLayoutBlockRowTransposed, "TestWeirdLayoutBlockRowTransposed", 1);
  ADDTEST(spdm3::TestWeirdLayoutBlockRowTransposed, "TestWeirdLayoutBlockRowTransposed", 2);
  ADDTEST(spdm3::TestWeirdLayoutBlockRowTransposed, "TestWeirdLayoutBlockRowTransposed", 4);
  
  ADDTEST(spdm3::TestWeirdLayoutDiagonal, "TestWeirdLayoutDiagonal", 1);
  ADDTEST(spdm3::TestWeirdLayoutDiagonal, "TestWeirdLayoutDiagonal", 2);
  ADDTEST(spdm3::TestWeirdLayoutDiagonal, "TestWeirdLayoutDiagonal", 4);
  
  ADDTEST(spdm3::TestWeirdLayoutDiagonalTransposed, "TestWeirdLayoutDiagonalTransposed", 1);
  ADDTEST(spdm3::TestWeirdLayoutDiagonalTransposed, "TestWeirdLayoutDiagonalTransposed", 2);
  ADDTEST(spdm3::TestWeirdLayoutDiagonalTransposed, "TestWeirdLayoutDiagonalTransposed", 4);
  
  ADDTEST(spdm3::TestLoadMatrixMarket, "TestLoadMatrixMarket", 1);
  ADDTEST(spdm3::TestLoadMatrixMarket, "TestLoadMatrixMarket", 2);
  ADDTEST(spdm3::TestLoadMatrixMarket, "TestLoadMatrixMarket", 4);
  ADDTEST(spdm3::TestLoadMatrixMarket, "TestLoadMatrixMarket", 8);
  ADDTEST(spdm3::TestLoadMatrixMarket, "TestLoadMatrixMarket", 16);
  
  MPI_Init(&argc, &argv);
  int p = 1, rank = 0;
  MPI_Comm_size(MPI_COMM_WORLD, &p);
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  if (p != 16) {
    std::cout << "This test must be run with 16 MPI processes. "
              << "Terminating..." << std::endl;
    exit(1);
  }
  
  // Runs tests.
  int failed = 0;
  for (auto test : tests) {
    int layers = std::get<2>(test);
    bool res = std::get<0>(test)(layers);
    if (rank == 0) {
      MPI_Reduce(MPI_IN_PLACE, &res, 1, MPI_BYTE, MPI_BAND, 0, MPI_COMM_WORLD);
      std::stringstream ss;
      ss << std::get<1>(test) << "C" << layers;
      std::cout << std::setw(50) << ss.str() << ": ";
      if (res) {
        std::cout << "PASSED." << std::endl;
      } else {
        ++failed;
        std::cout << "FAILED." << std::endl;
      }
    } else {
      MPI_Reduce(&res, NULL, 1, MPI_BYTE, MPI_BAND, 0, MPI_COMM_WORLD);
    }
  }
  
  // Summarizes.
  if (rank == 0) {
    if (!failed)
      std::cout << "All " << tests.size() << " tests passed." << std::endl;
    else
      std::cout << failed << " out of " << tests.size()
                << " tests failed." << std::endl;
  }

  MPI_Finalize();
  return 0;
}
