#include "distspmat.h"

#include <functional>
#include <list>
#include <utility>

#include "distio.h"
#include "utility.h"

#define IT  int
#define VT  double

namespace spdm3 {

bool TestGenerateUniform() {
  Comm *c1D = GetComms(MPI_COMM_WORLD);
  Comm *c2D = GetComms(MPI_COMM_WORLD, TWOD_ROWMAJOR, 1);
  DistSpMat<IT, VT> a(c1D);
  DistSpMat<IT, VT> b(c2D);
  a.GenerateUniform(15, 15, 0.5);
  b.GenerateUniform(15, 15, 0.5);
  delete [] c1D;
  delete [] c2D;
  return true;
}

bool TestGenerateWithG500OneD() {
  Comm *c1D = GetComms(MPI_COMM_WORLD);
  DistSpMat<IT, VT> a(c1D);
  double initiator[4] = {0.25, 0.25, 0.25, 0.25};
  int num_graphs = c1D[WORLD].size * 2;
  a.GenerateWithG500(initiator, 4, 0.5, num_graphs);
  a.SaveLocal("a");

  // Cleans up.
  char cmd[20];
  sprintf(cmd, "rm a-%05d", c1D[WORLD].rank);
  system(cmd);
  delete [] c1D;

  return true;
}

bool TestGenerateWithG500TwoD() {
  Comm *c2D = GetComms(MPI_COMM_WORLD, TWOD_ROWMAJOR, 1);
  DistSpMat<IT, VT> a(c2D);
  double initiator[4] = {0.25, 0.25, 0.25, 0.25};
  int num_graphs = c2D[WORLD].size * 2;
  a.GenerateWithG500(initiator, 4, 0.5, num_graphs);
  a.SaveLocal("a");
  
  // Cleans up.
  char cmd[20];
  sprintf(cmd, "rm a-%05d", c2D[WORLD].rank);
  system(cmd);
  delete [] c2D;
  
  return true;
}

bool TestGenerateSymmetric1D() {
  Comm *c1D = GetComms(MPI_COMM_WORLD);
  DistSpMat<IT, VT> a(c1D);
  a.GenerateSymmetric(19, 19, 0.5, c1D[WORLD].size * 2);
  a.SaveLocal("a");
  
  SpMat<IT, VT> A = a.GetWholeMatrix();
  bool res = A.IsSymmetric();

  // Cleans up.
  char cmd[20];
  sprintf(cmd, "rm a-%05d", c1D[WORLD].rank);
  system(cmd);
  delete [] c1D;

  return res;
}

bool TestGenerateSymmetric2D() {
  Comm *c2D = GetComms(MPI_COMM_WORLD, TWOD_ROWMAJOR, 1);
  DistSpMat<IT, VT> a(c2D);
  a.GenerateSymmetric(21, 21, 0.5, c2D[WORLD].size * 2);
  a.SaveLocal("a");
  
  SpMat<IT, VT> A = a.GetWholeMatrix();
  bool res = A.IsSymmetric();

  // Cleans up.
  char cmd[20];
  sprintf(cmd, "rm a-%05d", c2D[WORLD].rank);
  system(cmd);
  delete [] c2D;

  return res;
}

bool TestGetLocalSpMatsFromComm() {
  bool res = true;
  Comm *c1D = GetComms(MPI_COMM_WORLD);
  DistSpMat<IT, VT> a(c1D);
  a.GenerateUniform(20, 20, 0.5);
  SpMat<IT, VT> *spmats = a.GetLocalSpMatsFromComm(c1D[WORLD]);
  a.SaveLocal("local");
  
  MPI_Barrier(MPI_COMM_WORLD);
  if (a.rank() == 0) {
    for (int i = 0; i < a.nprocs(); ++i) {
      SpMat<IT, VT> b;
      b.Load("local", i);
      res &= spmats[i].IsEquivalent(b);
      if (!spmats[i].IsEquivalent(b)) {
        std::cout << i << std::endl;
        spmats[i].Print();
        b.Print();
      }
    }
    system("rm local-*");
  }
  
  delete [] c1D;
  return res;
}

bool TestGetWholeMatrix() {
  Comm *c1D = GetComms(MPI_COMM_WORLD);
  Comm *c2D = GetComms(MPI_COMM_WORLD, TWOD_ROWMAJOR, 1);
  DistSpMat<IT, VT> a(c1D);
  DistSpMat<IT, VT> b(c2D);
  a.GenerateUniform(15, 15, 0.5);
  b.GenerateUniform(15, 15, 0.5);
  
  SpMat<IT, VT> whole_a = a.GetWholeMatrix();
  SpMat<IT, VT> whole_b = b.GetWholeMatrix();
  
  delete [] c1D;
  delete [] c2D;
  
  return whole_a.IsEquivalent(whole_b);
}

bool TestSetIdentity() {
  Comm *c1D = GetComms(MPI_COMM_WORLD);
  Comm *c2D = GetComms(MPI_COMM_WORLD, TWOD_ROWMAJOR, 1);
  DistSpMat<IT, VT> a(c1D);
  DistSpMat<IT, VT> b(c2D);
  a.SetIdentity(21);
  b.SetIdentity(21);
  
  SpMat<IT, VT> whole_a = a.GetWholeMatrix();
  SpMat<IT, VT> whole_b = b.GetWholeMatrix();
  
  LT headptrs[22];
  LT indices[21];
  VT values[21];
  fill_series(headptrs, 22, (LT) 0);
  fill_series(indices, 21, (LT) 0);
  fill_vec(values, 21, (VT) 1.0);
  
  SpMat<IT, VT> I;
  I.PointersToSpMat(21, 21, 21, SPARSE_CSR, 0,
                    headptrs, indices, values, 0);
  
  delete [] c1D;
  delete [] c2D;
  
  bool res = true;
  res &= whole_a.IsEquivalent(I);
  res &= whole_b.IsEquivalent(I);

  return res;
}

bool TestShift1D() {
  Comm *c1D = GetComms(MPI_COMM_WORLD);
  DistSpMat<IT, VT> a(c1D);
  a.GenerateUniform(15, 15, 0.7);
  a.SaveLocal("a");
  MPI_Barrier(MPI_COMM_WORLD);
  
  bool res = true;
  int check = c1D[LAYER_COL].rank;
  for (int i = 0; i < c1D[LAYER_COL].size + 2; ++i) {
    a.Shift(LAYER_COL, 1);
    check = (check + 1) % c1D[LAYER_COL].size;
    SpMat<IT, VT> b;
    b.Load("a", check);
    res &= b.IsEquivalent(a.mat_);
  }
  
  // Cleans up.
  MPI_Barrier(MPI_COMM_WORLD);
  char cmd[20];
  sprintf(cmd, "rm a-%05d", c1D[WORLD].rank);
  system(cmd);
  delete [] c1D;
  
  return res;
}

bool TestShift2D() {
  Comm *c2D = GetComms(MPI_COMM_WORLD, TWOD_ROWMAJOR, 1);
  DistSpMat<IT, VT> a(c2D);
  a.GenerateUniform(14, 18, 0.8);
  a.SaveLocal("a");
  MPI_Barrier(MPI_COMM_WORLD);
  
  bool res = true;
  int row = c2D[LAYER_COL].rank;
  int col = c2D[LAYER_ROW].rank;
  for (int i = 0; i < c2D[LAYER_COL].size; ++i) {
    a.Shift(LAYER_COL, 1);
    row = (row + 1) % c2D[LAYER_COL].size;
    SpMat<IT, VT> b;
    b.Load("a", row * c2D[LAYER_ROW].size + col);
    res &= b.IsEquivalent(a.mat_);
  }
  for (int j = 0; j < c2D[LAYER_ROW].size; ++j) {
    a.Shift(LAYER_ROW, 1);
    col = (col + 1) % c2D[LAYER_ROW].size;
    SpMat<IT, VT> b;
    b.Load("a", row * c2D[LAYER_ROW].size + col);
    res &= b.IsEquivalent(a.mat_);
  }
  
  // Cleans up.
  MPI_Barrier(MPI_COMM_WORLD);
  char cmd[20];
  sprintf(cmd, "rm a-%05d", c2D[WORLD].rank);
  system(cmd);
  delete [] c2D;
  
  return res;
}

bool TestBcast1D() {
  Comm *c1D = GetComms(MPI_COMM_WORLD);
  DistSpMat<IT, VT> a(c1D);
  a.GenerateUniform(23, 21, 0.4);
  a.SaveLocal("a");
  MPI_Barrier(MPI_COMM_WORLD);
  
  DistSpMat<IT, VT> b(c1D);
  b.DeepCopy(a);
  
  bool res = true;
  for (int i = 0; i < c1D[LAYER_COL].size; ++i) {
    if (c1D[LAYER_COL].rank == i)
      b.mat_.DeepCopy(a.mat_);
    b.Bcast(LAYER_COL, i);
    SpMat<IT, VT> c;
    c.Load("a", i);
    res &= c.IsEquivalent(b.mat_);
  }
  
  // Cleans up.
  MPI_Barrier(MPI_COMM_WORLD);
  char cmd[20];
  sprintf(cmd, "rm a-%05d", c1D[WORLD].rank);
  system(cmd);
  delete [] c1D;
  
  return res;
}

bool TestBcast2D() {
  Comm *c2D = GetComms(MPI_COMM_WORLD, TWOD_ROWMAJOR, 1);
  DistSpMat<IT, VT> a(c2D);
  a.GenerateUniform(21, 23, 0.4);
  a.SaveLocal("a");
  MPI_Barrier(MPI_COMM_WORLD);
  
  DistSpMat<IT, VT> b(c2D), c(c2D);
  b.DeepCopy(a);
  bool res = true;
  for (int i = 0; i < c2D[LAYER_COL].size; ++i) {
    if (c2D[LAYER_COL].rank == i)
      b.mat_.DeepCopy(a.mat_);
    b.Bcast(LAYER_COL, i);
    SpMat<IT, VT> c;
    c.Load("a", i * c2D[LAYER_ROW].size + c2D[LAYER_COL].id);
    res &= c.IsEquivalent(b.mat_);
  }
  MPI_Barrier(MPI_COMM_WORLD);
  
  for (int j = 0; j < c2D[LAYER_ROW].size; ++j) {
    if (c2D[LAYER_ROW].rank == j)
      b.mat_.DeepCopy(a.mat_);
    b.Bcast(LAYER_ROW, j);
    SpMat<IT, VT> c;
    c.Load("a", c2D[LAYER_ROW].id * c2D[LAYER_ROW].size + j);
    res &= c.IsEquivalent(b.mat_);
  }
  MPI_Barrier(MPI_COMM_WORLD);
  
  char cmd[20];
  sprintf(cmd, "rm a-%05d", c2D[WORLD].rank);
  system(cmd);
  delete [] c2D;
  return res;
}

bool TestDiagonal() {
  Comm *c1D = GetComms(MPI_COMM_WORLD);
  DistSpMat<IT, VT> a(c1D), b(c1D);
  a.GenerateUniform(23, 23, 0.5);
  b.Diagonal(a);
  
  SpMat<IT, VT> wa = a.GetWholeMatrix();
  SpMat<IT, VT> wb = b.GetWholeMatrix();
  SpMat<IT, VT> d;
  d.Diagonal(wa);
  
  delete [] c1D;
  return wb.IsEquivalent(d);
}

bool TestDiagonalFromDense() {
  int n = 123;
  bool res = true;
  
  Comm *c1Drow = GetComms(MPI_COMM_WORLD, ONED_BLOCK_ROW, 1);
  Comm *c1Dcol = GetComms(MPI_COMM_WORLD, ONED_BLOCK_COLUMN, 1);
  
  // Block row.
  DistDMat<IT, VT> A(c1Drow);
  A.GenerateWithFunction(n, n);
  DistSpMat<IT, VT> Irow(c1Drow), Drow(c1Drow);
  Irow.SetIdentity(n);
  Irow.DotMultiplyI(A);
  Drow.Diagonal(A);
  res &= Irow.mat_.IsEquivalent(Drow.mat_);
  
  // Block column.
  DistDMat<IT, VT> B(c1Dcol);
  B.GenerateWithFunction(n, n);
  DistSpMat<IT, VT> Icol(c1Dcol), Dcol(c1Dcol);
  Icol.SetIdentity(n);
  Icol.DotMultiplyI(B);
  Dcol.Diagonal(B);
  res &= Icol.mat_.IsEquivalent(Dcol.mat_);
  
  delete [] c1Drow;
  delete [] c1Dcol;
  
  return true;
}

bool TestFrobeniusNormSquare() {
  Comm *c1D = GetComms(MPI_COMM_WORLD);
  DistSpMat<IT, VT> a(c1D);
  a.GenerateUniform(23, 21, 0.5);
  VT fro_square = a.FrobeniusNormSquare();
  
  SpMat<IT, VT> wa = a.GetWholeMatrix();
  VT ans = wa.FrobeniusNormSquare();
  
  delete [] c1D;
  return (fro_square == ans);
}

bool TestSave() {
  Comm *c1D = GetComms(MPI_COMM_WORLD);
  DistSpMat<IT, VT> a(c1D);
  a.GenerateUniform(23, 21, 0.5);
  a.Save("csr");
  a.SaveDense("dense");
  
  // Checks Save.
  bool res = true;
  SpMat<IT, VT> b = a.GetWholeMatrix();
  SpMat<IT, VT> c;
  c.Load("csr");
  res &= c.IsEquivalent(b);
  
  // Checks SaveDense.
  DMat<IT, VT> d;
  d.Load("dense");
  SpMat<IT, VT> s;
  s.Convert(d);
  res &= s.IsEquivalent(b);
  
  // Cleans up.
  if (a.rank() == 0)
    system("rm csr dense");
  delete [] c1D;
  return res;
}

bool TestSaveMatrixMarket() {
  Comm *c1D = GetComms(MPI_COMM_WORLD);
  DistSpMat<IT, VT> a(c1D);
  a.GenerateUniform(10, 10, 0.5);
  a.SaveMatrixMarket("a-parts");
  SpMat<IT, VT> wa = a.GetWholeMatrix();
  
  // Concatenates the saved parts and checks the answer.
  bool res;
  if (a.rank() == 0) {
    system("cat a-parts-* > a.mkt; rm a-parts-*");
    SpMat<IT, VT> loaded;
    loaded.LoadMatrixMarket("a.mkt");
    res = wa.IsEquivalent(loaded);
    system("rm -f a.mkt");
  } else {
    res = true;
  }
  
  delete [] c1D;
  return res;
}

bool TestLoadMatrixMarket() {
  SpMat<IT, VT> a;
  a.Generate(99, 99, 0.4);
  a.SaveMatrixMarket("a.mkt");
  
  Comm *c1D = GetComms(MPI_COMM_WORLD);
  DistSpMat<IT, VT> b(c1D);
  b.LoadMatrixMarket("a.mkt");
  SpMat<IT, VT> wb = b.GetWholeMatrix();
  
  delete [] c1D;
  system("rm -f a.mkt");
  
  return wb.IsEquivalent(a);
}

}  // namespace spdm3

#define ADDTEST(fn, name) tests.push_back(std::make_pair(fn, name));

int main(int argc, char* argv[]) {
  // Registers test cases.
  std::list< std::pair<std::function<bool()>, std::string> > tests;
  
  ADDTEST(spdm3::TestGenerateUniform, "TestGenerateUniform");
  ADDTEST(spdm3::TestGenerateSymmetric1D, "TestGenerateSymmetric1D");
  ADDTEST(spdm3::TestGenerateSymmetric2D, "TestGenerateSymmetric2D");
  ADDTEST(spdm3::TestGetLocalSpMatsFromComm, "TestGetLocalSpMatsFromComm");
  ADDTEST(spdm3::TestGetWholeMatrix, "TestGetWholeMatrix");
  ADDTEST(spdm3::TestSetIdentity, "TestSetIdentity");
  ADDTEST(spdm3::TestShift1D, "TestShift1D");
  ADDTEST(spdm3::TestShift2D, "TestShift2D");
  ADDTEST(spdm3::TestBcast1D, "TestBcast1D");
  ADDTEST(spdm3::TestBcast2D, "TestBcast2D");
  ADDTEST(spdm3::TestDiagonal, "TestDiagonal");
  ADDTEST(spdm3::TestDiagonalFromDense, "TestDiagonalFromDense");
  ADDTEST(spdm3::TestFrobeniusNormSquare, "TestFrobeniusNormSquare");
  ADDTEST(spdm3::TestSave, "TestSave");
  ADDTEST(spdm3::TestGenerateWithG500OneD, "TestGenerateWithG500OneD");
  ADDTEST(spdm3::TestGenerateWithG500TwoD, "TestGenerateWithG500TwoD");
  ADDTEST(spdm3::TestSaveMatrixMarket, "TestSaveMatrixMarket");
  ADDTEST(spdm3::TestLoadMatrixMarket, "TestLoadMatrixMarket");
  
  MPI_Init(&argc, &argv);
  int p = 1, rank = 0;
  MPI_Comm_size(MPI_COMM_WORLD, &p);
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  if (p != 4) {
    std::cout << "This test must be run with 4 MPI processes. "
              << "Terminating..." << std::endl;
    exit(1);
  }
  
  // Runs tests.
  int failed = 0;
  for (auto test : tests) {
    bool res = test.first();
    if (rank == 0) {
      MPI_Reduce(MPI_IN_PLACE, &res, 1, MPI_BYTE, MPI_BAND, 0, MPI_COMM_WORLD);
      std::cout << std::setw(50) << test.second << ": ";
      if (res) {
        std::cout << "PASSED." << std::endl;
      } else {
        ++failed;
        std::cout << "FAILED." << std::endl;
      }
    } else {
      MPI_Reduce(&res, NULL, 1, MPI_BYTE, MPI_BAND, 0, MPI_COMM_WORLD);
    }
  }
  
  // Summarizes.
  if (rank == 0) {
    if (!failed)
      std::cout << "All " << tests.size() << " tests passed." << std::endl;
    else
      std::cout << failed << " out of " << tests.size()
                << " tests failed." << std::endl;
  }

  MPI_Finalize();
  return 0;
}
