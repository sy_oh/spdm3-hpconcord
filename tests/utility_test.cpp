#include "utility.h"

#include <functional>
#include <iomanip>
#include <iostream>
#include <list>

namespace spdm3 {

bool TestCompareVec() {
  double a[] = {1, 2, 3, 4, 5};
  double b[] = {1, 2, 3, 4, 5};
  double c[] = {1, 2, 3, 4, 4};
  if (!(vec_equal(a, b, 5)))
    return false;
  return !(vec_equal(a, c, 5));
}

bool TestFillVec() {
  double a[] = {11, 11, 11, 11, 11};
  double b[] = {0, 0, 0, 0, 11};
  fill_vec(b, 4, 11.0);
  return vec_equal(a, b, 5);
}

bool TestFillSeries() {
  double a[] = {2, 3, 4, 5, 6};
  double b[5];
  fill_series(b, 5, 2.0);
  return vec_equal(a, b, 5);
}

bool TestReverseBit() {
  bool res = true;
  res &= (reverse_bit(11, (1<<4)-1) == 13);
  res &= (reverse_bit( 7, (1<<5)-1) == 28);
  res &= (reverse_bit(44, (1<<6)-1) == 13);
  res &= (reverse_bit(43, (1<<6)-1) == 53);
  return res;
}

bool TestPowersOfTwo() {
  bool res = true;
  res &= is_powers_of_two(1);
  res &= is_powers_of_two(2);
  res &= is_powers_of_two(16);
  res &= is_powers_of_two(32768);
  
  res &= !is_powers_of_two(0);
  res &= !is_powers_of_two(10);
  res &= !is_powers_of_two(31);
  res &= !is_powers_of_two(33);
  return res;
}

}  // namespace spdm3

#define ADDTEST(fn, name) tests.push_back(std::make_pair(fn, name));

int main(int argc, char* argv[]) {
  // Registers test cases.
  std::list< std::pair<std::function<bool()>, std::string> > tests;
  
  ADDTEST(spdm3::TestCompareVec, "TestCompareVec");
  ADDTEST(spdm3::TestFillVec, "TestFillVec");
  ADDTEST(spdm3::TestFillSeries, "TestFillSeries");
  ADDTEST(spdm3::TestReverseBit, "TestReverseBit");
  ADDTEST(spdm3::TestPowersOfTwo, "TestPowersOfTwo");
  
  // Runs tests.
  int failed = 0;
  for (auto test : tests) {
    bool res = test.first();
    std::cout << std::setw(50) << test.second << ": ";
    if (res) {
      std::cout << "PASSED." << std::endl;
    } else {
      ++failed;
      std::cout << "FAILED." << std::endl;
    }
  }
  
  // Summarizes.
  if (!failed)
    std::cout << "All " << tests.size() << " tests passed." << std::endl;
  else
    std::cout << failed << " out of " << tests.size()
              << " tests failed." << std::endl;
  return 0;
}