#include <stdio.h>
#include <mpi.h>

int main(int argc, char *argv[]){
  int procs, rank;
  
  // Inits MPI.
  MPI_Init(&argc, &argv);
  MPI_Comm_size(MPI_COMM_WORLD, &procs);
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  
  // Allocation.
  int n = 160;
  double *buffer = new double[n];
  double *temp = new double[n];
  
  // MPI requests.
  MPI_Request mpi_send_req;
  MPI_Request mpi_recv_req;
  
  // Deallocation.
  delete [] buffer;
  delete [] temp;
  
  // Finalizes MPI.
  MPI_Finalize();
  return 0;
}
